@extends('layouts.master')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Update User Profile</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Update User profile
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form class="form-horizontal" enctype="multipart/form-data" action="{{route('updateprofile')}}" method="post">
                 @csrf
                    <div class="text-center">
                    <input style="display:none;" name="profile_pic" type="file" id="imgupload" onchange="loadFile(event)">
                    <img id="profile_img" onclick="$('#imgupload').trigger('click'); return false;" class="profile-user-img img-fluid img-circle" src="{{asset('images/users/'.$user->profile_pic)}}" alt="User profile picture">
                    </div>
                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                          <input type="name" name="name" required="" class="form-control" value="{{$user->name}}" id="inputName" placeholder="Enter Name">
                        </div>
                      </div>
                      <input type="hidden" name="user_id" value="<?=$user->id?>">
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                          <input type="email" name="email" readonly class="form-control"  value="{{$user->email}}" id="inputEmail" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                          <input type="password" name="password" class="form-control"  value="" id="inputEmail" placeholder="Enter new password">
                        </div>
                      </div>
                
                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Mobile No.</label>
                        <div class="col-sm-10">
                          <input name="mobile_no" type="text" class="form-control" id="inputSkills"  value="{{$user->mobile_no}}" placeholder="Enter Phone Number">
                        </div>
                      </div>
                    
                      <div class="form-group row">
                        <div class="offset-sm-2 col-sm-10">
                          <button type="submit" class="btn btn-danger">Submit</button>
                        </div>
                      </div>
                    </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
    <script>
        var loadFile = function(event) {
            var output = document.getElementById('profile_img');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
            }
        };
    </script>
@endsection