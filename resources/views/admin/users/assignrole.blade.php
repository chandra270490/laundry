@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Assign Role</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
</br>
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Assign Roles</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form method="post" action="{{route('saverole')}}">
                @csrf
                <div class="row">
                   
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Select User</label>
                        <select name="user" class="form-control">
                            <option value="">Select User</option>
                            <option value="{{$user->id}}" selected>{{ $user->name }}</option>               
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <!-- Select multiple-->
                      <div class="form-group">
                        <label>Select Role</label>
                        <select name="roles[]" multiple="" class="form-control" >
                            <option value="">Select Role</option>
                            <option value="1" @if(!empty($userroles) && in_array(1,$userroles)) selected @endif>Facility</option>
                            <option value="2"  @if(!empty($userroles) && in_array(2,$userroles)) selected @endif>Admin</option>
                            <option value="3" @if(!empty($userroles) && in_array(3,$userroles)) selected @endif>Others</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Assign Role</button>
                        </div>
                    </div><!-- /.container-fluid -->
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

   
