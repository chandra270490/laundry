@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Update User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Update User
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('users.update',$user)}}" method="post">
                 @csrf
                 {{ method_field('PUT') }}
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="name" value="{{ old('name',$user->name ?? '')}}">
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="email" value="{{ old('email',$user->email ?? '')}}">
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Password</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" name="password" value="">
                  </div>
                  <div class="form-group">
                  <label for="exampleInputEmail1">Mobile No</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="mobile_no" value="{{ old('mobile_no',$user->mobile_no ?? '')}}">
                  </div>
                
                  <button type="submit" name="update_users" class="btn btn-primary">Update User</button>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
