@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Show User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <p>Show User content will goes here.....</p>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

   
