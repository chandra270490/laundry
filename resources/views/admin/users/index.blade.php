@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Users</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   
     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
      @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Users</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="users" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#SR No.</th>
                    <th>User Name</th>
                    <th>Email</th>
                    <th>Change Permission Role</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @foreach($users as $user)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{$user->name}}
                      </td>
                      <td>{{$user->email}}</td>
                      <td>
                         <a href="{{ route('assignrole',$user->id)}}" class="btn btn-xs btn-primary"><i class="fas fa-arrow-alt-circle-right"></i> Change Role</a>
                        <!-- @if($user->role_id == 1) API User @elseif($user->role_id == 0) Fresh User @endif --></td>
                      <td>
                     
                      <a href="{{route('users.edit',$user->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('users.show',$user->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a href="{{route('users.destroy',$user->id)}}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  @endforeach
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </section>
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#users').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection

   
