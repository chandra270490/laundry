@extends('layouts.master')
@php  use App\User; @endphp
@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Pages</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('pages.create')}}" class="btn btn-success">Add Page +</a>
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
      </div><!-- /.container-fluid -->
    </section>
</br>
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
     @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Pages</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="pages" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Publish Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $i=1; @endphp
                  @if(!empty($pages) && count($pages) > 1)
                  @foreach($pages as $page)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$page->name}}</td>
                    <td>{{substr($page->description, 0, 100)}}...</td>
                    <td>@php echo $page->status==1?'<i class="fa fa-circle  text-green"></i> Active':'<i class="fa fa-circle  text-warning"></i> Draft'; @endphp</td>
                    <td>
                      <a href="{{route('pages.edit',$page->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('pages.show',$page->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$page->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <div class="modal fade" id="modal-default<?=$page->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Page</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('pages.destroy', $page->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              </div>
                  @endforeach
                  @elseif(count($pages) == 1)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$pages[0]->name}}</td>
                   
                    <td>{{substr($pages[0]->description, 0, 100)}}...</td>
                    <td>@php echo $pages[0]->status==1?'<i class="fa fa-circle  text-green"></i>Active':'<i class="fa fa-circle  text-warning"></i> Draft'; @endphp</td>
                    <td>
                      <a href="{{route('pages.edit',$pages[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('pages.show',$pages[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a role="button" data-toggle="modal" data-target="#modal-default" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <div class="modal fade" id="modal-default">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Page</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('pages.destroy', $pages[0]->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @else
                  <tr>
                    <td colspan="5">No Data availabale </td>
                  </tr>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#pages').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection

   
