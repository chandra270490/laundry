@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">{{$page->name}}</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <!-- form start -->
              <form action="{{ route('pages.update',$page)}}" enctype="multipart/form-data" method="POST">
                @csrf
               {{ method_field('PUT') }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Page Name <span class="text-danger">*</span></label>
                    <input type="text" id="name" name="name" value="{{ old('name',$page->name ?? '')}}" required="" class="form-control" placeholder="Enter Page name">
                  </div>
                  <div class="form-group">
                    <label for="title">Title <span class="text-danger">*</span></label>
                    <input type="text" id="title" name="title" value="{{ old('name',$page->title ?? '')}}" required="" class="form-control" placeholder="Page Title">
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="feature_image">Picture (Optional)</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                        <span class="input-group-text">
                          @if(!empty($page->feature_image))
                            <img src="{{ asset('images/blogs/'.$page->feature_image)}}" width="150px" height="68px">
                          @endif    
                      </span>
                    </div>
                    <input type="file" id="feature_image" name="feature_image" class="form-control file-upload-control">
                  </div>
                  
                  </div>

                  <div class="col-md-8">
                  <div class="form-group">
                    <label for="media_link">Media Link (Optional)</label>
                    <input type="text" id="media_link" name="media_link" value="{{ old('media_link',$page->media_link ?? '')}}" placeholder="https://www.youtube.com/videolink" class="form-control">
                  </div>
                </div>
              </div>
              <br/>
                  <div class="form-group">
                    <div class="row">
                    <div class="col-md-4">
                      <label>Add in header Navbar</label>
                      <input type="radio" id="add_top_navbar_yes" name="add_top_navbar" @if(isset($page->add_top_navbar) && $page->add_top_navbar == 0) checked @endif value="0"  /> 
                      <label for="add_top_navbar_yes">No </label>
                      <input type="radio"  id="add_top_navbar_no" name="add_top_navbar" @if(isset($page->add_top_navbar) && $page->add_top_navbar == 1) checked @endif value="1" /> <label for="add_top_navbar_no">Yes </label>
                    </div>
                    <div class="col-md-4">
                      <label>Add in Footer Navbar</label>
                      <input type="radio" id="add_footer_navbar_no" name="add_footer_navbar" @if(isset($page->add_footer_navbar) && $page->add_footer_navbar == 0) checked @endif value="0" /> 
                      <label for="add_footer_navbar_no">No </label>
                      <input type="radio"  id="add_footer_navbar_yes" name="add_footer_navbar" @if(isset($page->add_footer_navbar) && $page->add_footer_navbar == 1) checked @endif value="1" /> <label for="add_footer_navbar_yes">Yes </label>
                    </div>
                  </div>
                  <br/>
                  <div class="form-group">
                    <label for="codeMirrorDemo"> Page Description</label>
                    <textarea id="summernote" name="description" class="form-control p-3">{{ old('description',$page->description ?? '')}}</textarea>
                  </div>
                  </div>
                  <div class="text-right">
                  <button type="submit" name="update_page" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button></div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
 