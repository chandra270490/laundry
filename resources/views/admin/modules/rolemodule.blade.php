@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Assign Role To Module</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
</br>
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
     @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Assign Roles To Modules</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form method="post" action="{{route('saveroletomodule')}}">
                @csrf
                <div class="row">
                    <div class="col-sm-6">
                      <!-- Select multiple-->
                      <div class="form-group">
                        <label>Select Role</label>
                        <select name="roles[]" multiple="" class="form-control">
                            <option value="">Select Role</option>
                            @foreach($roles as $role)
                            <option value="{{$role->id}}" @if(!empty($roleModulesArray) && is_array($roleModulesArray) && in_array($role->id,$roleModulesArray)) selected @endif>{{$role->name}}</option>
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label>Please Check Modules</label>
                        @foreach($modules as $module)
                       
                        <div class="form-check">
                          <input class="form-check-input"  @if(!empty($moduleroles))
                        @foreach($moduleroles as $moduleKey=>$moduleVal) @if($moduleVal->id == $module->id) checked @endif @endforeach @endif type="checkbox" name="modules[]" value="{{$module->id}}">
                          <label for="customCheckbox1" class="form-check-label">{{$module->name}}</label>
                        </div>

                        @endforeach
                      </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Assign Role</button>
                        </div>
                    </div><!-- /.container-fluid -->
                  </div>
                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection

   
