@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <form enctype="multipart/form-data" action="{{route('settings.store')}}" method="post">
                 @csrf
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Update Setting</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><button type="submit" name="update_settings" class="btn btn-primary"><i class="fa fa-save"></i> Update Setting</button></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">
              <i class="fas fa-edit"></i>
                Settings
            </h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-5 col-sm-3">
                <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                  <a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="true">General</a>
                  <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Social Media</a>
                  <a class="nav-link" id="vert-tabs-messages-tab" data-toggle="pill" href="#vert-tabs-messages" role="tab" aria-controls="vert-tabs-messages" aria-selected="false">Contact</a>
                </div>
              </div>
              <div class="col-7 col-sm-9">
                
                <div class="tab-content" id="vert-tabs-tabContent">
                <div class="tab-pane text-left fade show active" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
                  <div class="card-body">
                    <div class="row">
                    <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Primary Phone</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="primary_phone" value="{{ old('primary_phone',$setting->primary_phone ?? '')}}" placeholder="Enter Primary Phone">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Primary Email</label>
                    <input type="text" class="form-control" name="primary_email" value="{{ old('primary_email',$setting->primary_email ?? '')}}" id="exampleInputPassword1" placeholder="Enter Primary Email">
                  </div>

                </div>
                <div class="col-md-6">  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Secondary Phone</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="secondary_phone" value="{{ old('secondary_phone',$setting->secondary_phone ?? '')}}" placeholder="Enter Secondary Phone">
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Secondary Email</label>
                    <input type="text" class="form-control" name="secondary_email" value="{{ old('secondary_email',$setting->secondary_email ?? '')}}" id="exampleInputPassword1" placeholder="Enter Secondary Email">
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Website Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="website_name" value="{{ old('website_name',$setting->website_name ?? '')}}" placeholder="Enter Website Name">
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Website Url</label>
                    <input type="text" class="form-control" name="website_url" value="{{ old('website_url',$setting->website_url ?? '')}}" id="exampleInputPassword1" placeholder="Enter Website URL">
                  </div>

                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputFile">Logo</label>
                    <div class="input-group">
                      @if(!empty($setting->logo))
                      <input type="hidden" name="old_logo" value="{{$setting->logo}}">
                      <div class="input-group-append">
                        <span class="input-group-text">

                      <img src="{{ asset('images/settings/'.$setting->logo)}}" alt="Logo" width="20px" height="20px">
                    </span>
                      </div>
                      @endif
                      <div class="custom-file">
                        <input type="file" name="logo" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      
                    </div>
                    
                  </div>

                </div>
                <div class="col-md-6">  
                  <div class="form-group">
                    <label for="exampleInputEmail1">Timezone</label>
                    <input type="text" name="timezone" value="{{old('timezone',$setting->timezone ?? '')}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Timezone">
                  </div>

                </div>
                
                <div class="col-md-6">  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Date Time Format</label>
                    <input type="text" class="form-control" name="date_time_format" value="{{old('date_time_format',$setting->date_time_format ?? '')}}" id="exampleInputPassword1" placeholder="Password">
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label for="exampleInputEmail1">Android App Link</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="android_app_link" value="{{ old('android_app_link',$setting->android_app_link ?? '')}}" placeholder="Enter Android App Link">
                    </div>

                </div>
                <div class="col-md-6"> 
                  <div class="form-group">
                  <label for="exampleInputEmail1">IOS App Link</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="ios_app_link" value="{{ old('ios_app_link',$setting->ios_app_link ?? '')}}" placeholder="Enter IOS App Link">
                    </div>
                  </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleSelectBorder">Set Language</label>
                    <select name="language" class="custom-select form-control" id="exampleSelectBorder">
                      <option value="">Please Select</option>
                      <option value="en" @if(isset($setting->language) && $setting->language == 'en') selected @endif>English</option>
                      <!-- <option value="fr" @if(isset($setting->language) && $setting->language == 'fr') selected @endif>French</option> -->
                    </select>
                  </div>
                  
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleSelectBorder">Set Currency</label>
                    <select name="currency" class="custom-select form-control" id="exampleSelectBorder">
                      <option value="">Please Select</option>
                      <option value="$" @if(isset($setting->currency) && $setting->currency == '$') selected @endif>$</option>
                      <option value="£" @if(isset($setting->currency) && $setting->currency == '£') selected @endif>£</option>
                      <option value="€" @if(isset($setting->currency) && $setting->currency == '€') selected @endif>€</option>
                    </select>
                  </div>

                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Working Time</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="working_time" value="{{ old('working_time',$setting->working_time ?? '')}}" placeholder="Enter Working Time">
                  </div>

                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Working Days</label>
                    <input type="text" class="form-control" name="working_days" value="{{ old('working_days',$setting->working_days ?? '')}}" id="exampleInputPassword1" placeholder="Enter Working Days">
                  </div>

                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Page Title</label>
                    <input type="text" class="form-control" name="page_title" value="{{old('page_title',$setting->page_title ?? '')}}" id="exampleInputPassword1" placeholder="Password">
                  </div>

                </div>
                <div class="col-md-12">    
                  <div class="form-group">
                    <label for="exampleInputPassword1">Footer About Us</label>
                    <input type="text" class="form-control" name="footer_about_us" value="{{ old('footer_about_us',$setting->footer_about_us ?? '')}}" id="exampleInputPassword1" placeholder="Enter Footer Content">
                  </div>
                
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Copyright text</label>
                    <input type="text" class="form-control" name="copyright" value="{{ old('working_days',$setting->copyright_text ?? '')}}" id="exampleInputPassword1" placeholder="Enter Copyright">
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">

                 
                  <label for="exampleInputEmail1">Script Third Party API</label>
                      <textarea class="form-control" id="exampleInputEmail1" name="script_thired_party_api" value="{{ old('script_thired_party_api',$setting->script_thired_party_api ?? '')}}" placeholder="Enter Script Third Party API"></textarea>
                    </div>

                </div>
              </div>
            </div>
                </div>

                  <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">
                  <div class="card-body">
                    <h4>Social Media Links</h4>
                    <hr/>
                    <div class="row">
                      <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-whatsapp mr-1"></i> Number</span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_whatsapp_link" value="{{ old('social_whatsapp_link',$setting->social_whatsapp_link ?? '')}}" placeholder="Enter Social Whatsapp Link">
                    </div>
                  </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-facebook "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_fb_link" value="{{ old('social_fb_link',$setting->social_fb_link ?? '' )}}" placeholder="Enter Social Facebook Link">
                    </div>
                    </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-twitter "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_twitter_link" value="{{ old('social_twitter_link',$setting->social_twitter_link ?? '')}}" placeholder="Enter Social Twitter Link">
                    </div>
                    </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-linkedin "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_linkedin_link" value="{{ old('social_linkedin_link',$setting->social_linkedin_link ?? '' )}}" placeholder="Enter Social Linkedin Link">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-instagram "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_instagram_link" value="{{ old('social_instagram_link',$setting->social_instagram_link ?? '')}}" placeholder="Enter Social Instagram Link">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-youtube "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_youtube_link" value="{{ old('social_youtube_link',$setting->social_youtube_link ?? '')}}" placeholder="Enter Social Youtube Link">
                    </div>
                </div>
                <div class="col-md-6">
                
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-pinterest "></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_pinterest_link" value="{{ old('social_pinterest_link',$setting->social_pinterest_link ?? '')}}" placeholder="Enter Social Pinterest Link">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group mb-3">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fab fa-tumblr"></i></span>
                      </div>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="social_tumblr_link" value="{{ old('social_tumblr_link', $setting->social_tumblr_link ?? '')}}" placeholder="Enter Social Tumblr Link">
                    </div>
                  </div>
                
                </div>
                </div>
              </div>
                <div class="tab-pane fade" id="vert-tabs-messages" role="tabpanel" aria-labelledby="vert-tabs-messages-tab">
                  <div class="card-body">
                    <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Location Address</label>
                      <textarea class="form-control" placeholder="Enter Address" id="exampleInputEmail1" name="location_address1">{{ old('location_address1',$setting->location_address1 ?? '')}}</textarea>
                    </div>
                    </div>
                <div class="col-md-6">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Location Address2</label>
                    <textarea placeholder="Enter Second Address" class="form-control" id="exampleInputEmail1" name="location_address2">{{ old('location_address2',$setting->location_address2 ?? '')}}</textarea>
                    </div>

                    </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="exampleInputEmail1">City</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="city" value="{{ old('city',$setting->city ?? '')}}" placeholder="Enter City">
                    </div>

                    </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="exampleInputEmail1">State</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="state" value="{{ old('state',$setting->state ?? '')}}" placeholder="Enter State">
                    </div>

                    </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Country</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="country" value="{{ old('country',$setting->country ?? '')}}" placeholder="Enter Country">
                    </div>
                    </div>
                <div class="col-md-3">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Post Code</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="zip_code" value="{{ old('zip_code',$setting->zip_code ?? '')}}" placeholder="Enter Post Code">
                    </div>

                    </div>
                <div class="col-md-12">
                    <div class="form-group">
                    <label for="exampleInputEmail1">Google Map Link</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" name="location_map_link" value="{{ old('location_map_link',$setting->location_map_link ?? '')}}" placeholder="Enter Google Map Link">
                    </div>
                </div> 
              </div>
            </div>
                </div>
                <div class="row">
                  <div class="col-md-12 text-right  pr-4 ">
                <button type="submit" name="update_settings" class="btn btn-primary"><i class="fa fa-save"></i> Update Setting</button>
              </div></div>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div><!-- /.container-fluid -->
    </section></form>
    <!-- /.content -->
@endsection