@extends('layouts.master')
@section('content')
@php $roleId = App\Model\RoleUser::where('id',Auth::user()->id)->pluck('role_id')->first(); @endphp
@php $roleName = App\Model\Role::where('id',$roleId)->pluck('name')->first();@endphp
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Orders</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Orders</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if($roleId == 2) 
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Order Number</th>
                      <th>Order Date</th>
                      <th>User Name</th>
                      <th>Address</th>
                      <th>Collection Date</th>
                      <th>Delivery Date</th>
                      <th>Facility Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @foreach($orders as $order)
                    <tr>
                      <td>{{ $order->id }}</td>
                      <td>{{ $order->order_no }}</td>
                      <td>{{ $order->created_at }}</td>
                      <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                      <td>{{ $order->address }}</td>
                      <td>{{ $order->collection_date }}</td>
                      <td>{{ $order->delivery_date }}</td>
                      <td>@php echo $order->facility_status==2?'<label class="badge badge-success">Ready to Deliver</label>':'<label class="badge badge-warning">Pending At Facility</label>'; @endphp</td>
                      <td>
                        <a href="{{route('orders.show',$order->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="{{route('orders.edit',$order->id)}}" class="btn btn-xs btn-info"><i class="fa fa-pencil-alt"></i></a>
                        <!--<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>-->
                      </td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
                @else
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Order Number</th>
                      <th>Order Date</th>
                      <th>User Name</th>
                      <th>Pickup Address</th>
                      <th>Collection Date</th>
                      <th>Delivery Date</th>
                      <th>Facility Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @foreach($orders as $order)
                    @if($order->facility_name == Auth::user()->id)
                    <tr>
                      <td>{{ $order->id }}</td>
                      <td>{{ $order->order_no }}</td>
                      <td>{{ $order->created_at }}</td>
                      <td>{{ $order->first_name }} {{ $order->last_name }}</td>
                      <td>{{ $order->address }}</td>
                      <td>{{ $order->collection_date }}</td>
                      <td>{{ $order->delivery_date }}</td>
                      <td>@php echo $order->facility_status==2?'<label class="badge badge-success">Ready to Deliver</label>':'<label class="badge badge-warning">Pending At Facility</label>'; @endphp</td>
                      <td>
                        <a href="{{route('orders.show',$order->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
                        <a href="{{route('orders.edit',$order->id)}}" class="btn btn-xs btn-info"><i class="fa fa-pencil-alt"></i></a>
                        <!--<a href="#" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>-->
                      </td>
                    </tr>
                    @endif
                    @endforeach
                  </tbody>                  
                </table>
                @endif
              </div>
              <!-- /.card-body -->
            </section>
@endsection

   
