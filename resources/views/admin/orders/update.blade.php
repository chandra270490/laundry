@extends('layouts.master')
@php $roleId = App\Model\RoleUser::where('id',Auth::user()->id)->pluck('role_id')->first(); @endphp
    @php $roleName = App\Model\Role::where('id',$roleId)->pluck('name')->first();@endphp
<style type="text/css">
body {
  
    padding: 0px !important;
    margin: 0px !important;
  
}

.profile-nav, .profile-info{
    margin-top:30px;   
}

.profile-nav .user-heading {
    background: #fbc02d;
    color: #fff;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    padding: 30px;
    text-align: center;
}

.profile-nav .user-heading.round a  {
    border-radius: 50%;
    -webkit-border-radius: 50%;
    border: 10px solid rgba(255,255,255,0.3);
    display: inline-block;
}

.profile-nav .user-heading a img {
    width: 112px;
    height: 112px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
}

.profile-nav .user-heading h1 {
    font-size: 22px;
    font-weight: 300;
    margin-bottom: 5px;
}

.profile-nav .user-heading p {
    font-size: 12px;
}

.profile-nav ul {
    margin-top: 1px;
}

.profile-nav ul > li {
    border-bottom: 1px solid #ebeae6;
    margin-top: 0;
    line-height: 30px;
}

.profile-nav ul > li:last-child {
    border-bottom: none;
}

.profile-nav ul > li > a {
    border-radius: 0;
    -webkit-border-radius: 0;
    color: #89817f;
    border-left: 5px solid #fff;
}

.profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
    background: #f8f7f5 !important;
    border-left: 5px solid #fbc02d;
    color: #89817f !important;
}

.profile-nav ul > li:last-child > a:last-child {
    border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
}

.profile-nav ul > li > a > i{
    font-size: 16px;
    padding-right: 10px;
    color: #bcb3aa;
}

.r-activity {
    margin: 6px 0 0;
    font-size: 12px;
}


.p-text-area, .p-text-area:focus {
    border: none;
    font-weight: 300;
    box-shadow: none;
    color: #c3c3c3;
    font-size: 16px;
}

.profile-info .panel-footer {
    background-color:#f8f7f5 ;
    border-top: 1px solid #e7ebee;
}

.profile-info .panel-footer ul li a {
    color: #7a7a7a;
}

.bio-graph-heading {
    background: #007bff;
    color: #fff;
    text-align: center;
    padding: 40px 110px;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    font-size: 16px;
    font-weight: 300;
}


.bio-row {
    width: 33%;
    float: left;
    margin-bottom: 10px;
    padding:0 15px;
}

.bio-row p span {
    width: 100px;
    display: inline-block;
}

</style>

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Order Summery</h1>
        </div><!-- /.col -->
        <div class="col-sm-6 text-right">
          <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="profile-info col-md-12">
      <div class="panel">
        <div class="panel-body bio-graph-info"> 
          <div class="row">
            <div class="container table-responsive" style="background-color: #fff; margin-top: 25px;border-top-color: #ff0000;border-top: 4px solid #ff0000;">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Order No</th>
                    <th scope="col">Pickup Address</th>
                    <th scope="col">Extra Address</th>
                    <th scope="col">Items</th>
                    <th scope="col">Extra Sevice Request</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:20%">{{$order->order_no}}</td>
                    <td style="width:20%">{{$order->address}}</td>
                    <td style="width:20%">{{$order->extra_address}}</td>
                    @php
                      $items = json_decode(json_decode($order->services));
                    @endphp
                    <td style="width:20%">
                    @php
                      $items = json_decode(json_decode($order->services));
                    @endphp
                    @foreach($items->service as $mydata)
                      <span style="text-transform: capitalize;">{{$mydata->item}} <small>({{$mydata->service}})</small></span>
                      <br/>
                    @endforeach 
                    </td>
                    <td style="width:20%">{{$order->extra_service_request}}</td>
                  </tr>  
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr> 
                    <th scope="col">Collection Date & Time</th>
                    <th scope="col">Delivery Date & Time</th>
                    <th scope="col">Delivery Instruction</th>
                    <th scope="col">Order Date</th> 
                    <th scope="col">Invoice Created</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:20%">{{$order->collection_date}} {{$order->collection_time}}</td>
                    <td style="width:20%">{{$order->delivery_date}} {{$order->delivery_time}}</td>
                    <td style="width:20%">{{$order->delivery_instruction}}</td>
                    <td style="width:20%">{{$order->order_date}}</td>
                    <td style="width:25%">{{$order->invoice_status==1?'Created':'Not Created'}}</td>  
                  </tr>  
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Email Id</th>
                    <th scope="col">Postcode</th>
                    <th scope="col">Mobile Number</th>
                    <th scope="col"></th> 
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:20%">{{$order->first_name}} {{$order->last_name}}</td>
                    <td style="width:20%">{{$order->email_id}}</td>
                    <td style="width:20%">{{$order->postcode}}</td>
                    <td style="width:20%">{{$order->mobile_no}}</td>
                    <td style="width:20%"></td>
                  </tr> 
                </tbody>
              </table>
              <hr/>
              <form action="{{route('orders.update',$order->id)}}" method="post">
            @csrf
            {{ method_field('PUT') }}
          <div class="form-group">
            <div class="row">
              @if($roleId == 1)
              <div class="col-md-2">
                <label for="facilitystatus">Order Status</label>
              </div>

              <div class="col-md-4">
                <select name="facility_status" required="" class="custom-select form-control-border">
            
                  <option value="1" {{ $order->facility_status == 1?'selected':'' }}>Delivery Pending</option>
                  <option value="2" {{ $order->facility_status == 2?'selected':'' }}>Ready To Deliver</option>
                </select>
              </div>
            @endif
            @if($roleId == 2)  
              <div class="col-md-2">
                <label for="invoice_status">Generate Invoice</label>
              </div>

              <div class="col-md-4">
                <select name="invoice_status" required="" class="custom-select form-control-border">
                  <option value="1" @php echo $order->invoice_status == 1?'selected':'' @endphp>Yes</option>
                  <option value="0" @php echo $order->invoice_status == 0?'selected':'' @endphp>No</option>
                </select>
              </div>
             @endif 
              <div class="col-md-6">
                <button type="submit" name="update_order" class="btn btn-primary">Update Order</button>
              </div>
            </div>
          </div>
          </form>
            </div>
          </div>
          
          


        </div>
      </div>
    </div>
    
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
