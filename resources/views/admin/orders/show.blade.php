@extends('layouts.master')
<style type="text/css">
body {
  
    padding: 0px !important;
    margin: 0px !important;
  
}

.profile-nav, .profile-info{
    margin-top:30px;   
}

.profile-nav .user-heading {
    background: #fbc02d;
    color: #fff;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    padding: 30px;
    text-align: center;
}

.profile-nav .user-heading.round a  {
    border-radius: 50%;
    -webkit-border-radius: 50%;
    border: 10px solid rgba(255,255,255,0.3);
    display: inline-block;
}

.profile-nav .user-heading a img {
    width: 112px;
    height: 112px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
}

.profile-nav .user-heading h1 {
    font-size: 22px;
    font-weight: 300;
    margin-bottom: 5px;
}

.profile-nav .user-heading p {
    font-size: 12px;
}

.profile-nav ul {
    margin-top: 1px;
}

.profile-nav ul > li {
    border-bottom: 1px solid #ebeae6;
    margin-top: 0;
    line-height: 30px;
}

.profile-nav ul > li:last-child {
    border-bottom: none;
}

.profile-nav ul > li > a {
    border-radius: 0;
    -webkit-border-radius: 0;
    color: #89817f;
    border-left: 5px solid #fff;
}

.profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
    background: #f8f7f5 !important;
    border-left: 5px solid #fbc02d;
    color: #89817f !important;
}

.profile-nav ul > li:last-child > a:last-child {
    border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
}

.profile-nav ul > li > a > i{
    font-size: 16px;
    padding-right: 10px;
    color: #bcb3aa;
}

.r-activity {
    margin: 6px 0 0;
    font-size: 12px;
}


.p-text-area, .p-text-area:focus {
    border: none;
    font-weight: 300;
    box-shadow: none;
    color: #c3c3c3;
    font-size: 16px;
}

.profile-info .panel-footer {
    background-color:#f8f7f5 ;
    border-top: 1px solid #e7ebee;
}

.profile-info .panel-footer ul li a {
    color: #7a7a7a;
}

.bio-graph-heading {
    background: #007bff;
    color: #fff;
    text-align: center;
    padding: 40px 110px;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    font-size: 16px;
    font-weight: 300;
}


.bio-row {
    width: 33%;
    float: left;
    margin-bottom: 10px;
    padding:0 15px;
}

.bio-row p span {
    width: 100px;
    display: inline-block;
}

</style>

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Show Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="profile-info col-md-12">
        <div class="panel">
          <div class="panel-body bio-graph-info"> 

            <div class="row">
              <!--<div class="bio-row">
                <p><span>Order Id: </span> {{$order->id}}</p>
              </div>

              <div class="bio-row">
                <p><span>Order No: </span> {{$order->order_no}}</p>
              </div>

              <div class="bio-row">
                <p><span>Pickup Address: </span> {{$order->address}}</p>
              </div>

              <div class="bio-row">
                <p><span>Extra Address: </span> {{$order->extra_address}}</p>
              </div>

              <div class="bio-row">
                <p><span>Services: </span> {{$order->services}}</p>
              </div>

              <div class="bio-row">
                <p><span>Extra Sevice Request: </span> {{$order->extra_service_request}} </p>
              </div>

              <div class="bio-row">
                <p><span>Collection Date: </span> {{$order->collection_date}} {{$order->collection_time}}</p>
              </div>

              <div class="bio-row">
                <p><span>Collection Instruction: </span> {{$order->collection_instruction}}</p>
              </div>

              <div class="bio-row">
                <p><span>Delivery Date: </span> {{$order->delivery_date}} {{$order->delivery_time}} </p>
              </div>

              <div class="bio-row">
                <p><span>Delivery Instruction: </span> {{$order->delivery_instruction}}</p>
              </div>

              <div class="bio-row">
                <p><span>Name: </span> {{$order->first_name}} {{$order->last_name}}</p>
              </div>

              <div class="bio-row">
                <p><span>Email Id: </span> {{$order->email_id}}</p>
              </div>

              <div class="bio-row">
                <p><span>Postcode: </span> {{$order->postcode}} </p>
              </div>

              <div class="bio-row">
                <p><span>Mobile Number: </span> {{$order->mobile_no}}</p>
              </div>

              <div class="bio-row">
                <p><span>Postal Code State: </span> {{$order->payment_gateway_with_card}}</p>
              </div>

              <div class="bio-row">
                <p><span>Status: </span> {{$order->status}}</p>
              </div>

              <div class="bio-row">
                <p><span>Created Date: </span> {{$order->created_at}}</p>
              </div>

              <div class="bio-row">
                <p><span>Order Date: </span> {{$order->order_date}}</p>
              </div>

              <div class="bio-row">
                <p><span>Facility Name: </span> {{$order->facility_name}}</p>
              </div>

              <div class="bio-row">
                <p><span>Invoice Status: </span> {{$order->invoice_status}}</p>
              </div>

              <div class="bio-row">
                <p><span>Payment Status: </span> {{$order->payment_status}}</p>
              </div>

              <div class="bio-row">
                <p><span>Voucher Code: </span> {{$order->voucher_code}}</p>
              </div>

              <div class="bio-row">
                <p><span>Job Final: </span> {{$order->job_final}}</p>
              </div>

              <div class="bio-row">
                <p><span>Total Payment: </span> {{$order->total_payment}}</p>
              </div>-->

              <div class="container table-responsive py-5" style="background-color: #fff; padding: 35px;margin-top: 25px;border-top-color: #ff0000;border-top: 4px solid #ff0000;">
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Order Id</th>
                    <th scope="col">Order No</th>
                    <th scope="col">Pickup Address</th>
                    <th scope="col">Extra Address</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:25%">{{$order->id}}</td>
                    <td style="width:25%">{{$order->order_no}}</td>
                    <td style="width:25%">{{$order->address}}</td>
                    <td style="width:25%">{{$order->extra_address}}</td>
                  </tr>  
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Services</th>
                    <th scope="col">Extra Sevice Request</th>
                    <th scope="col">Collection Date</th>
                    <th scope="col">Delivery Date</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:25%">
                    @php
                      $items = json_decode(json_decode($order->services));
                    @endphp
                    @foreach($items->service as $mydata)
                      <span style="text-transform: capitalize;">{{$mydata->item}} <small>({{$mydata->service}})</small></span>
                      <br/>
                    @endforeach 
                    </td>
                    <td style="width:25%">{{$order->extra_service_request}}</td>
                    <td style="width:25%">{{$order->collection_date}} {{$order->collection_time}}</td>
                    <td style="width:25%">{{$order->delivery_date}} {{$order->delivery_time}}</td>
                  </tr>  
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Delivery Instruction</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email Id</th>
                    <th scope="col">Postcode</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:25%">{{$order->delivery_instruction}}</td>
                    <td style="width:25%">{{$order->first_name}} {{$order->last_name}}</td>
                    <td style="width:25%">{{$order->email_id}}</td>
                    <td style="width:25%">{{$order->postcode}}</td>
                  </tr> 
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Mobile Number</th>
                    <th scope="col">Postal Code State</th>
                    <th scope="col">Order Date</th>
                    <th scope="col">Facility Name</th>                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:25%">{{$order->mobile_no}}</td>
                    <td style="width:25%">{{$order->payment_gateway_with_card}}</td>
                    <td style="width:25%">{{$order->order_date}}</td>
                    <td style="width:25%">{{$order->facility_name}}</td>
                  </tr> 
                </tbody>
              </table>

              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th scope="col">Invoice Created</th>
                    <th scope="col">Payment Status</th>
                    <th scope="col">Voucher Code</th>
                    <th scope="col">Total Payment</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="width:25%">{{$order->invoice_status}}</td>
                    <td style="width:25%">{{$order->payment_status}}</td>
                    <td style="width:25%">{{$order->voucher_code}}</td>
                    <td style="width:25%">{{$order->total_payment}}</td>
                  </tr> 
                </tbody>
              </table>
              </div>
            </div>


          </div>
        </div>
      </div>
      
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
