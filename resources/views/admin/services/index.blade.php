@extends('layouts.master')
@php  use App\User; @endphp
@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Services</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('services.create')}}" class="btn btn-success">Add Service +</a>
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
     @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Services</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="services" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Parent Service</th>
                    <th>Descriptions</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $i=1; @endphp
                  @if(!empty($services) && count($services) > 1)
                  @foreach($services as $service)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$service->name}}</td>
                    <td>
                        @php $parentName=''; @endphp
                        @if(!empty($service->parent_id))
                          @php 
                            $childService=App\Model\Service::where('id',$service->parent_id)->first();
                            if(!empty($childService))
                            $parentName=$childService->name;
                          @endphp
                          @if(!empty($parentName)) <strong>{{$parentName}}</strong>@endif
                        @endif  
                    </td>
                    <td>{{substr(htmlentities(strip_tags($service->description)),0,40)}}...</td>
                    <td>
                      <a href="{{route('services.edit',$service->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('services.show',$service->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$service->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$service->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Service</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('services.destroy', $service->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  </tr>
                  @endforeach
                  @elseif(count($services) == 1)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{$services[0]->name}}</td>
                    <td>
                        @php $parentName=''; @endphp
                        @if(!empty($service[0]->parent_id))
                          @php 
                            $childService=App\Model\Service::where('id',$service[0]->parent_id)->first();
                            if(!empty($childService))
                            $parentName=$childService->name;
                          @endphp
                          @if(!empty($parentName)) <strong>{{$parentName}}</strong>@endif
                        @endif  
                    </td>
                    <td>{{substr(htmlentities(strip_tags($services[0]->description)),0,40)}}</td>
                    <td>
                      <a href="{{route('services.edit',$services[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('services.show',$services[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$services[0]->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$services[0]->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Service</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('services.destroy', $services[0]->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#services').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection

   
