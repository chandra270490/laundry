@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Add Services</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Services
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('services.store')}}" method="post">
                 @csrf
                 <div class="row">
                  <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name<span class="text-danger">*</span></label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="name" value="" placeholder="Enter Service Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Title<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="title" value="" placeholder="Enter Service Title">
                  </div>

                </div>
                <div class="col-md-6">  
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="icon" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputFile">Feature image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="feature_image" class="custom-file-input">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1"><span class="text-danger">*</span>Price</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="price" value="" placeholder="Enter Price">
                  </div>

                
                </div>
                <div class="col-md-6">  
                  <div class="form-group">
                    <label for="exampleInputPassword1">Media Link</label>
                    <input type="text" class="form-control" name="media_link" placeholder="Enter Media Link">
                  </div>
                  
                 
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleSelectBorder">Parent Service</label>
                    <select name="parent_id" class="custom-select form-control" id="exampleSelectBorder">
                      <option value="">No Parent Service</option>
                      @foreach($services as $service)
                      <option value="{{$service->id}}" >{{$service->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <label for="exampleSelectBorder">Show on Header Nav</label>
                  <br>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_top_navbar" class="form-check-input" value="1" checked="" /> Yes
                      </label>
                  </div>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_top_navbar" class="form-check-input" value="0"/> No
                      </label>
                  </div>
                </div>
                <div class="col-sm-3">
                  <label for="exampleSelectBorder">Show on Footer Nav</label>
                  <br><div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_footer_navbar" class="form-check-input" value="1" checked="" /> Yes
                      </label>
                    </div>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_footer_navbar" class="form-check-input" value="0"/> No
                      </label>
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1"><span class="text-danger">*</span>Description</label>
                    <textarea id="summernote" required="" class="form-control" name="description" value="" placeholder="Enter description"></textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                <h6><strong>SEO Related Content (Optional)</strong></h6>  
                <hr/>
                </div>
                  <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Meta Title</label>
                    <input type="text" name="meta_title" value="" class="form-control" id="exampleInputEmail1" placeholder="Enter Meta Title">
                  </div>
                </div>

                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Meta Description</label>
                    <textarea  class="form-control" name="meta_description" value="" placeholder="Enter Meta description"></textarea>
                  </div>
                </div>
                <div class="form-group">
                    <label for="exampleSelectBorder">Publish Mode</label>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="service_availablity" class="form-check-input" value="1" checked="">Publish Now
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="service_availablity" class="form-check-input" value="0">Save as Draft
                      </label>
                    </div>
                  </div>
                </div>
                  <div class="text-right">
                  <button type="submit" name="add_services" class="btn btn-primary"><i class="fa fa-save"></i> Add Service</button>
                </div>
                </div>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
