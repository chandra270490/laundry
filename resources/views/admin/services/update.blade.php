@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Update Service</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  <strong>{{$service->name}}</strong>
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('services.update',$service)}}" method="post">
                  @csrf
                {{ method_field('PUT') }}
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1"><span class="text-danger">*</span>Name</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="name" value="{{ old('name',$service->name)}}" placeholder="Enter Service Name">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                  <label for="exampleInputEmail1"><span class="text-danger">*</span>Title</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="title" value="{{ old('title',$service->title)}}" placeholder="Enter Service Title">
                  </div>

                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputFile">Icon</label>
                    <div class="input-group">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          @if(!empty($service->icon))
                            <img src="{{ asset('images/services/icons/'.$service->icon)}}" width="150px" height="68px">
                          @endif
                        </span>
                      </div>
                      <input type="file" name="icon" class="form-control file-upload-control">
                      </div>
                      
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="exampleInputFile">Feature image</label>
                      <div class="input-group">
                        <div class="input-group-append">
                          <span class="input-group-text">
                              @if(!empty($service->feature_image))
                                <img src="{{ asset('images/services/'.$service->feature_image)}}" width="150px" height="68px">
                              @endif
                          </span>
                        </div>
                        <input type="file" name="feature_image" class="form-control file-upload-control">
                      </div>
                      </div>
                    </div>
                <div class="col-sm-6">
                  
                  <div class="form-group">
                    <label for="exampleInputEmail1"><span class="text-danger">*</span>Price</label>
                    <input type="text" required="" class="form-control" id="exampleInputEmail1" name="price" value="{{ old('price',$service->price )}}" placeholder="Enter Price">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Media Link</label>
                    <input type="text" class="form-control" name="media_link" value="{{ old('media_link',$service->media_link)}}" placeholder="Enter Media Link">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleSelectBorder">Parent Category</label>
                    <select name="parent_id" class="custom-select form-control-border" id="exampleSelectBorder">
                      <option value="">No Category</option>
                      @foreach($services as $servicedata)
                      <option value="{{$servicedata->id}}" @if($service->parent_id == $servicedata->id) selected @endif >{{$servicedata->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-sm-3">
                  <label for="exampleSelectBorder">Show on Header Nav</label>
                  <br>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_top_navbar"  class="form-check-input" value="1" @if(isset($service->add_top_navbar) && $service->add_top_navbar == 1) checked @endif /> Yes
                      </label>
                  </div>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_top_navbar" class="form-check-input" value="0" @if(isset($service->add_top_navbar) && $service->add_top_navbar == 0) checked @endif/> No
                      </label>
                  </div>
                </div>
                <div class="col-sm-3">
                  <label for="exampleSelectBorder">Show on Footer Nav</label>
                  <br>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_footer_navbar" class="form-check-input" value="1" @if(isset($service->add_footer_navbar) && $service->add_footer_navbar == 1) checked @endif /> Yes
                      </label>
                    </div>
                  <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="add_footer_navbar" class="form-check-input" value="0" @if(isset($service->add_footer_navbar) && $service->add_footer_navbar == 0) checked @endif/> No
                      </label>
                  </div>
                </div>  
                  <div class="col-sm-12">
                  <div class="form-group">
                    <label for="exampleInputEmail1"><span class="text-danger">*</span>Description</label>
                    <textarea  id="summernote"  required=""  class="form-control" name="description" value="" placeholder="Enter description">{{ old('description',$service->description)}}</textarea>
                  </div>
                </div>
                <div class="col-sm-12">
                <h6><strong>SEO Related Content (Optional)</strong></h6>  
                <hr/>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Meta Title</label>
                    <input type="text" name="meta_title" value="{{ old('meta_title',$service->meta_title)}}" class="form-control" id="exampleInputEmail1" placeholder="Enter Meta Title">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Meta Description</label>
                    <textarea  class="form-control" name="meta_description" value="{{ old('meta_description',$service->meta_description)}}" placeholder="Enter Meta description"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleSelectBorder">Publish Mode</label>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="service_availablity" class="form-check-input" value="1" @if(isset($service->status) && $service->status == 1) checked @endif >Publish Now
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="service_availablity" class="form-check-input" value="0" @if(isset($service->status) && $service->status == 0) checked @endif>Save as Draft
                      </label>
                    </div>
                  </div>
                  <div class="text-right">
                 
                  <button type="submit" name="update_services" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button>
                  </div>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
