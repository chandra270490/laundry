@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add Items</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Location Detail
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('items.store')}}" method="post">
                 @csrf
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" required="" class="form-control" id="exampleInputEmail1" name="name" value="{{old('name')}}" placeholder="Enter Item Name" maxlength="30">
                      </div>
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Customer Price</label>
                        <input type="number" required="" class="form-control" id="exampleInputEmail1" name="customer_price" value="{{old('customer_price')}}" placeholder="Enter Item Customer Price">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                    <div class="col-md-6">
                    <label for="exampleSelectBorder">Choose Service</label>
                    <select name="services" required="" class="custom-select form-control">
                      <option value="">Please Select</option>
                      @foreach($services as $serviceKey=>$serviceVal)
                      <option value="{{old('services',$serviceVal->id)}}" >{{$serviceVal->name}}</option>
                      @endforeach
                    </select>
                    </div>
                    <div class="col-md-6">
                    <label for="exampleSelectBorder">Select Icons</label>
                    <input name="item_image" type="file" class="custom-select form-controlr">
                    </div>
                  </div>
                  </div>
                  <div class="text-right">
                  <button type="submit" name="add_item" class="btn btn-primary"><i class="fa fa-save"></i> Add Item</button>
                </div>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
