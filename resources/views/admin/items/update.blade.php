@extends('layouts.master')

@section('content')
@php $roleId = App\Model\RoleUser::where('id',Auth::user()->id)->pluck('role_id')->first(); @endphp
@php $roleName = App\Model\Role::where('id',$roleId)->pluck('name')->first();@endphp

 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Update Item</h1>
          </div><!-- /.col -->
         <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Item Detail
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('items.update',$item->id)}}" method="post">
                 @csrf
                 {{ method_field('PUT') }}
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" required="" class="form-control" id="exampleInputEmail1" name="name" value="{{old('name',$item->name)}}" placeholder="Enter Item Name">
                      </div>
                      @if($roleId == 2)
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Customer Price</label>
                        <input type="text" required="" class="form-control" id="exampleInputEmail1" name="customer_price" value="{{old('customer_price',$item->customer_price)}}" placeholder="Enter Item Customer Price">
                      </div>
                      @else
                      @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Facility Price</label>
                        <input type="text" required="" class="form-control" id="exampleInputEmail1" name="facility_price" value="{{old('facility_price',$item->facility_price)}}" placeholder="Enter Facility Price">
                      </div>
                      <div class="col-md-6">
                        <label for="exampleInputEmail1">Item In Count</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" name="item_no" value="{{old('item_no',$item->item_no)}}" placeholder="Enter Item Numbers">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                    <div class="col-md-6">
                    <label for="exampleSelectBorder">Choose Service</label>
                    <select name="services" required="" class="custom-select form-control-border">
                      <option value="">Please Select</option>
                      @foreach($services as $serviceKey=>$serviceVal)
                      <option value="{{old('services',$serviceVal->id)}}" @if(isset($item->service_id) && $item->service_id == $serviceVal->id) selected @endif >{{$serviceVal->name}}</option>
                      @endforeach
                    </select>
                    </div>
                    <div class="col-md-6">
                    <label for="exampleSelectBorder">Choose Status</label>
                    <select name="status" class="custom-select form-control-border">
                      <option value="">Please Select</option>           
                      <option value="{{old('status',1)}}" @if(isset($item->status) && $item->status == 1) selected @endif >yes</option>
                      <option value="{{old('status',0)}}" @if(isset($item->status) && $item->status == 0) selected @endif>No</option>
                    </select>
                    </div>
                  </div>
                  </div>
                  <button type="submit" name="update_item" class="btn btn-primary">Update Item</button>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
