@extends('layouts.master')

@section('content')
@php $roleId = App\Model\RoleUser::where('id',Auth::user()->id)->pluck('role_id')->first(); @endphp
@php $roleName = App\Model\Role::where('id',$roleId)->pluck('name')->first();@endphp

 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Items</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('items.create')}}" class="btn btn-success">Add Item +</a>
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
      @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Items</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if($roleId == 2) 
                <table id="items" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#SR No.</th>
                    <th>Item Name</th>
                    <th>Service</th>
                    <th>Customer Price</th>
                    <th>Facility Price</th>
                    <th>Icon</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @if(!empty($items) && count($items) > 1)
                    @foreach($items as $item)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{$item->name}}
                      </td>
                      <td>{{$item->service->name}}</td>
                      <td>{{$item->customer_price}}</td>
                      <td>{{$item->facility_price}}</td>
                      <td><img src="{{url("images/services/items/$item->item_image")}}" height="80" width="80" /></td>
                      <td>
                  
                      <a href="{{route('items.edit',$item->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--<a href="{{route('items.show',$item->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$item->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$item->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Item</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('items.destroy', $item->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @endforeach
                  @elseif(count($items) == 1)
                  <tr>
                  <td>{{ $i++ }}</td>
                      <td>{{$items[0]->name}}
                      </td>
                      <td>{{$items[0]->service->name}}</td>
                      <td>{{$items[0]->customer_price}}</td>
                      <td>{{$items[0]->facility_price}}</td>
                      <td><img src="{{url("images/services/items/$items[0]->item_image")}}" height="80" width="80" /></td>
                      <td>
                  
                      <a href="{{route('items.edit',$items[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--<a href="{{route('items.show',$items[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$items[0]->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$items[0]->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Item</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('items.destroy', $items[0]->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @endif
                  </tfoot>
                </table>
              @elseif($roleId == 1)
                <table id="items" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#SR No.</th>
                    <th>Item Name</th>
                    <th>Service</th>
                    <!--<th>Customer Price</th>-->
                    <th>Facility Price</th>
                    <th>Icon</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @if(!empty($items) && count($items) > 1)
                    @foreach($items as $item)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{$item->name}}
                      </td>
                      <td>{{$item->service->name}}</td>
                      <!--<td>{{$item->customer_price}}</td>-->
                      <td>{{$item->facility_price}}</td>
                      <td><img src="{{url("images/services/items/$item->item_image")}}" height="80" width="80" /></td>
                      <td>
                  
                      <a href="{{route('items.edit',$item->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--<a href="{{route('items.show',$item->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$item->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$item->id?>">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Delete Item</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                          <form action="{{ route('items.destroy', $item->id) }}" method="get">
                              <p>Are you sure you want to proceed ?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Yes</button>
                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                  @endforeach
                  @elseif(count($items) == 1)
                  <tr>
                  <td>{{ $i++ }}</td>
                      <td>{{$items[0]->name}}
                      </td>
                      <td>{{$items[0]->service->name}}</td>
                      <!--<td>{{$items[0]->customer_price}}</td>-->
                      <td>{{$items[0]->facility_price}}</td>
                      <td><img src="{{url("images/services/items/$items[0]->item_image")}}" height="80" width="80" /></td>
                      <td>
                  
                      <a href="{{route('items.edit',$items[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--<a href="{{route('items.show',$items[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>-->
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$items[0]->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$items[0]->id?>">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title">Delete Item</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div class="modal-body">
                          <form action="{{ route('items.destroy', $items[0]->id) }}" method="get">
                              <p>Are you sure you want to proceed ?</p>
                            </div>
                            <div class="modal-footer justify-content-between">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-primary">Yes</button>
                            </div>
                          </form>
                        </div>
                        <!-- /.modal-content -->
                      </div>
                      <!-- /.modal-dialog -->
                    </div>
                  @endif
                  </tfoot>
                </table>
              @else
              @endif
              </div>
              <!-- /.card-body -->
            </section>
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#items').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection

   
