@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">{{$page->test_user_name}}</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <!-- form start -->
              <form action="{{ route('testimonials.update',$page)}}" enctype="multipart/form-data" method="POST">
                @csrf
               {{ method_field('PUT') }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="test_user_name">Customer Name <span class="text-danger">*</span></label>
                    <input type="text" id="test_user_name" name="test_user_name" required="" class="form-control" placeholder="Enter Customer name" value="{{ old('test_user_name',$page->test_user_name ?? '')}}">
                  </div>
                  <div class="form-group">
                    <label for="test_user_role">Job Title <span class="text-danger">*</span></label>
                    <input type="text" id="test_user_role" name="test_user_role" required="" class="form-control" placeholder="Enter Job Title" value="{{ old('name',$page->test_user_role ?? '')}}">
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <label for="feature_image">Picture (Optional)</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                              <span class="input-group-text">
                                @if(!empty($page->feature_image))
                                  <img src="{{ asset('images/blogs/'.$page->feature_image)}}" width="150px" height="68px">
                                @endif    
                            </span>
                          </div>
                          <input type="file" id="feature_image" name="feature_image" class="form-control file-upload-control">
                        </div>
                  
                  </div>

                  <div class="col-md-8">
                  <div class="form-group">
                    <label for="media_link">Media Link (Optional)</label>
                    <input type="text" id="media_link" name="media_link" value="{{ old('media_link',$page->media_link ?? '')}}" placeholder="https://www.youtube.com/videolink" class="form-control">
                  </div>
                </div>
              
                 <div class="form-group">
                    <label for="codeMirrorDemo"> Customer Message</label>
                    <textarea id="summernote" name="description" class="form-control p-3">{{ old('description',$page->description ?? '')}}</textarea>
                  </div>
                  </div>
                  <div class="text-right">
                  <button type="submit" name="update_page" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button></div>
                </div>
              </form>
            </div>
          </div>
        </div></div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
 