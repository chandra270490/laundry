@extends('layouts.master')
<style type="text/css">
body {
  
    padding: 0px !important;
    margin: 0px !important;
  
}

.profile-nav, .profile-info{
    margin-top:30px;   
}

.profile-nav .user-heading {
    background: #fbc02d;
    color: #fff;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    padding: 30px;
    text-align: center;
}

.profile-nav .user-heading.round a  {
    border-radius: 50%;
    -webkit-border-radius: 50%;
    border: 10px solid rgba(255,255,255,0.3);
    display: inline-block;
}

.profile-nav .user-heading a img {
    width: 112px;
    height: 112px;
    border-radius: 50%;
    -webkit-border-radius: 50%;
}

.profile-nav .user-heading h1 {
    font-size: 22px;
    font-weight: 300;
    margin-bottom: 5px;
}

.profile-nav .user-heading p {
    font-size: 12px;
}

.profile-nav ul {
    margin-top: 1px;
}

.profile-nav ul > li {
    border-bottom: 1px solid #ebeae6;
    margin-top: 0;
    line-height: 30px;
}

.profile-nav ul > li:last-child {
    border-bottom: none;
}

.profile-nav ul > li > a {
    border-radius: 0;
    -webkit-border-radius: 0;
    color: #89817f;
    border-left: 5px solid #fff;
}

.profile-nav ul > li > a:hover, .profile-nav ul > li > a:focus, .profile-nav ul li.active  a {
    background: #f8f7f5 !important;
    border-left: 5px solid #fbc02d;
    color: #89817f !important;
}

.profile-nav ul > li:last-child > a:last-child {
    border-radius: 0 0 4px 4px;
    -webkit-border-radius: 0 0 4px 4px;
}

.profile-nav ul > li > a > i{
    font-size: 16px;
    padding-right: 10px;
    color: #bcb3aa;
}

.r-activity {
    margin: 6px 0 0;
    font-size: 12px;
}


.p-text-area, .p-text-area:focus {
    border: none;
    font-weight: 300;
    box-shadow: none;
    color: #c3c3c3;
    font-size: 16px;
}

.profile-info .panel-footer {
    background-color:#f8f7f5 ;
    border-top: 1px solid #e7ebee;
}

.profile-info .panel-footer ul li a {
    color: #7a7a7a;
}

.bio-graph-heading {
    background: #007bff;
    color: #fff;
    text-align: center;
    padding: 40px 110px;
    border-radius: 4px 4px 0 0;
    -webkit-border-radius: 4px 4px 0 0;
    font-size: 16px;
    font-weight: 300;
}


.bio-graph-info h1 {
    font-size: 22px;
    font-weight: 300;
    margin: 0 0 20px;
}

.bio-row {
    width: 33%;
    float: left;
    margin-bottom: 10px;
    padding:0 15px;
}

.bio-row p span {
    width: 100px;
    display: inline-block;
}

.bio-chart, .bio-desk {
    float: left;
}



</style>
@php  use App\User; @endphp
@section('content')

 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Show Page Details</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Show Page Details</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container-fluid">
<div class="row">

  <div class="profile-info col-md-12">
    
      <div class="panel">
          
          <div class="panel-body bio-graph-info">
              <h1>Page Details</h1>
              @php $createdBy = User::where('id',$page->created_by)->pluck('name')->first(); $imageName = isset($page->feature_image) && !empty($page->feature_image) ? $page->feature_image:'no_image.png'; @endphp
              <div class="row">
                  <div class="bio-row">
                      <p><span>Title: </span>  {{$page->title}}</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Created By: </span>  {{$createdBy}}</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Media Link: </span>  {{$page->media_link}}</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Canonical URL:</span> {{$page->media_link}}</p>
                  </div>
                 
                  <div class="bio-row">
                      <p><span>Feature Image:</span> <img height="100px" width="100px" src="{{asset('images/pages/'.$imageName  )}}"></p>
                  </div>
                  <div class="bio-row">
                      <p><span>Occupation: </span> UI Designer</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Email: </span> jsmith@flatlab.com</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Mobile: </span> (12) 03 4567890</p>
                  </div>
                  <div class="bio-row">
                      <p><span>Phone: </span> 88 (02) 123456</p>
                  </div>
              </div>
              <h1>Page Description</h1>
                <div class="bio-row">
                    {{$page->description}}
                </div>
          </div>
      </div>
    
  </div>
  
</div>
</div>
@endsection
