@extends('layouts.master')

@section('content')
 <!-- Content Header (Blog header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">{{ old('name',$blog->title ?? '')}}</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <!-- form start -->
              <form action="{{ route('blogs.update',$blog)}}" enctype="multipart/form-data" method="POST">
                @csrf
               {{ method_field('PUT') }}
                <div class="card-body">
                  
                  <div class="form-group">
                    <label for="title">Title <span class="text-danger">*</span></label>
                    <input type="text" id="title" name="title" value="{{ old('name',$blog->title ?? '')}}" required="" class="form-control" placeholder="Blog Title">
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                  <div class="form-group">
                    <label for="title">Tags <span class="text-danger">*</span></label>
                    <textarea id="title" name="tags" required="" rows="3" class="form-control" placeholder="Blog Tags">{{ old('tags',$blog->tags ?? '')}}</textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="title">Keywords <span class="text-danger">*</span></label>
                    <textarea id="title" name="keywords" required="" class="form-control" placeholder="Blog Keywords" rows="3"> {{ old('keywords',$blog->keywords ?? '')}}</textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <label>Picture</label>
                    
                  <div class="input-group">
                    <div class="input-group-addon">
                      <label class="input-group-text">
                        @if(!empty($blog->feature_image))
                          <img src="{{ asset('images/blogs/'.$blog->feature_image)}}" width="150px" height="68px">
                        @endif
                      </label>
                    </div>
                    <input type="file" id="feature_image" name="feature_image" class="form-control file-upload-control">
                  </div>
                  
                </div>
              </div>
                  <div class="form-group">
                    <label for="codeMirrorDemo"> Blog Description</label>
                    <textarea id="summernote" name="description" class="form-control p-3">{{ old('description',$blog->description ?? '')}}</textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleSelectBorder">Publish Mode</label>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" value="1" @if(isset($blog->status) && $blog->status == 1) checked="" @endif>Publish Now
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" value="0" @if(isset($blog->status) && $blog->status == 0) checked="" @endif>Save as Draft
                      </label>
                    </div>
                  </div>
                  
                  
                  <div class="row">
                    <div class="col-md-12 pr-2 text-right">
                  <button type="submit" name="update_blog" class="btn btn-primary"><i class="fa fa-save"></i> Save Changes</button></div>
                </div>
              </form></div>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
 