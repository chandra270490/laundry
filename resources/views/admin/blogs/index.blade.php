@extends('layouts.master')
@php  use App\User; @endphp
@section('content')
 <!-- Content Header (Blog header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Blogs</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('blogs.create')}}" class="btn btn-success">Add Blog +</a>
                <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
              
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- /.content -->
     <!-- Main content -->
     <section class="content">
     @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Blogs</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="blogs" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Tags</th>
                    <th>Published Date</th>
                    <th>Published Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $i=1; @endphp
                  @if(!empty($blogs) && count($blogs) > 1)
                  @foreach($blogs as $blog)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{substr($blog->title,0,25)}}...</td>
                    <td>{{substr($blog->tags,0,55)}}...</td>
                    <td>@php echo date('Y-m-d', strtotime($blog->created_at));@endphp</td>
                    <td>@php echo $blog->status==1?'<i class="fa fa-circle  text-green"></i> Active':'<i class="fa fa-circle  text-warning"></i> Draft'; @endphp</td>
                    <td>
                      <a href="{{route('blogs.edit',$blog->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <a href="{{route('blogs.show',$blog->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$blog->id?>" href="{{route('blogs.destroy',$blog->id)}}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <div class="modal fade" id="modal-default<?=$blog->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Blog</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('blogs.destroy', $blog->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              </div>
              <!-- /.modal -->
                  @endforeach
                  @elseif(count($blogs) == 1)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{substr($blogs[0]->title,0,10)}}</td>
                    <td>{{$blogs[0]->tags}}
                    </td>
                    <td>@php echo date('Y-m-d', strtotime($blogs[0]->created_at));@endphp</td>
                    <td>@php echo $blogs[0]->status==1?'<i class="fa fa-circle  text-green"></i> Active':'<i class="fa fa-circle  text-warning"></i> Draft'; @endphp</td>
                    <td>
                      <a href="{{route('blogs.edit',$blogs[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <!--a href="{{route('blogs.show',$blogs[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a-->
                      <a role="button" data-toggle="modal" data-target="#modal-default"  class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>

                <div class="modal fade" id="modal-default">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Blog</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('blogs.destroy', $blogs[0]->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
              </div>
              <!-- /.modal -->
                  @else
                  <tr>
                    <td colspan="5">No Data availabale </td>
                  </tr>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#blogs').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection


   
