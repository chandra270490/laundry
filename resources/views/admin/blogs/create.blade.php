@extends('layouts.master')

@section('content')
 <!-- Content Header (blog header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            
            <h3 class="m-0">Add New Blog</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <!-- form start -->
              <form action="{{ route('blogs.store')}}" enctype="multipart/form-data" method="POST">
              @csrf
                <div class="card-body">
                  
                  <div class="form-group">
                    <label for="title">Title <span class="text-danger">*</span></label>
                    <input type="text" id="title" name="title" required="" class="form-control" placeholder="Blog Title">
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                  <div class="form-group">
                    <label for="title">Tags <span class="text-danger">*</span></label>
                    <textarea id="title" name="tags" required="" class="form-control" placeholder="Blog Tags"></textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="title">Keywords <span class="text-danger">*</span></label>
                    <textarea id="title" name="keywords" required="" class="form-control" placeholder="Blog Keywords"></textarea>
                  </div>
                </div>
                <div class="col-md-4">
                  
                  <div class="form-group">
                    <label for="feature_image">Picture (Optional)</label>
                    <input type="file" id="feature_image" name="feature_image" class="form-control">
                  </div>
                </div>
              </div>
                  
                  <div class="form-group">
                    <label for="codeMirrorDemo"> Blog Description</label>
                    <textarea id="summernote" name="description"  class="form-control p-3"></textarea>
                  </div>
                  <div class="form-group">
                    <label for="exampleSelectBorder">Publish Mode</label>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" value="1" checked="">Publish Now
                      </label>
                    </div>
                    <div class="form-check-inline">
                      <label class="form-check-label">
                        <input type="radio" name="status" class="form-check-input" value="0">Save as Draft
                      </label>
                    </div>
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 p-4 text-right">
                  <button type="submit" name="add_blog" class="btn btn-primary"><i class="fa fa-save"></i> Add Blog</button>
                </div>
              </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
 