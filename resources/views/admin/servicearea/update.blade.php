@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Update Location</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Location Detail
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form action="{{route('servicearea.update',$location)}}" enctype="multipart/form-data" method="post">
                @csrf
                {{ method_field('PUT') }}
                  <div class="form-group">
                    <label for="exampleInputEmail1">Location Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{ old('name',$location->name)}}" placeholder="Enter Location name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">City Postcode</label>
                    <input type="text" name="postal_code_city" value="{{ old('postal_code_city',$location->postal_code_city)}}" placeholder="Enter City Postcode" class="form-control" id="exampleInputEmail1"  >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">State Postcode</label>
                    <input type="text" name="postal_code_state" value="{{ old('postal_code_state',$location->postal_code_state)}}" class="form-control" id="exampleInputEmail1" placeholder="Enter State Postcode">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <textarea  class="form-control" name="address" value="" placeholder="Enter Address">{{ old('address',$location->address)}}</textarea>
                  </div>
        
                  <h4>Save Type</h4>
                  <div class="form-group">
                    <label for="exampleSelectBorder">Choose options</label>
                    <select name="status" class="custom-select form-control-border" id="exampleSelectBorder">
                      <option value="">Please Select</option>
                      <option value="1" @if(isset($location->status) && $location->status == 1) selected @endif>Publish Now</option>
                      <option value="0" @if(isset($location->status) && $location->status == 0) selected @endif>Save as Draft</option>
                    </select>
                  </div>
                 
                  <button type="submit" name="add_location" class="btn btn-primary">Update Location</button>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
