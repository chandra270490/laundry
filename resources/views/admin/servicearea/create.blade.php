@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Add Location</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="card card-primary card-outline">
            <div class="card-header">
              <h3 class="card-title">
                <i class="fas fa-edit"></i>
                  Location Detail
              </h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12 col-sm-3">
                <form enctype="multipart/form-data" action="{{route('servicearea.store')}}" method="post">
                 @csrf
                  <div class="form-group">
                    <label for="exampleInputEmail1">Location Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="" placeholder="Enter Location Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">City Postcode</label>
                    <input type="text" name="postal_code_city" value="" class="form-control" id="exampleInputEmail1" placeholder="Enter City Postcode">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">State Postcode</label>
                    <input type="text" name="postal_code_state" value="" class="form-control" id="exampleInputEmail1" placeholder="Enter State Postcode">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Address</label>
                    <textarea  class="form-control" name="address" value="" placeholder="Enter Address"></textarea>
                  </div>
        
                  <h4>Save Type</h4>
                  <div class="form-group">
                    <label for="exampleSelectBorder">Choose options</label>
                    <select name="status" class="custom-select form-control-border" id="exampleSelectBorder">
                      <option value="">Please Select</option>
                      <option value="1" selected="">Publish Now</option>
                      <option value="0" >Save as Draft</option>
                    </select>
                  </div>
                 
                  <button type="submit" name="add_location" class="btn btn-primary">Add Location</button>
                </form>
                </div>
              </div>
            </div>
        </div>
      </div>
    </section>
@endsection
