@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Service Location</h1>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <a href="{{ route('servicearea.create')}}" class="btn btn-success">Add Service Area +</a>
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

   
     <!-- Main content -->
     <section class="content">
      <div class="container-fluid">
      @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
      @endif
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Service Locations</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="servicearea" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#.</th>
                    <th>Location</th>
                    <th>State's Post Code</th>
                    <th>City's Post Code</th>
                    <th>Address</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    @php  $i=1; @endphp
                    @if(!empty($locations) && count($locations) > 1)
                    @foreach($locations as $location)
                    <tr>
                      <td>{{ $i++ }}</td>
                      <td>{{$location->name}}
                      </td>
                      <td>{{$location->postal_code_state}}</td>
                      <td>{{$location->postal_code_city}}</td>
                      <td>{{$location->address}}</td>
                      <td>@if($location->status == 1) Enabled @elseif($location->status == 0) Disabled @endif</td>
                      <td>
                  
                      <a href="{{route('servicearea.edit',$location->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <a href="{{route('servicearea.show',$location->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$location->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$location->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Location</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('servicearea.destroy', $location->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @endforeach
                  @elseif(count($locations) == 1)
                  <tr>
                      <td>1</td>
                      <td>{{$locations[0]->name}}
                      </td>
                      <td>{{$locations[0]->postal_code_state}}</td>
                      <td>{{$locations[0]->postal_code_city}}</td>
                      <td>{{$locations[0]->address}}</td>
                      <td>@if($locations[0]->status == 1) Enabled @elseif($locations[0]->status == 0) Disabled @endif</td>
                      <td>
                  
                      <a href="{{route('servicearea.edit',$locations[0]->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-pencil-alt"></i></a>
                      <a href="{{route('servicearea.show',$locations[0]->id)}}" class="btn btn-xs btn-info"><i class="fa fa-eye"></i></a>
                      <a role="button" data-toggle="modal" data-target="#modal-default<?=$locations[0]->id?>" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <div class="modal fade" id="modal-default<?=$locations[0]->id?>">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title">Delete Location</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="{{ route('servicearea.destroy', $locations[0]->id) }}" method="get">
                          <p>Are you sure you want to proceed ?</p>
                        </div>
                        <div class="modal-footer justify-content-between">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Yes</button>
                        </div>
                      </form>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>
                  @else
                  <tr>
                  <td colspan="5">No Servicearea Available</td>
                  </tr>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </section>
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#servicearea').DataTable({
          "paging": true,
          "pageLength": 10,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
@endsection

   
