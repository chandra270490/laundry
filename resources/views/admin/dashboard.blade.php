@extends('layouts.master')
@php use App\User;  @endphp
@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h3>{{$servicecount}}</h3>

                  <p>Total Services</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h3>{{$usercount}}</h3>

                  <p>Total Users</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-warning">
                <div class="inner">
                  <!--- CNS --->
                  <h3>{{$ordercount}}</h3>
                  <p>Today orders</p>
                  <!--- CNS --->
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <!--- CNS --->
                  <h3>{{$ordercount}}</h3>
                  <p>Total orders</p>
                  <!--- CNS --->
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
          
          <div class="row">
            <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Latest 5 added Services</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="services" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>      
                    <th>Price</th>
                    <th>Description(s)</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $i=1; @endphp
                  @if(!empty($services) && count($services) > 1)
                  @foreach($services as $service)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$service->title}}
                    </td>
                    <td>{{$service->price}}</td>
                    <td>{{htmlentities(strip_tags($service->description))}}</td>
                    
                  </tr>
                  @endforeach
                  @elseif(count($services) == 1)
                  <tr>
                    <td>{{ $i }}</td>
                    <td>{{$services[0]->title}}
                    </td>
                    <td>{{$services[0]->price}}</td>
                    <td>{{htmlentities(strip_tags($services[0]->description))}}</td>
                  </tr>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          </div>

          <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Latest 5 added Pages</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="pages" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Published By</th>
                    <th>Description(s)</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php $i=1; @endphp
                  @if(!empty($pages) && count($pages) > 1)
                  @foreach($pages as $page)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$page->title}}
                  @php $createdBy = User::where('id',$page->created_by)->pluck('name')->first(); $imageName = isset($page->feature_image) && !empty($page->feature_image) ? $page->feature_image:'no_image.png'; @endphp
                  <td>{{$createdBy}}</td>
                    <td>{{substr($page->description, 0, 100)}}...</td>
                  </tr>
                 
                  @endforeach
                  @elseif(count($pages) == 1)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{$pages[0]->title}}
                    </td>
                    @php $createdBy = User::where('id',$pages[0]->created_by)->pluck('name')->first(); @endphp
                    <td>{{$createdBy}}</td>
                    <td>{{substr($pages[0]->description, 0, 100)}}...</td>
                   
                  </tr>
                 
                  @else
                  <tr>
                    <td colspan="5">No Data availabale </td>
                  </tr>
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
@section('footer-scripts')
  <script>
      $(function () {
        $('#pages,#services').DataTable({
          "paging": true,
          "pageLength": 3,
          "lengthChange": false,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": false,
          "responsive": true,
        });
      });
  </script>
  @endsection

   
