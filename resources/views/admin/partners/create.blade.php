@extends('layouts.master')

@section('content')
 <!-- Content Header (Page header) -->
 <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h3 class="m-0">Add New Page</h3>
          </div><!-- /.col -->
          <div class="col-sm-6 text-right">
            <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Go Back</button>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

 

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
         <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card">
              <!-- form start -->
              <form action="{{ route('pages.store')}}" enctype="multipart/form-data" method="POST">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Page Name <span class="text-danger">*</span></label>
                    <input type="text" id="name" name="name" required="" class="form-control" placeholder="Enter Page name">
                  </div>
                  <div class="form-group">
                    <label for="title">Title <span class="text-danger">*</span></label>
                    <input type="text" id="title" name="title" required="" class="form-control" placeholder="Page Title">
                  </div>
                  <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="feature_image">Picture (Optional)</label>
                        <input type="file" id="feature_image" name="feature_image" class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-8">
                  <div class="form-group">
                    <label for="media_link">Media Link (Optional)</label>
                    <input type="text" id="media_link" name="media_link" placeholder="https://www.youtube.com/videolink" class="form-control">
                  </div>
                </div>
              </div>
                  <div class="form-group">
                    <div class="row">
                    <div class="col-md-4">
                      <label>Add in header Navbar</label>
                      <input type="radio" id="add_top_navbar_yes" name="add_top_navbar" value="0" /> 
                      <label for="add_top_navbar_yes">No </label>
                      <input type="radio" checked="" id="add_top_navbar_no" name="add_top_navbar" value="1" /> <label for="add_top_navbar_no">Yes </label>
                    </div>
                    <div class="col-md-4">
                      <label>Add in Footer Navbar</label>
                      <input type="radio" id="add_footer_navbar_no" name="add_footer_navbar" value="0" /> 
                      <label for="add_footer_navbar_no">No </label>
                      <input type="radio" checked="" id="add_footer_navbar_yes" name="add_footer_navbar" value="1" /> <label for="add_footer_navbar_yes">Yes </label>
                    </div>
                  </div>
              </div>
                  <div class="form-group">
                    <label for="codeMirrorDemo"> Page Description</label>
                    <textarea id="summernote" name="description"  class="form-control p-3"></textarea>
                  </div>
                  <div class="text-right">
                  <button type="submit" name="add_page" class="btn btn-primary"><i class="fa fa-save"></i> Add Page</button></div>
                </div></form>
              </form>
            </div>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection
 