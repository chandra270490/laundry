<?php 
use App\Model\RoleUser;
use App\Model\Role;
$id = Auth::user()->id;
$roleusers = RoleUser::where('user_id',$id)->select('role_id')->get();

?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->
      <li class="nav-item">
        <!--a class="nav-link" data-widget="navbar-search" href="#" role="button">
          <i class="fas fa-search"></i>
        </a-->
        <!--div class="navbar-search-block">
          <form class="form-inline">
            <div class="input-group input-group-sm">
              <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                </button>
                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                  <i class="fas fa-times"></i>
                </button>
              </div>
            </div>
          </form>
        </div-->
      </li>
      
      <!--- CNS --->
      @php 
      $notifications= DB::table('notifications')->get()->where('read_at', NULL); 
      $notify_data = $notifications;
      $cnt_notify = count($notify_data);
      /*
      foreach($notify_data as $data1){
        echo $data1->data;
      }
      */
      @endphp
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link  dropdown-toggle" href="#" data-bs-toggle="dropdown">
          <i class="far fa-bell"></i>
          <span class="badge badge-danger navbar-badge">{{ $cnt_notify > 0?$cnt_notify:0 }}</span>
        </a> 
        <ul class="dropdown-menu">
            @foreach($notify_data as $data1)
              @php
                $items = json_decode($data1->data);
                $order_id =  $items->{'id'};
                $order_no = $items->{'order_no'};
              @endphp
              <li value="{{ $data1->id }}" onClick="hideNotifyFun('{{$data1->id}}')">
                <a href="{{route('orders.edit', $order_id)}}" class="dropdown-item">
                  New Order #{{ $order_no }} 
                </a>
              </li>
            @endforeach
          </ul>
      </li>
      <!--- CNS --->
  
      <li class="nav-item dropdown">
        <form method="POST" action="{{route('setrole')}}">
          @csrf
          <a class="nav-link  dropdown-toggle" href="#" data-bs-toggle="dropdown">  User Section  </a>
            <ul class="dropdown-menu">
            <li><a href="{{route('showprofile',auth()->user()->id)}}" class="dropdown-item">{{ auth()->user()->name}}</a></li>
            <li><a class="dropdown-item" href="{{url('admin/logout')}}">Logout </a></li>
            @if(count($roleusers) > 1)
            @foreach($roleusers as $role => $roleval)
            @php $roleName = Role::where('id',$roleval->role_id)->pluck('name')->first();  @endphp
            <li> <input type="radio" @if(isset($_SESSION['roledashboard']) && $_SESSION['roledashboard'] == $roleval->role_id) checked @endif onChange="$(this).closest('form').submit();" class="form-radio-input" name="roles" value="{{$roleval->role_id}}">
                          <label for="customCheckbox1" class="form-radio-label">{{$roleName}}</label></li>
            @endforeach
            @endif
          </ul>
        </form>
        </li>
    
    </ul>
  </nav>
<style type="text/css">
@media all and (min-width: 992px) {
	.navbar .nav-item .dropdown-menu{ display: none; }
	.navbar .nav-item:hover .nav-link{   }
	.navbar .nav-item:hover .dropdown-menu{ display: block; }
	.navbar .nav-item .dropdown-menu{ margin-top:0; }
}
.navbar-badge {font-size: 11px; font-weight: 600; padding: 4px 7px; position: absolute; right: 20px; top: -1px;border-radius: 50%;}
.nav-item .dropdown-menu {margin: .125rem -23px 0px !important;}
</style>
  <!-- /.navbar -->