
    @php 
    $segments = request()->segments();
    $urlSegment  = end($segments);
    @endphp

    @php $roleId = App\Model\RoleUser::where('id',Auth::user()->id)->pluck('role_id')->first(); @endphp
    @php $roleName = App\Model\Role::where('id',$roleId)->pluck('name')->first();@endphp
     
  
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{ asset('images/logo/logo.jpeg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Laundry</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="{{ route('dashboard') }}" class="nav-link @if($urlSegment == 'dashboard') active @endif">
              <i class="nav-icon fas fa-tachometer-alt text-info"></i>
              <p>Dashboard</p>
            </a>
          </li>
         <!-- @if(!empty($_SESSION['moduleArray']))
          @foreach($_SESSION['moduleArray'] as $moduleKey=>$moduleVal)
          @if(isset($moduleVal['route']))
          <li class="nav-item">
            <a href="{{ route($moduleVal['route']) }}" class="nav-link @if($urlSegment == $moduleVal['url_segment']) active @endif">
              <i class="nav-icon fas {{$moduleVal['class']}} text-info"></i>
              <p>{{ $moduleVal['name'] }}</p>
            </a>
          </li>
          @endif
          @endforeach
          @endif -->

           <li class="nav-item">
            <a href="{{ route('items.index') }}" class="nav-link @if($urlSegment == 'items') active @endif">
              <i class="nav-icon far fa-folder text-info"></i>
              <p>Items</p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('orders.index') }}" class="nav-link @if($urlSegment == 'orders') active @endif">
              <i class="nav-icon fas fa-shopping-cart text-info"></i>
              <p>Orders</p>
            </a>
          </li>
          @if($roleId == 2)
          <li class="nav-item">
            <a href="{{ route('pages.index') }}" class="nav-link @if($urlSegment == 'pages') active @endif">
              <i class="nav-icon far fa-file text-info"></i>
              <p>Pages</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('blogs.index') }}" class="nav-link @if($urlSegment == 'blogs') active @endif">
              <i class="nav-icon far fa-file text-info"></i>
              <p>Blogs</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('services.index') }}" class="nav-link @if($urlSegment == 'services') active @endif">
              <i class="nav-icon fas fa-map-marker text-info"></i>
              <p>Services</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('servicearea.index') }}" class="nav-link @if($urlSegment == 'servicearea') active @endif">
              <i class="nav-icon fas fa-wrench text-info"></i>
              <p>Service Area</p>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a href="{{ route('assignroletomodule') }}" class="nav-link @if($urlSegment == 'modules') active @endif">
              <i class="nav-icon far fa-user text-info"></i>
              <p>Manage Roles</p>
            </a>
          </li> -->
          <li class="nav-item">
            <a href="{{ route('users.index') }}" class="nav-link @if($urlSegment == 'users') active @endif">
              <i class="nav-icon far fa-user text-info"></i>
              <p>Users</p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ route('testimonials.index') }}" class="nav-link @if($urlSegment == 'testimonials') active @endif">
              <i class="nav-icon fas fa-users text-info"></i>
              <p>Testimonials</p>
            </a>
          </li>
         

          <li class="nav-item">
            <a href="{{ route('settings.index') }}" class="nav-link @if($urlSegment == 'settings') active @endif">
              <i class="nav-icon fas fa-cog text-info"></i>
              <p>Settings</p>
            </a>
          </li>
          @endif
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>

