@extends('frontend.master')
@section('meta')
	<title>Nearest Laundry - Cleaning Services</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection
<style>
    label.error {
         color: #dc3545;
         font-size: 14px;
    }
</style>
@section('body')
    <!-- BANNER -->
<div class="section banner-page">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="title-page">Register</div>
<ol class="breadcrumb">
<li><a href="{{ url('/')}}">Home</a></li>
<li class="active">Register</li>
</ol>
</div>
</div>
</div>
</div>
<!-- Register -->

<div class="section pad bg1 ">
   <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-offset-3 col-md-6 crd">
                @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                    @endif
                <form id="regForm" method="post" action="{{route('apisignup')}}" onsubmit="return validateForm(this);"> 
                    <div class="form-row">
                    <div class="form-group col-md-offset-1 col-md-10">
                    <label for="first_name">First Name<span class="text-danger">*</span></label>
                    <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="inputEmail4" placeholder="First Name" required="" oninput="return validateRequired(this);" onchange="return validateRequired(this);">
                    </div>
                    <div class="form-group col-md-offset-1 col-md-10">
                    <label for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" id="inputEmail4" placeholder="Last Name" value="{{old('last_name')}}" oninput="return validateRequired(this);" onchange="return validateRequired(this);">
                    </div>
                    </div>
                <div class="form-row">
                <div class="form-group col-md-offset-1 col-md-10">
                <label for="email">Email <span class="text-danger">*</span></label>
                <input required  type="text" value="{{old('email')}}" name="email" class="form-control" id="email" placeholder="Email " autocomplete="false" required pattern="^.{0,64}$" title="Enter an email address with a maximum of 64 characters." oninput="validateEmail(this);" onchange="validateEmail(this);">
                </div>
                <div class="form-group col-md-offset-1 col-md-10">
                <label for="password">Password<span class="text-danger">*</span></label>
                <input  type="password" name="password" class="form-control" id="password" placeholder="Password" autocomplete="false" required pattern="^((?=.*[0-9])(?=.*[a-zA-Z])).{6,12}$" title="Enter a password between 6 and 12 characters, containing at least one letter and one number." oninput="validatePassword(this);" onchange="validatePassword(this);">
                </div>
                </div>

                <div class="form-row">
                <div class="form-group col-md-offset-1 col-md-10">
                <label for="password_confirmation" title="Passwords must be between 6 and 12 characters long, containing at least one letter and one number. Please note, passwords are case sensitive.">Confirm Password <span class="text-danger">*</span></label>
                <input required type="password" name="password_confirmation" class="form-control" id="passwordConfirm" placeholder="Confirm Password ">
                
                </div>

                </div>

                <div class="form-row">
                <div class="form-group col-md-offset-1 col-md-10">
                    <label for="phone">Phone </label>
                    <input type="text" name="phone" class="form-control" value="{{old('phone')}}" id="myform_phone" placeholder="Phone " maxlength="10" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" required>
                </div>
                </div>
                <div class="form-group col-md-offset-1 col-md-10 text-left"><a href="{{route('show-login')}}" class="btn-link"><p>Already have an account? Sign in</p></a></div>
                <div class="form-row">
                    <div class="form-group col-md-offset-1 col-md-10"><button type="submit" class="btn butn " title="Register Now">Register Now </button>
                </div>
                </div>
                </form>
</div>
</div>
</div>
</div>
<!-- CTA -->
<div class="section pad cta-bgc">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="cta-content">
<h3 class="cta-title-3">Ready to book your cleaning?</h3>
</div>
<div class="cta-action"><a href="contact.html" class="btn btn-white" title="Learn More">Contact Us</a></div>
</div>

 </div>
</div>
</div>

@endsection
@section('frontend_scripts')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script>
        $(document).ready(function() {
            $("#regForm").validate({
                rules: {
                    first_name: "required",
                    email: "required",
                    phone: "required",
                    password: "required",
                    password_confirmation: "required",
                   
                },
                messages: {
                    first_name: "First name is required",
                    email: "Email is required",
                    phone: "Phone number is required",
                    password: "Password is required",
                    password_confirmation: "Confirm password is required",
                  
                }
            });
        });
    </script>
    <script>
    /*
Email regex pattern, same as the HTML5 form validation.
https://www.w3.org/TR/html-markup/datatypes.html#form.data.emailaddress
*/
var emailPattern = new RegExp('^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$');

 

 

var email = document.forms['userRegForm'].email;
var password = document.forms['userRegForm'].password;
var passwordConfirm = document.forms[0].passwordConfirm;
var firstName = document.forms['userRegForm'].firstName;
var lastName = document.forms['userRegForm'].lastName;

 

// Email length regex pattern.
var emailLengthPattern = new RegExp($(email).attr('pattern'));

 

// Password regex pattern.
var passwordPattern = new RegExp($(password).attr('pattern'));

 

// Initialize popover for all required inputs
$('input[required]').popover({
    placement: 'bottom',
    container: 'body',
    trigger: 'manual',
    selector: 'true',
    content: function() {
        return $(this).attr('title');
    },
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><div class="popover-content"></div></div>'
}).focus(function() {
    $(this).popover('hide');
});

 

function validateForm(form) {
    
    // Email validation.
  if (!emailPattern.test(email.value) || !emailLengthPattern.test(email.value)) {
      $(email).popover('show');
        return false;
    } else {
        $(email).addClass('valid')
    }
    
    // Password validation.
    if (!passwordPattern.test(password.value)) {
        $(password).popover('show');
        return false;
    }
    
    // Password confirm validation.
    if (!passwordPattern.test(passwordConfirm.value)) {
        $(passwordConfirm).popover('show');
        return false;
    }
    
    // Password match validation.
    if (password.value !== passwordConfirm.value) {
        $(passwordConfirm).popover('show');
        return false;
    }
    
    // First name validation.
    if (firstName.value.length === 0) {
        $(firstName).popover('show');
        return false;
    }
    
    // Last name validation.
    if (lastName.value.length === 0) {
        $(lastName).popover('show');
        return false;
    }
    
    // No validation errors.
    alert('Submitted successfully');
    
    // In this demo, prevent the form from submitting.
    return false;
}

 

function validateEmail(input) {
    if (emailPattern.test(input.value) && emailLengthPattern.test(input.value)) {
        $(input).addClass('valid')
    } else {
        $(input).removeClass('valid');
    }
}

 

/*
Sets a custom validation to require both password fields to match each other
*/
function validatePassword(input) {
    
    if (input.setCustomValidity) {
        input.setCustomValidity('');
        
        if (input.validity && !input.validity.valid) {
            input.setCustomValidity(input.title);
        }
    }
    
    if (passwordConfirm.setCustomValidity) {
        if (password.value !== passwordConfirm.value) {
                passwordConfirm.setCustomValidity(passwordConfirm.title);
        } else {
            passwordConfirm.setCustomValidity('');
        }
    } else {

 

        if (passwordPattern.test(input.value)) {
            $(input).addClass('valid');

 

            if (password.value === passwordConfirm.value) {
                $(passwordConfirm).addClass('valid');
            } else {
                $(passwordConfirm).removeClass('valid');
            }
        } else {
            $(input).removeClass('valid');
        }
    }
}

 

function validateRequired(input) {
    
    if (input.setCustomValidity) {
        input.setCustomValidity('');
        
        if (input.validity && !input.validity.valid) {
            input.setCustomValidity(input.title);
        }
    }
    
    if (input.value.length > 0) {
      $(input).addClass('valid');
  } else {
        $(input).removeClass('valid');
    }
}

 

// Phone 

 

var phone_input = document.getElementById("myform_phone");

 

phone_input.addEventListener('input', () => {
  phone_input.setCustomValidity('');
  phone_input.checkValidity();
});

 

phone_input.addEventListener('invalid', () => {
  if(phone_input.value === '') {
    phone_input.setCustomValidity('Enter phone number!');
  } else {
    phone_input.setCustomValidity('Enter phone number in this format: 123-456-7890');
  }
});

 

    </script>
    @endsection