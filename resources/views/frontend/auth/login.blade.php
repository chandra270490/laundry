@extends('frontend.master')
@section('meta')
	<title>Nearest Laundry - Cleaning Services</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection
<style>
    label.error {
         color: #dc3545;
         font-size: 14px;
    }
</style>
@section('body')
<!-- BANNER -->
<div class="section banner-page">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="title-page">Login</div>
<ol class="breadcrumb">
<li><a href="{{url('/')}}">Home</a></li>
<li class="active">Login</li>
</ol>
</div>
</div>
</div>
</div>
<!-- ABOUT FEATURE -->
<div class="section pad bg1">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-offset-3 col-md-6 crd">
<div class="">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form id="loginForm" method="post" action="{{route('apilogin')}}">
<div class="form-row">
<div class="form-group col-md-offset-1 col-md-10">
<label for="email">Email <span class="text-danger">*</span></label>
<input type="email" class="form-control" id="email" name="email" placeholder="Email ">
</div>
<div class="form-group col-md-offset-1 col-md-10">
<label for="password">Password<span class="text-danger">*</span></label>
<input type="password" class="form-control" id="password" name="password" placeholder="Password">
</div>
</div>
<div class="form-group col-md-offset-1 col-md-10 text-center">
    <!-- <p><strong>Forgot your password?</strong></p> -->
<p>Don't have an account? <a href="{{route('show-register')}}">Sign Up</a></p></div>
<div class="form-group col-md-offset-1 col-md-10 text-center">
<button type="submit" class="btn butn">Login</button>
<!-- <a href="#" class="btn btn-primary" title="Register Now">Login</a> -->
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<!-- CTA -->
<div class="section pad cta-bgc">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="cta-content">
<h3 class="cta-title-3">Ready to book your cleaning?</h3>
</div>
<div class="cta-action"><a href="contact.html" class="btn btn-white" title="Learn More">Contact Us</a></div>
</div>

 </div>
</div>
</div>
@endsection
@section('frontend_scripts')
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
<script>
        $(document).ready(function() {
            $("#loginForm").validate({
                rules: {
                   
                    email: "required",
                    password: "required",
                  
                   
                },
                messages: {
                   
                    email: "Email is required",
                    password: "Password is required",
                  
                }
            });
        });
    </script>
    @endsection