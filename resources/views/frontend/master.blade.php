@php 
	$setting = json_decode(json_encode($setting));

	$dataPages=json_decode(json_encode($topNavPages)); 
	$topNavPages=$dataPages->response;
	
@endphp
<!DOCTYPE html>
<html lang="zxx">
  <head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @yield('meta')
    <meta name="author" content=""> 
	@include('frontend.inc.style')
	@yield('frontend-styles')
</head>

<body>

	<!-- Load page -->
	<!-- <div class="animationload">
		<div class="loader"></div>
	</div> -->
	
	<!-- HEADER -->
    @include('frontend.inc.navbar')
 
	@yield('body')
	 
	<!-- FOOTER SECTION -->
	@include('frontend.inc.footer')
	@include('frontend.inc.script')	
	@yield('frontend_scripts')
</body>
</html>
