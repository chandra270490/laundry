<div class="col-sm-3 col-md-3">
					<div class="widget cta">
						<h4>Ready to book your cleaning?</h4>
						<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
						<a href="{{route('order')}}" class="btn btn-primary btn-sidebar"><span class="fa fa-calendar-o"></span> Book Now</a>
					</div>
					<div class="widget contact-info-sidebar">
						<div class="widget-title">
							Contact Info
						</div>
						<div class="row">
							<div class="col-md-12">
								<a href="https://api.whatsapp.com/send/?phone={{$sidebarSetting['social_whatsapp_link']}}">
									<span class="fab fa-whatsapp blog-contact-con whatsapp-bg"></span> <span class="whatsapp-text">Connect with whatsapp</span>
								</a>
							</div>
						</div>
						<br/>	
						<hr/>	
						<div class="row">
							<div class="col-md-12">
								<a href="">
									<span class="fab fa-facebook-messenger blog-contact-con messanger-bg"></span> <span class="whatsapp-text">Connect with messanger</span>
								</a>
							</div>
						</div>	
					</div> 

				</div>