<!-- ==============================================
	Favicons
	=============================================== -->
	<link rel="shortcut icon" href="{{asset('frontend/images/favicon.ico')}}"><!-- 
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png"> -->
	
	<!-- ==============================================
	CSS VENDOR
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/bootstrap.min.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/simple-line-icons.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/owl.theme.default.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/vendor/magnific-popup.css')}}">	
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
	<!-- ==============================================
	Custom Stylesheet
	=============================================== -->
	<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/style.css')}}" />	
	
    <script type="text/javascript" src="{{asset('frontend/js/vendor/modernizr.min.js')}}"></script>
   <style>
/* SLIDER OFFER Start */
.blog_item {margin-bottom: 30px;position: relative;}

.slideroffer .owl-theme .owl-controls .owl-nav [class*=owl-] {color: #fff;
    font-size: 21px;
    margin: 5px;
    padding: 10px 10px;
    background: #3d79aa !important;
    display: inline-block;
    cursor: pointer;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    text-transform: uppercase;
}

.card .card-body p.tt-value {font-size: 28px; color: #52b765; text-align:center}

.card {background: #fff; box-shadow: 0 6px 10px rgba(0, 0, 0, .08), 0 0 6px rgba(0, 0, 0, .05); transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12); border: 0;
    border-radius: 1rem}

.card-img,.card-img-top {border-top-left-radius: calc(1rem - 1px); border-top-right-radius: calc(1rem - 1px)}

.card p {font-weight: 500;font-size: 18px;text-align: center;}

.card .card-title { overflow: hidden; font-weight: 600; font-size: 21px; text-align: center;}

.card-img-top { width: 100%; max-height: 180px; object-fit: contain; padding:20px}

/*.card:hover {transform: scale(1.05); box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06)}*/

.card p{margin-bottom:10px !important;}

@media (max-width: 768px) {.card-img-top {max-height: 250px}}
.card .btn{color:#fff}
.btn-warning { background: none #f7810a; color: #ffffff; fill: #ffffff; border: none;text-decoration: none; outline: 0;    box-shadow: -1px 6px 19px rgba(247, 129, 10, 0.25);  border-radius: 100px}

.btn-warning:hover {background: none #ff962b; color: #ffffff; box-shadow: -1px 6px 13px rgba(255, 150, 43, 0.35)}

/* SLIDER OFFER END */
</style>