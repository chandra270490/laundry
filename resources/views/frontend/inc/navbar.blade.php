<div class="header">
    	<!-- TOPBAR -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="col-sm-3 col-md-4">
						<div class="topbar-left">
							<div class="welcome-text">
								<a href="https://api.whatsapp.com/send/?phone={{$setting->social_whatsapp_link}}" style="color:#fff" title="Click here to chat on whatswapp" target="_blank"><i class="fab fa-whatsapp fa-lg"></i> Connect With WhatsApp</a>
							</div>
						</div>
					</div>
					<div class="col-sm-9 col-md-8">
						<div class="topbar-right">
							<ul class="topbar-menu">
							<li> @if(session()->has('token')) <a class="register_now" href="{{route('customerdashboard')}}" style="color:#fff"><i class="fas fa-dashboard"></i> Dashboard  </a> | 
								<a class="register_now" href="{{route('logoutuser')}}" style="color:#fff"><i class="fas fa-sign-out-alt"></i> Logout  </a>
								
								@else
								<a class="register_now" href="{{route('show-register')}}" style="color:#fff"><i class="fas fa-user-tie"></i> Register Now  </a>
							    @endif
							</li>
							<li><a class="order_book"  href="{{route('order')}}" style="color:#fff;"><i class="far fa-calendar-alt"></i> Book Your Order</a></li>		
							</ul>
						</div>
					</div>
				</div>
			</div
>		</div>

		<!-- NAVBAR SECTION -->
		<div class="navbar navbar-main">
		
			<div class="container container-nav">
						
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					
					<a class="navbar-brand" href="{{url('/')}}">
						<img src="{{asset("images/settings/$setting->logo")}}" alt="{{$setting->website_name}} Logo" title="{{$setting->website_name}} Logo"/>
					</a>
				</div>
				

				<nav class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="{{url('/')}}">Home</a></li>
						@if(!empty($topNavPages) && is_array($topNavPages))
						@foreach($topNavPages as $navPage)
						<li> <a href="{{url("$navPage->slug")}}">{{$navPage->name}}</a> </li>
						@endforeach 
						@elseif(!empty($topNavPages))
						<li> <a href="{{url("$topNavPages[0]->slug")}}">{{$topNavPages[0]->name}}</a> </li>
						@endif
						<li class="dropdown"><a href="{{url('services')}}" class="dropdown-toggle" data-toggle="dropdown">Services <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="{{url('services')}}">All Services</a></li>
							@if(!empty($topNavService) && is_array($topNavService))
							
							@foreach($topNavService as $navPageKey=>$navPage)
								@php   $slug = $navPage['slug']; @endphp
								@if(empty($navPage['children']))
								<li> <a href="{{url("service/$slug")}}">{{$navPage['name']}}</a></li> 
								@elseif(!empty($navPage['children']) && count($navPage['children']) == 1)
									<li class="dropdown dropdown-submenu"> <a  class="dropdown-toggle" data-toggle="dropdown" href="{{url("service/$slug")}}">{{$navPage['name']}}</a>	
									@php   $innerSlug = $navPage['children'][0]['slug']; @endphp
									<ul class="dropdown-menu"><li> <a href="{{url("service/$innerSlug")}}">{{ $navPage['children'][0]['name'] }}</a></li></ul>
								@elseif(!empty($navPage['children']) && count($navPage['children']) > 1)
								<li class="dropdown dropdown-submenu"><a class="dropdown-toggle" data-toggle="" href="{{url("service/$slug")}}">{{$navPage['name']}}</a>
									<ul class="dropdown-menu"> 	
										@foreach($navPage['children'] as $childkey=>$childval)
										@php   $innerSlug = $childval['slug']; @endphp
										<li><a href="{{url("service/$innerSlug")}}">{{ $childval['name'] }}</a></li>
										@endforeach
									</ul>
								@endif
							</li>
							@endforeach
							@elseif(!empty($topNavService))
							<li> <a href="{{url("service/$topNavService[0]['slug']")}}">{{$topNavService[0]['name']}}</a> </li>
							@endif

						</ul>
						</li>
						<!-- This data is coming from AppServiceProvider -->			
						
						<li><a href="{{url('price')}}">Price</a></li>
						<li><a href="{{url('blogs')}}">Blogs</a></li>
						<li><a href="{{url('contact-us')}}">Contact</a></li>
					</ul>
				</nav>						
			</div>
	    </div>
    </div>
