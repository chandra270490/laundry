<!-- JS VENDOR -->
	<script type="text/javascript" src="{{asset('frontend/js/vendor/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/jquery.superslides.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/owl.carousel.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/bootstrap-hover-dropdown.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/jquery.magnific-popup.min.js')}}"></script>
	
	<!-- sendmail -->
	<script type="text/javascript" src="{{asset('frontend/js/vendor/validator.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/js/vendor/form-scripts.js')}}"></script>
	
	<script type="text/javascript" src="{{asset('frontend/js/script.js')}}"></script>	
	<script type="text/javascript" src="{{asset('frontend/js/order5.js')}}"></script>
	<script>
$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    dots:false,
    nav:true,
    autoplay:true,   
    smartSpeed: 3000, 
    autoplayTimeout:7000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})
</script>