<div class="footer">
		
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="footer-item">
						<img src="{{asset("images/settings/$setting->logo")}}" alt="{{$setting->website_name}} Logo" title="{{$setting->website_name}} Logo" class="logo-bottom"/>
					
						<p style="text-align: justify;">{{$setting->footer_about_us}}</p>
						
					</div>
				</div>
				<div class="col-sm-4 col-md-4 col-md-offset-1">
					<div class="footer-item">
						<div class="footer-title">
							Extra Links
						</div>
						<ul class="list">
							<li><a title="#" href="{{url('/')}}">Home</a></li>
							<li><a href="{{url('about-us')}}" title="">About</a></li>
							<li><a title="Services" href="{{url('services')}}">Services</a></li>
							<!-- <li><a href="FAQs" href="{{url('faqs')}}">Faqs</a></li> -->
							<li><a href="{{url('price')}}" title="Price">Price</a></li>
							<li><a href="{{url('blogs')}}" title="Blogs">Blogs</a></li>
							<li><a href="{{url('contact-us')}}" title="Contact us">Contact us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-4 col-md-4">
					<div class="footer-item">
						 <div class="footer-title">
							Follow us
						</div>
						<!--<p>Get latest updates and offers.</p>
						<form action="#" class="footer-subscribe">
			              <input type="email" name="EMAIL" class="form-control" placeholder="enter your email">
			              <input id="p_submit" type="submit" value="send">
			              <label for="p_submit"><i class="fa fa-envelope"></i></label>
			              
			            </form> -->
			            <div class="footer-sosmed">
			            	<a href="https://api.whatsapp.com/send/?phone={{$setting->social_whatsapp_link}}" title="">
								<div class="item">
									<i class="fab fa-whatsapp"></i>
								</div>
							</a>
							<a href="{{$setting->social_fb_link}}" title="Facebook">
								<div class="item">
									<i class="fab fa-facebook"></i>
								</div>
							</a>
							<a href="{{$setting->social_twitter_link}}" title="Twitter">
								<div class="item">
									<i class="fab fa-twitter"></i>
								</div>
							</a>
							<a href="{{$setting->social_linkedin_link}}" title="">
								<div class="item">
									<i class="fab fa-linkedin"></i>
								</div>
							</a>
							<a href="{{$setting->social_instagram_link}}" title="">
								<div class="item">
									<i class="fab fa-instagram"></i>
								</div>
							</a>
							<!--a href="{{$setting->social_pinterest_link}}" title="">
								<div class="item">
									<i class="fab fa-pinterest"></i>
								</div>
							</a-->
						
							<!--a href="{{$setting->social_tumblr_link}}" title="">
								<div class="item">
									<i class="fab fa-tumblr"></i>
								</div>
							</a-->
							
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="fcopy">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-12">
						<p class="ftex">&copy; {{$setting->copyright_text}} </p> 
					</div>
				</div>
			</div>
		</div>
		
	</div>