@php
	$data = json_decode(json_encode($ServicesContent));
	$ServicesContents=$data->response;

	$data = json_decode(json_encode($ServiceDetails));
	$ServiceDetails=$data->response;
@endphp

@extends('frontend.master')
@section('meta')
	<title>{{$ServiceDetails->title}}</title>
    <meta name="description" content="{{$ServiceDetails->meta_description}}">
    <meta name="keywords" content="">
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">{{$ServiceDetails->title}}</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">Home</a></li>
						<li><a href="{{url('services')}}">Services</a></li>
						<li class="active">{{$ServiceDetails->name}}</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="widget categories">
						<ul class="category-nav">
							@foreach($ServicesContents as $serviceList)
								<li @php echo request()->is("service/$serviceList->slug")==$serviceList->slug? 'class="active"':''; @endphp><a href="{{url("service/$serviceList->slug")}}">{{$serviceList->name}}</a></li>
							@endforeach
						</ul>
					</div>
					<div class="widget cta">
						<h4>Ready to book your cleaning?</h4>
						<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
						<a href="{{route('order')}}" class="btn butn_catg"><span class="fa fa-calendar-o"></span> Book Now</a>
					</div>
					<!-- <div class="widget download">
						<a href="#" class="btn btn-secondary btn-block btn-sidebar"><span class="fa fa-file-pdf-o"></span> Company Brochure</a>
					</div> -->
				</div>

				<div class="col-sm-9 col-md-9">

					<div class="single-page">
						<img src="{{url("images/services/$ServiceDetails->feature_image")}}" alt="{{$ServiceDetails->name}}" class="img-responsive">
						<hr/>
						<h2 class="section-heading-2 fs-46">{{$ServiceDetails->name}}</h2>
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<p class="para">
									{!!$ServiceDetails->description!!}
								</p>
							</div>	
						</div>
					 </div>
				</div>
			</div>
		</div>
	</div>
<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>
			</div>
		</div>
	</div>
@endsection