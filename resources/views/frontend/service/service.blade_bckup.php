@extends('frontend.master')
@section('meta')
	<title>Service Page</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Laundry Service</div>
					<ol class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li><a href="services.html">Services</a></li>
						<li class="active">Laundry Service</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="widget categories">
						<ul class="category-nav">
							<li class="active"><a href="laundry-service.html">Laundry Service</a></li>
							<li><a href="dry-cleaning-services.html">Dry Cleaning Services</a></li>
							<li><a href="ironing-service.html">Ironing Service</a></li>
							<li><a href="wash-iron.html">Wash & Iron</a></li>
							<li><a href="shoe-repair-service.html">Shoe Repair Service</a></li>
							<li><a href="alteration-service.html">Alteration Service</a></li>
						</ul>
					</div>
					<div class="widget cta">
						<h4>Ready to book your cleaning?</h4>
						<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
						<a href="#" class="btn butn_catg"><span class="fa fa-calendar-o"></span> Book Now</a>
					</div>
					<div class="widget download">
						<a href="#" class="btn btn-secondary btn-block btn-sidebar"><span class="fa fa-file-pdf-o"></span> Company Brochure</a>
					</div>
				</div>

				<div class="col-sm-9 col-md-9">

					<div class="single-page">
						<img src="images/laundry_service_page.jpg" alt="" class="img-responsive"> 
						<div class="margin-bottom-30"></div>
						<h2 class="section-heading-2 fs-46">Laundry Service</h2>
						<div class="row">
							<div class="col-md-12">
							<p class="para c2">A free pickup and same day laundry services could be a bit hectic to find in London, Essex & all East London areas, but not anymore! Hello Laundry, a top most yet a genuine brand has got your back! Laundry has never been the easiest part of household chores, especially if the clothes have stains on it that are much more stubborn than our kids.</p>
							</div>
							<div class="col-md-6 col-sm-6">
								<p class="para">We, at Hello Laundry, provide all types of laundry services right at your doorstep. Just like any other home deliveries, we do same day pick up and drop off services of your clothes after the dry cleaning and laundry services are finished smoothly. Having a customer is a blessing for any business and that goes without saying, customer satisfaction and happiness is our motive.</p>
							</div>							
							<div class="col-md-6 col-sm-6">
								<p class="para">Our dedicated team of experts give baby care to the clothes and provide proper attention till it becomes just like the new. We know how hard it is to earn money, and that’s why we care for every single penny of your hard earnings. Right from inspecting the clothes at the first counter to processing the clothes through the laundry machine as according to the requirements of your clothes.</p>
							</div>
						</div>
						
						
						<blockquote>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</blockquote>
						<div class="margin-bottom-50"></div>
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<h2 class="section-heading-2">
									What We Do
								</h2>
								<div class="section-subheading">We never disappoint our beloved customers and make sure that the clothes are delivered in proper time without a delay. </div>
								<div class="about-div">
									<ul class="check-list half-list mb_15 mt_20">
										<li><span><i class="fa fa-check"></i></span>Quality</li>
										<li><span><i class="fa fa-check"></i></span>Free Pick Up & Drop Off (24 Hours)</li>
										<li><span><i class="fa fa-check"></i></span>Hanger Packing</li>
										<li><span><i class="fa fa-check"></i></span>Affordable</li>
										<li><span><i class="fa fa-check"></i></span>Spotter-Spotting</li>
										<li><span><i class="fa fa-check"></i></span>Same Day Dry Cleaning</li>
									</ul>
								</div>	
								<!-- <ul class="checklist">
									<li>100% Secure, consectetuer adipiscing elit, sed diam nonummy nibh euismod.</li>
									<li>Easy to claim, sed diam nonummy nibh euismod. Lorem ipsum dolor sit amet.</li>
									<li>More benefit nonummy nibh euismod. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
								</ul> -->
								<div class="form-group">
								<div id="success"></div>
								<button type="submit" class="btn btn-secondary">ASK A QUOTE</button>
								</div>
							</div>
						</div>
						<div class="margin-bottom-50"></div>
						<h2 class="section-heading-2">Frequently asked questions</h2>
						<div class="section-subheading">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div> 
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
							<div class="panel-heading active" role="tab" id="heading1">
							  <h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" class="">
								  Certain pests can be very dangerous?
								</a>
							  </h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1" aria-expanded="true">
							  <div class="panel-body">
								<p>Create and publilsh dynamic websites for desktop, tablet, and mobile devices that meet the latest web standards- without writing code. Design freely using familiar tools and hundreds of web fonts. easily add interactivity, including slide shows, forms, and more.</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading2">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
								  We provide the 5 star protection plan guarantees you stay safe from pest without hassle?
								</a>
							  </h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>When you click "Buy" button you'll be directed to our web store "Themeforest". You can purchase our template there, and  you will given permission to download our templates.</p>
								<ul class="list-unstyled">
									<li><i class="fa fa-check"></i> Ready for all devices.</li>
									<li><i class="fa fa-check"></i> HTML template</li>
									<li><i class="fa fa-check"></i> Made with Bootstrap Framework.</li>
									<li><i class="fa fa-check"></i> Easy Costumizable.</li>
									<li><i class="fa fa-check"></i> Affordable Price.</li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading3">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
								  Schedule a Complimentary inspection of your Residential?
								</a>
							  </h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>Unzip the file, locate muse file and double click the file and you will directly to adobe muse. Next step you can modifications our template, you can customize color, text, font, content, logo and image  with your need using familiar tools on adobe muse without writing any code.</p>
								<p>You can't re-distribute the Item as stock, in a tool or template, or with source files. You can't re-distribute or make available the Item as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free.</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading4">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
								  Don't worry we will help out you?
								</a>
							  </h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>Open muse file, on the top bar adobe muse you will see text tool. click and add web font do you want. Over 100+ typekit web font ready to use.</p>
								<p>You just need open "asset" bar on the right side adobe muse. Next right click on the image asset, then "Relink" image asset with your image fit. All image automatically change on tablet and phone too. it's fast and simple. we make your life easier.</p>
							  </div>
							</div>
						  </div>
						</div>
					 </div>
				</div>
			</div>
		</div>
	</div>

	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="#" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
@endsection