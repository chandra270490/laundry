@php
	$data = json_decode(json_encode($ServicesContent));
	$ServicesContents=$data->response;
	$alternateNumber=1;
@endphp
@extends('frontend.master')
@section('meta')
	<title>Services Page</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection

@section('body')

<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Services</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active">Services</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- ABOUT FEATURE -->
	<div class="section pad">
		<div class="container">
			
			<div class="box-service">
				@foreach($ServicesContents as $service)
					<div class="box-service-item">
						<div class="row">
							@if($alternateNumber%2!=0)
							<div class="col-sm-4 col-md-4">
								<div class="body">
					                <h3><a href="{{url("service/$service->slug")}}" class="title">{{$service->name}}</a></h3>
					                <p class="para1">{!!\Illuminate\Support\Str::limit($service->description,197, '...')!!}</p>
					                <a href="{{url("service/$service->slug")}}" class="readmore">Read More <i class="fa fa-angle-right"></i></a>
				              	</div>
							</div>
							<div class="col-sm-8 col-md-8">
								<div class="media">
					                <img src="{{asset("images/services/$service->feature_image")}}" alt="{{$service->name}}" class="img-responsive" title="{{$service->name}}">
				              	</div>
							</div>
							@else
							<div class="col-sm-8 col-md-8">
								<div class="media">
					                <img src="{{asset("images/services/$service->feature_image")}}" alt="{{$service->name}}" class="img-responsive" title="{{$service->name}}">
				              	</div>
							</div>
							<div class="col-sm-4 col-md-4">
								<div class="body">
					                <h3><a href="{{url("service/$service->slug")}}" class="title">{{$service->name}}</a></h3>
					                <p class="para1">{!!\Illuminate\Support\Str::limit($service->description,197, '...')!!}</p>
					                <a href="{{url("service/$service->slug")}}" class="readmore">Read More <i class="fa fa-angle-right"></i></a>
				              	</div>
							</div>
							@endif
						</div>
					</div>
				@php $alternateNumber++ @endphp
				@endforeach
				
				
			</div>
			
		</div>
	</div>

	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>
			</div>
		</div>
	</div>
@endsection