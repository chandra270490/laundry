@extends('frontend.master')
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection
@section('body')
	 <!-- Dashboard Section -->
	 <div class="section">
		 <div class="content ">
      <div class="container">
        <div class="row">        	
		@include('frontend.customer.partials.navbar')
            <div class="col-lg-9 col-md-9">
            	<table class="table table-bordered table-striped table-hover table-responsive">
  <thead>
    <tr>
      <th scope="col">Sr. No.</th>
      <th scope="col">Items</th>
      <th scope="col">Order Id</th>
      <th scope="col">Order Date</th>
      <th scope="col">Payment Status</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
  @php
  $i=1;
  @endphp
    @if(!empty($orders))
      @foreach($orders as $keyorder=>$valorder)
      <tr>
        <th scope="row">{{$i}}</th>
        <td>
          @php
            $items = json_decode(json_decode($valorder->services));
          @endphp  
          @foreach($items->service as $mydata)
            <span style="text-transform: capitalize;">{{$mydata->item}} <small>({{$mydata->service}})</small></span>
            <br/>
          @endforeach  
          </td>
        <td>{{ $valorder->order_no }}</td>
        <td>{{date('Y-M-d', strtotime($valorder->created_at))}}</td>
        <td>@php $payment_status = $valorder->payment_status == 0 ? 'Pending':'Completed'; @endphp {{ $payment_status }}</td>
        <td><button type="button" class="btn_details" data-toggle="modal" data-target=".bd-example-modal-lg"><i class="far fa-eye"></i>  Details</button></td>
      </tr>
	  
	  @php
  $i++;
  @endphp
      @endforeach
    @endif
  </tbody>
</table>
            </div>
        </div> 
    </div>
  </div>
    <!-- Dashboard Section End -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    	 <div class="modal-header">
        <h5 class="modal-title text-center" id="exampleModalLabel"><strong>Order Status</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div class="content3">
        <div class="shipment">
			<div class="confirm">
                <div class="imgcircle pickUp">
                    <img src="{{ asset('frontend/images/orders/pickup-car.png') }}" alt="PickUp">
            	</div>
				<span class="line"></span>
				<p>PickUp</p>
			</div>
			<div class="process">
           	 	<div class="imgcircle">
                	<img src="{{ asset('frontend/images/orders/agent.png') }}" alt="PickUp">
            	</div>
				<span class="line"></span>
				<p>Facility</p>
			</div>
			<div class="quality">
				<div class="imgcircle">
                	<img src="{{ asset('frontend/images/orders/delivery-guy.png') }}" alt="quality check">
            	</div>
				
				<p>Delivered</p>
			</div>


			<div class="clear"></div>
		</div>
</div>
    </div>
  </div>
</div>
	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
@endsection