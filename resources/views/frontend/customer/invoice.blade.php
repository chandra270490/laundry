@extends('frontend.master')
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection
@php 
if(!empty($order))
{
$items = $order[0]->services; 
$items = json_decode(json_decode($items));
$items = $items->service;
}
@endphp
@section('body')
<!-- Dashboard Section -->
<div class="section">
    <div class="content">
        <div class="container">
            <div class="row">
                @include('frontend.customer.partials.navbar')
                <div class="col-lg-9">
                    <div class="row invoice row-printable">
                        @if(!empty($order))
                        <div class="col-md-10">
                            <!-- col-lg-12 start here -->
                            <div class="panel panel-default plain" id="dash_0">
                                <!-- Start .panel -->
                                <div class="panel-body p30">
                                    <div class="row">
                                        <!-- Start .row -->
                                        <div class="col-lg-6">
                                            <!-- col-lg-6 start here -->
                                            <div style="margin-top: 22px;" class="invoice-logo"><img width="197" height="71" src="images/logo.png" alt="Invoice logo"></div>
                                        </div>
                                        <!-- col-lg-6 end here -->
                                        <div class="col-lg-6">
                                            <!-- col-lg-6 start here -->
                                            <div class="invoice-from">
                                                <ul class="list-unstyled text-right">
                                                    <li>{{$order[0]->address}}</li>
                                                    <li>2500 Ridgepoint Dr, Suite 105-C</li>
                                                    <li>Austin TX 78754</li>
                                                    <li>VAT Number EU826113958</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!-- col-lg-6 end here -->
                                        <div class="col-lg-12">
                                            <!-- col-lg-12 start here -->
                                            <div class="invoice-details">
                                                <div class="well">
                                                    <ul class="list-unstyled mb0">
                                                        <li><strong>Invoice</strong> {{$order[0]->order_no}}</li>
                                                        <li><strong>Invoice Date:</strong>{{date("F jS, Y", strtotime($order[0]->created_at))}}</li>
                                                        <li><strong>Due Date:</strong>{{date("F jS, Y", strtotime($order[0]->created_at))}}</li>
                                                        <li><strong>Status:</strong> <span class="label label-danger">UNPAID</span></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="invoice-to">
                                                <ul class="list-unstyled">
                                                    <li><strong>Invoiced To</strong></li>
                                                    <li>{{$order[0]->first_name}} {{$order[0]->last_name}}</li>
                                                    <li>{{$order[0]->address}}</li>
                                                    <li>New York, NY, 2014</li>
                                                    <li>USA</li>
                                                </ul>
                                            </div>
                                            <div class="invoice-items">
                                                <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="0">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th class="text-center" width="25%">Image</th>
                                                                <th class="text-center" width="25%">Name</th>
                                                                <th class="text-center" width="25%">Qty</th>
                                                                <th class="text-center" width="25%">Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($items as $itemKey=>$itemVal)
                                                            <tr>
                                                                <td><img src="{{asset('images/frontend/users/')}}" class="img-thumbnail" width='100px'></td>
                                                                <td>{{ $itemVal->item }}</td>
                                                                <td class="text-center">{{ $itemVal->qty }}</td>
                                                                <td class="text-center">£{{ $itemVal->price }}</td>
                                                            </tr> 
                                                            @endforeach 
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th colspan="3" class="text-right">Sub Total:</th>
                                                                <th class="text-center">£237.00</th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="3" class="text-right">20% VAT:</th>
                                                                <th class="text-center">£47.40</th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="3" class="text-right">Credit:</th>
                                                                <th class="text-center">£00.00</th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="3" class="text-right">Total:</th>
                                                                <th class="text-center">£284.40</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="invoice-footer mt25">
                                                <p class="text-center">Generated on Monday, October 08th, 2015 <!-- <a href="#" class="btn btn-default ml15"><i class="fa fa-print mr5"></i> Print</a> --></p>
                                            </div>
                                        </div>
                                        <!-- col-lg-12 end here -->
                                    </div>
                                    <!-- End .row -->
                                </div>
                            </div>
                            <!-- End .panel -->
                        </div>
                        @endif
                        <!-- col-lg-12 end here -->
                </div>
            </div>
        </div>         
    </div>
</div>
<!-- Dashboard Section End -->
    
<!-- CTA -->
<div class="section pad cta-bgc">
    <div class="container">
        <div class="row">
            
            <div class="col-sm-12 col-md-12">
                <div class="cta-content">
                    <h3 class="cta-title-3">Ready to book your cleaning?</h3>
                </div>
                <div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
            </div>

        </div>
    </div>
</div>
<!-- CTA Ends -->
@endsection