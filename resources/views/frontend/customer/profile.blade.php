
	@extends('frontend.master')
    @section('frontend-styles')
	<style>
.nav-tabs{border-bottom: 1px solid #ccc !important; padding:0px !important ;}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {color: #555;cursor: default;background-color: #fff;border: 1px solid #ddd;border-bottom-color: transparent;}
}
</style>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    @endsection
    @section('body')
     <!-- Dashboard Section -->
	 <div class="section">
		 <div class="content ">
        @if (session('message'))
          <div class="alert alert-success">
              {{ session('message') }}
          </div>
        @endif
      <div class="container">
        <div class="row">        	
         @include('frontend.customer.partials.navbar')
            <div class="col-lg-9 col-md-9">
            <div class="bootstrap snippet">
            <form class="form" enctype="multipart/form-data" action="{{ route('customerupdateprofile')}}" method="post" id="registrationForm">
   				@csrf	 
                    <div class="row">
  							<div class="col-sm-3"><!--left col-->   
  								<div class="text-center">
                    @php
                      $profilePic="avatar_2x.png";
                      if($customer->profile_pic!='') $profilePic=$customer->profile_pic;
                    @endphp
  								  <img src="{{asset("images/frontend/users/$profilePic")}}" class="avatar img-circle img-thumbnail" alt="{{$customer->name}}">
  								  <h6>Upload a different photo...</h6>
  								  <input type="file" name="profile_pic" value="{{asset('images/frontend/users/'.$customer->profile_pic)}}" class="text-center center-block file-upload mt-2">
  								</div> 
  							</div><!--/col-3-->
    						<div class="col-sm-9">
            		<ul class="nav nav-tabs">
               	 	<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
                	<!--<li><a data-toggle="tab" href="#messages">Change Password</a></li>-->
              	</ul>

              
          <div class="tab-content">
            <div class="tab-pane active" id="home">
                
                 
                      <div class="form-group">
                        <input type="hidden" name="customer_id" value="<?=$customer->id?>">
                          <div class="col-xs-6">
                              <label for="first_name"><h4>Full Name</h4></label>
                              <input type="text" class="form-control" name="name" id="name" value="{{$customer->name}}" placeholder="Full name" title="Enter Full name">
                          </div>
                      </div>

                      <div class="form-group">
                          <div class="col-xs-6">
                             <label for="mobile"><h4>Mobile / WhatsApp</h4></label>
                              <input type="text" class="form-control" name="mobile_no" value="{{$customer->mobile_no}}" id="mobile" placeholder="Enter Mobile / WhatsApp number" title="enter your mobile / whatsapp number">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>E-mail</h4></label>
                              <input type="email" class="form-control" name="email" id="email" value="{{$customer->email}}" placeholder="you@email.com" readonly="" title="Enter Email Address">
                          </div>
                      </div>
                      <div class="form-group">
                          
                          <div class="col-xs-6">
                              <label for="email"><h4>Location</h4></label>
                              <input type="text" class="form-control" id="location" value="{{$customer->addLine}}" name="addLine" placeholder="Enter Address" title="Enter Address">
                          </div>
                      </div>

                      <div class="form-group">                          
                          <div class="col-xs-6">
                              <label for="postcode"><h4>Postal Code</h4></label>
                              <input type="text" class="form-control" id="pCode" value="{{$customer->pCode }}" name="pCode" placeholder="Enter Postal Code" title="Enter Postal Code">
                          </div>
                      </div>

                      <div class="form-group">                          
                        <div class="col-xs-6">
                            <label for="password"><h4>New Password</h4></label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" title="enter your password.">
                        </div>
                      </div>
                    
                      <div class="form-group">
                           <div class="col-xs-12">
                                <br>
                              	<button class="btn btn-lg btn-success" type="submit">
                                  <i class="fa fa-save"></i> Save Changes</button>
                               	<!--<button class="btn btn-lg" type="reset">
                                  <i class="fa fa-refresh"></i> Reset</button>-->
                            </div>
                      </div>
              	</form>
              
             
              
             </div><!--/tab-pane-->
             <div class="tab-pane" id="messages">
                <h2></h2>
                <form class="form" action="##" method="post" id="registrationForm">
                    <div class="form-group">                          
                        <div class="col-xs-6">
                            <label for="password"><h4>New Password</h4></label>
                            <input type="password" class="form-control" name="password_confirmation" id="password" placeholder="Enter Password" title="enter your password.">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <br>
                            <button class="btn btn-lg btn-success" type="submit"><i class="fa fa-save"></i> Update Password</button>
                        </div>
                    </div>
                </form>
               
             </div><!--/tab-pane-->
             
               
              </div><!--/tab-pane-->
          </div><!--/tab-content-->

        </div><!--/col-9-->
    </div><!--/row-->
  </div>
        </div> 
    </div>
  </div>

  <!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
    <!-- Dashboard Section End -->
@endsection
@section('frontend_scripts')
<script>
	$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
});
		</script>
@endsection	
