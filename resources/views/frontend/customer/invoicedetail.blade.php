@extends('frontend.master')
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection
@section('body')	 
<!-- Dashboard Section -->


<div class="section">
	<div class="content">
      <div class="container">
        <div class="row">
        	<div class="col-lg-12" style="margin-left: 90px;">
            <div class="row invoice row-printable">
                <div class="col-md-10">
                    <!-- col-lg-12 start here -->
                    <div class="panel panel-default plain" id="dash_0">
                        <!-- Start .panel -->
                        <div class="panel-body p30">
                            <div class="row">
                                <!-- Start .row -->
                                <div class="col-lg-6">
                                    <!-- col-lg-6 start here -->
                                    <div style="margin-top: 22px;" class="invoice-logo"><img width="197" height="71" src="images/logo.png" alt="Invoice logo"></div>
                                </div>
                                <!-- col-lg-6 end here -->
                                <div class="col-lg-6">
                                    <!-- col-lg-6 start here -->
                                    <div class="invoice-from">
                                        <ul class="list-unstyled text-right">
                                            <li>Dash LLC</li>
                                            <li>2500 Ridgepoint Dr, Suite 105-C</li>
                                            <li>Austin TX 78754</li>
                                            <li>VAT Number EU826113958</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- col-lg-6 end here -->
                                <div class="col-lg-12">
                                    <!-- col-lg-12 start here -->
                                    <div class="invoice-details">
                                        <div class="well">
                                            <ul class="list-unstyled mb0">
                                                <li><strong>Invoice</strong> #936988</li>
                                                <li><strong>Invoice Date:</strong> Monday, October 10th, 2015</li>
                                                <li><strong>Due Date:</strong> Thursday, December 1th, 2015</li>
                                                <li><strong>Status:</strong> <span class="label label-danger">UNPAID</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="invoice-to">
                                        <ul class="list-unstyled">
                                            <li><strong>Invoiced To</strong></li>
                                            <li>Jakob Smith</li>
                                            <li>Roupark 37</li>
                                            <li>New York, NY, 2014</li>
                                            <li>USA</li>
                                        </ul>
                                    </div>
                                    <div class="invoice-items">
                                        <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="0">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                            
                                                        <th class="text-center">Items</th>
                                                        <th class="text-center">Qty</th>
                                                        <th class="text-center">Total</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Dress Silk</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center">£25.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Puff Jacket</td>
                                                        <td class="text-center">1</td>
                                                        <td class="text-center">£200.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Waistcoat</td>
                                                        <td class="text-center">12</td>
                                                        <td class="text-center">£12.00</td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th colspan="2" class="text-right">Sub Total:</th>
                                                        <th class="text-center">£237.00</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="text-right">20% VAT:</th>
                                                        <th class="text-center">£47.40</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="text-right">Credit:</th>
                                                        <th class="text-center">£00.00</th>
                                                    </tr>
                                                    <tr>
                                                        <th colspan="2" class="text-right">Total:</th>
                                                        <th class="text-center">£284.40</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="invoice-footer mt25">
                                        <p class="text-center">Generated on Monday, October 08th, 2015 <!-- <a href="#" class="btn btn-default ml15"><i class="fa fa-print mr5"></i> Print</a> --></p>
                                    </div>
                                </div>
                                <!-- col-lg-12 end here -->
                            </div>
                            <!-- End .row -->
                        </div>
                    </div>
                    <!-- End .panel -->
                </div>
                <!-- col-lg-12 end here -->
            </div>
        </div>
    </div>         
</div>
@endsection	 
@section('frontend_scripts')
@endsection	