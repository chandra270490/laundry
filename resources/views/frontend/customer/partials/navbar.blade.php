@php 

$segment = Request::segment(2); 
$segmentDashboard = Request::segment(1); 
$rule = '
    text-decoration: none;
    outline: none;
    background-color: #0f8be1;
    color: #fff;
    ';
@endphp
<div class="col-lg-3 col-md-3">
    <div class="sidebar-container">
        <div class="sidebar-logo">Navigation</div>
        <ul class="sidebar-navigation">
            <li><a href="{{ route('customerdashboard')}}" @if($segmentDashboard == 'dashboard') style="{{$rule}}" @endif><i class="fa fa-home" aria-hidden="true"></i> Dashboard</a></li>
            <!-- <li> <a href="{{ route('invoices')}}"  @if($segment == 'invoices') style="{{$rule}}" @endif> <i class="fa fa-tachometer" aria-hidden="true"></i> Invoice </a> </li> -->
            <li><a href="{{ route('profile')}}" @if($segment == 'profile') style="{{$rule}}" @endif><i class="fa fa-users" aria-hidden="true"></i> Profile</a></li>
            <li><a href="{{ route('orders')}}" @if($segment == 'orders') style="{{$rule}}" @endif><i class="fa fa-info-circle" aria-hidden="true"></i> Orders</a></li>
        </ul>
    </div>
</div>