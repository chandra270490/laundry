@php
	
	$data = json_decode(json_encode($page));
	$page=$data->response;

@endphp
@extends('frontend.master')
@section('meta')
	<title>{{$page->title}}</title>
    <meta name="description" content="{{$page->meta_description}}">
    <meta name="keywords" content="">
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">{{$page->name}}</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active">{{$page->name}}</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	<!-- Page Content -->
	<div class="section pad">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>{{$page->title}}</h1>
				</div>
				<hr/>
				{!!$page->description!!}
			</div>
		</div>
	</div>
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>
			</div>
		</div>
	</div>
@endsection