@extends('frontend.master')
@section('meta')
	<title>Nearest Laundry - Cleaning Services</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection

@section('body')
<!-- BANNER -->
<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Frequently Asked Questions</div>
					<ol class="breadcrumb">
						<li><a href="index.html">Home</a></li>
						<li class="active">FAQs</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad">
		<div class="container">			
			<div class="row">
				<div class="col-sm-9 col-md-9">
					<div class="single-page"> 
						<div class="margin-bottom-30"></div>
						<h2 class="section-heading-2">Frequently asked questions</h2>
						<h2 class="section-heading-2"></h2>
						<!-- Laundry Services FAQs Start -->
						<div class="section-subheading fs-16 c2">Laundry Services</div> 
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
							<div class="panel-heading active" role="tab" id="heading1">
							  <h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1" class="">
								  Certain pests can be very dangerous?
								</a>
							  </h4>
							</div>
							<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1" aria-expanded="true">
							  <div class="panel-body">
								<p>Create and publilsh dynamic websites for desktop, tablet, and mobile devices that meet the latest web standards- without writing code. Design freely using familiar tools and hundreds of web fonts. easily add interactivity, including slide shows, forms, and more.</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading2">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
								  We provide the 5 star protection plan guarantees you stay safe from pest without hassle?
								</a>
							  </h4>
							</div>
							<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>When you click "Buy" button you'll be directed to our web store "Themeforest". You can purchase our template there, and  you will given permission to download our templates.</p>
								<ul class="list-unstyled">
									<li><i class="fa fa-check"></i> Ready for all devices.</li>
									<li><i class="fa fa-check"></i> HTML template</li>
									<li><i class="fa fa-check"></i> Made with Bootstrap Framework.</li>
									<li><i class="fa fa-check"></i> Easy Costumizable.</li>
									<li><i class="fa fa-check"></i> Affordable Price.</li>
								</ul>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading3">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
								  Schedule a Complimentary inspection of your Residential?
								</a>
							  </h4>
							</div>
							<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>Unzip the file, locate muse file and double click the file and you will directly to adobe muse. Next step you can modifications our template, you can customize color, text, font, content, logo and image  with your need using familiar tools on adobe muse without writing any code.</p>
								<p>You can't re-distribute the Item as stock, in a tool or template, or with source files. You can't re-distribute or make available the Item as-is or with superficial modifications. These things are not allowed even if the re-distribution is for Free.</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading4">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
								  Don't worry we will help out you?
								</a>
							  </h4>
							</div>
							<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>Open muse file, on the top bar adobe muse you will see text tool. click and add web font do you want. Over 100+ typekit web font ready to use.</p>
								<p>You just need open "asset" bar on the right side adobe muse. Next right click on the image asset, then "Relink" image asset with your image fit. All image automatically change on tablet and phone too. it's fast and simple. we make your life easier.</p>
							  </div>
							</div>
						  </div>
						</div>
						<!-- Laundry Services FAQs END -->
						<!-- Dry Cleaning Services FAQs Start -->
						<div class="hr-1 bg-black opacity-1 mt-10 mb-25"></div>
						<div class="section-subheading fs-16 c2">Dry Cleaning Services</div> 
						<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading5">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
								  Don't worry we will help out you?
								</a>
							  </h4>
							</div>
							<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>Create and publilsh dynamic websites for desktop, tablet, and mobile devices that meet the latest web standards- without writing code. Design freely using familiar tools and hundreds of web fonts. easily add interactivity, including slide shows, forms, and more.</p>
							  </div>
							</div>
						  </div>
						  <div class="panel panel-default">
							<div class="panel-heading" role="tab" id="heading6">
							  <h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
								  We provide the 5 star protection plan guarantees you stay safe from pest without hassle?
								</a>
							  </h4>
							</div>
							<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6" aria-expanded="false" style="height: 0px;">
							  <div class="panel-body">
								<p>When you click "Buy" button you'll be directed to our web store "Themeforest". You can purchase our template there, and  you will given permission to download our templates.</p>
								<ul class="list-unstyled">
									<li><i class="fa fa-check"></i> Ready for all devices.</li>
									<li><i class="fa fa-check"></i> HTML template</li>
									<li><i class="fa fa-check"></i> Made with Bootstrap Framework.</li>
									<li><i class="fa fa-check"></i> Easy Costumizable.</li>
									<li><i class="fa fa-check"></i> Affordable Price.</li>
								</ul>
							  </div>
							</div>
						  </div>						  
						</div>
						<!-- Dry Cleaning Services FAQs END -->
						
					 </div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="widget cta">
						<h4>Ready to book your cleaning?</h4>
						<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
						<a href="#" class="btn butn_catg"><span class="fa fa-calendar-o"></span> Book Now</a>
					</div>
					<div class="widget download">
						<a href="#" class="btn btn-secondary btn-block btn-sidebar"><span class="fa fa-file-pdf-o"></span> Company Brochure</a>
					</div>
				</div>

				
			</div>
		</div>
	</div>

	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="contact.html" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
@endsection