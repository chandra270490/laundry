@extends('frontend.master')
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>

@endsection
@section('body')	 
<!-- Dashboard Section -->


	 <div class="section">
	 	<div class="content">
      <div class="container">
        <div class="row">
        	<div class="col-lg-10 col-lg-offset-2">
                <div class="canvas_div_pdf">
                    <div class="row invoice row-printable">
                        <div class="col-md-10">
                            <!-- col-lg-12 start here -->
                            <div class="panel panel-default plain" id="dash_0">
                                <!-- Start .panel -->
                                <div class="panel-body p30">
                                    <div class="row">
                                        <!-- Start .row -->
                                        <div class="col-lg-6">
                                            <!-- col-lg-6 start here -->
                                            <div class="invoice-logo"><img width="197" height="71" src="{{url('/').'/'.'images/settings/logo.png'}}" alt="Invoice logo"></div>
                                        </div>
                                        <!-- col-lg-6 end here -->
                                        <div class="col-lg-6">
                                            <!-- col-lg-6 start here -->
                                            <div class="invoice-from">
                                                <!--<ul class="list-unstyled text-right">
                                                    <li>Address: {{$orders[0]->address}}</li>

                                                </ul>-->
                                                <ul class="list-unstyled text-right">
                                                    <li><strong>Invoice</strong> #{{$orders[0]->order_no}}</li>
                                                    <li><strong>Invoice Date:</strong> {{ \Carbon\Carbon::parse($orders[0]->created_at)->format('D, d F, Y')}} </li>
                                                    <li><strong>Due Date:</strong> {{ \Carbon\Carbon::parse($orders[0]->delivery_date)->format('D, d F, Y')}} </li>
                                                    <!--<li><strong>Status:</strong> <span class="label label-danger">@if($orders[0]->total_payment==NULL)UNPAID @else PAID @endif</span></li>-->
                                                </ul>
                                            </div>
                                        </div>
                                        <hr/>
                                        <!-- col-lg-6 end here -->
                                        <div class="col-lg-12">
                                            <!-- col-lg-12 start here -->
                                            <!--<div class="invoice-details">
                                                <div class="well">
                                                    <ul class="list-unstyled mb0">
                                                        <li><strong>Invoice</strong> #{{$orders[0]->order_no}}</li>
                                                        <li><strong>Invoice Date:</strong> {{ \Carbon\Carbon::parse($orders[0]->created_at)->format('D, d F, Y')}} </li>
                                                        <li><strong>Due Date:</strong> {{ \Carbon\Carbon::parse($orders[0]->delivery_date)->format('D, d F, Y')}} </li>
                                                        <li><strong>Status:</strong> <span class="label label-danger">@if($orders[0]->total_payment==NULL)UNPAID @else PAID @endif</span></li>
                                                    </ul>
                                                </div>
                                            </div>-->
                                            <div class="invoice-to">
                                                <ul class="list-unstyled">
                                                    <li><strong>Invoiced To</strong></li>
                                                    <li>{{$orders[0]->first_name}} {{$orders[0]->last_name}}</li>
                                                    <li>{{$orders[0]->address}}</li>  
                                                </ul>
                                            </div>
                                            <div class="invoice-items">
                                                <div class="table-responsive" style="overflow: hidden; outline: none;" tabindex="0">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                    
                                                                <th class="text-center">Items</th>
                                                                <th class="text-center">Qty</th>
                                                                <th class="text-center">Total</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        
                                                        @php
                                                        $subtotal=0;	
                                                        
                                                        @endphp
                                                        @if(!empty($orders))
                        
                    
                                                            @php
                                                            $items = json_decode(json_decode($orders[0]->services));
                                                            @endphp  
                                                            @foreach($items->service as $mydata)
                                                                @php
                                                                $subtotal+=$mydata->tot;
                                                                @endphp
                                                            <tr>
                                                                <td>{{$mydata->item}}</td>
                                                                <td class="text-center">{{$mydata->qty}}</td>
                                                                <td class="text-center">£{{$mydata->tot}}</td>
                                                            </tr>
                                                            @endforeach
                                                        @endif
                                                        
                                                        </tbody>
                                                        <tfoot>
                                                            <!--<tr>
                                                                <th colspan="2" class="text-right">Sub Total:</th>
                                                                <th class="text-center">£{{ $subtotal}}</th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="2" class="text-right">20% VAT:</th>
                                                                @php $vat= $subtotal*20/100; $grandtotal  = $subtotal + $vat   @endphp
                                                                <th class="text-center">£{{$vat}}</th>
                                                            </tr>
                                                            <tr>
                                                                <th colspan="2" class="text-right">Credit:</th>
                                                                <th class="text-center">£00.00</th>
                                                            </tr>-->
                                                            <tr>
                                                            
                                                                <th colspan="2" class="text-right">Total:</th>
                                                                <th class="text-center">£ {{$subtotal}}</th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- col-lg-12 end here -->
                                    </div>
                                    <!-- End .row -->
                                </div>
                            </div>
                            <!-- End .panel -->
                        </div>
                        <!-- col-lg-12 end here -->
                    </div>
                </div>
                </div>

                <div id="alert" style="text-align:center"></div>

                <div class="invoice-footer mt25">
                    <p class="text-center"><!--{{ \Carbon\Carbon::parse($orders[0]->created_at)->format('D, d F, Y')}}--> <a href="#" class="btn btn-default ml15" onclick="getPDF()"><i class="fa fa-print mr5"></i>Download PDF</a></p>
                </div>
        </div>         
    </div>
  </div>
  <!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
	@endsection	 
    @section('frontend_scripts')
    @endsection	

<script>
function getPDF(){

    var HTML_Width = $(".canvas_div_pdf").width();
    var HTML_Height = $(".canvas_div_pdf").height();
    var top_left_margin = 15;
    var PDF_Width = HTML_Width+(top_left_margin*2);
    var PDF_Height = (PDF_Width*1.5)+(top_left_margin*2);
    var canvas_image_width = HTML_Width;
    var canvas_image_height = HTML_Height;

    var totalPDFPages = Math.ceil(HTML_Height/PDF_Height)-1;


    html2canvas($(".canvas_div_pdf")[0],{allowTaint:true}).then(function(canvas) {
        canvas.getContext('2d');
        
        console.log(canvas.height+"  "+canvas.width);
        
        
        var imgData = canvas.toDataURL("image/jpeg", 1.0);
        var pdf = new jsPDF('p', 'pt',  [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin,canvas_image_width,canvas_image_height);
        
        
        for (var i = 1; i <= totalPDFPages; i++) { 
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height*i)+(top_left_margin*4),canvas_image_width,canvas_image_height);
        }
        
        pdf.save("Invoice.pdf");
        document.getElementById('alert').innerHTML='<b style="color:green">PDF Downloaded Successfully...</b>';
    });
};
</script>