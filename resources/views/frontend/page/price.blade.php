@extends('frontend.master')
@section('meta')
	<title>Price - Nearestlaundry</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
@endsection

@section('body')

<?php 
	//Convert API Data of Services Section Content for Home Page Start	
		$dataServices = json_decode(json_encode($ServicesContent)); 
		$ServicesContent=$dataServices->response;
	//Convert API Data of Services Section Content for Home Page Start
	
	//Convert API Data of Items Section Content for Home Page Start	
		$items = json_decode(json_encode($items)); 
		$items=$items->response;
	//Convert API Data of Items Section Content for Home Page Start

?>
<!-- BANNER -->
<div class="section banner-page">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="title-page">Price</div>
				<ol class="breadcrumb">
					<li><a href="{{url('/')}}">Home</a></li>
					<li class="active">Price</li>
				</ol>
			</div>
		</div>
	</div>
</div>

<!-- Page Content -->
<div class="section pad">
	<div class="container mt-5">			
		<div class="row">	
			<div class="col-md-12 ml-auto col-sm-12 mr-auto">
				<div class="card">
					<div class="card-header">
						<ul class="nav nav-tabs justify-content-center" role="tablist">
							@php $i=1 @endphp
							@foreach($ServicesContent as $topservices)
							<li class="nav-item @php echo $i==1?'active':''; @endphp">
								<a class="nav-link" data-toggle="tab" href="#{{$topservices->slug}}{{$i}}" role="tab">
									{{$topservices->name}}
								</a>
							</li>
							@php $i++ @endphp
							@endforeach
						</ul>
					</div>
					
					<div class="card-body" style="margin: 25px 15px;">
						<div class="tab-content text-center">
						@php $j=1 @endphp
						@foreach($ServicesContent as $index => $service_name)
							<div class="tab-pane @php echo $j==1?'active':''; @endphp" id="{{$service_name->slug}}{{$j}}" role="tabpanel">
								<!-- ACCORDION  -->
								@php $lp = 1 @endphp
								@foreach($service_name->children as $children)
								<div class="panel-group panel-faq" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default">
										<div class="panel-heading active" role="tab" id="heading{{$lp}}{{$j}}">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$lp}}{{$j}}" aria-expanded="true" aria-controls="collapse{{$lp}}{{$j}}" class="" style="text-align:left">{{$children->name}}</a>
											</h4>
										</div>

										<div id="collapse{{$lp}}{{$j}}" class="panel-collapse collapse @php echo $lp==1?'in':''; @endphp" role="tabpanel" aria-labelledby="heading{{$lp}}{{$j}}" aria-expanded="true">
											<div class="panel-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th scope="col-md-9" style="color:#0d8ce7">Products Name</th>
															<th scope="col-md-3" style="color:#0d8ce7; text-align:right">Prices</th>
														</tr>
													</thead>
													<tbody>
														@foreach($children->items as $item)
														<tr>
															<td>{{ $item->name }}</td>
															<td style="text-align:right">{{ $item->customer_price }}</td>
														</tr>
														@endforeach
													</tbody>
												</table>
											</div>
										</div>
									</div> 
								</div>	
								<!-- ACCORDION END -->
								@php $lp++; @endphp
								@endforeach
							</div>
						@php $j++ @endphp
						@endforeach
						</div>
					</div>	
				</div>
			</div>
		</div>			
	</div>
</div>


<!-- CTA -->
<div class="section pad cta-bgc">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<div class="cta-content">
					<h3 class="cta-title-3">Ready to book your laundary services?</h3>
				</div>
				<div class="cta-action"><a href="{{url('/contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
			</div>

		</div>
	</div>
</div>
@endsection