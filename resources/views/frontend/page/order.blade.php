@extends('frontend.master')
@php 
use Carbon\Carbon;
$timeSlotArray = array()
@endphp
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/order5.css')}}" />
<style type="text/css">
	#card-holder-name
{width: 74%;
    padding: 10px;
    margin: 0px 5px;
    border-radius: 5px;
    border: 1px #c5c5c5 solid;}
    .mt-15{margin-top: 15px;}
    
    input[type="number"] {
    width: 50px;
    }
</style>
@endsection
	@section('body')
	<div class="section pad bglight order_booking vue_laundry_form bg1" id="laundryForm">
		<div class="container">			
			<div class="row">
			<div class="">	
				<div class="col-sm-8 col-md-8 col-xs-12">
				<section class="wizard-section">
				<div class="row no-gutters">
				<div class="form-wizard">
					<form class="card-form" action="{{ route('saveorder') }}" method="post" role="form">
						@csrf
						<div class="form-wizard-header">
							<h6 class="text-center">Order Booking Form</h6>
							<ul class="list-unstyled form-wizard-steps clearfix">
								<li class="active"><span>1</span></li>
								<li><span>2</span></li>
								<li><span>3</span></li>
								<li><span>4</span></li>
								<li><span>5</span></li>
							</ul>
						</div>
						<fieldset class="wizard-fieldset show">
							<h5>Address Details</h5>
								<div class="row">
									<div class="col-md-9 col-sm-6">
										<div class="form-group">
											<label for="zcode">Postcode *</label>
											<input type="text" placeholder="Enter Postcode" class="form-control wizard-required" id="zcode" name="postalCode" >	
											<div id="postcode_error"></div>
										</div>
									</div> 
									<div class="col-md-3 col-sm-6">
										<div class="form-group">
											<!--<a href="#" class="btn btn-success c1 mt35">Get Address</a> -->
											<button type="button" class="btn btn-success c1 mt35" onClick='getAddress()'>Get Address</button>
										</div>
									</div>
								</div>
							<div class="form-group">
								<label for="address" >Select Address*</label>
								<select class="form-control wizard-required" name="single_address" id="single_address">
									<option>Select Address</option>
			
								</select>
								<div class="wizard-form-error">Please Select Address</div>
							</div>
							<div class="form-group">
								<label for="extraRequest">Please Specify Any Extra Address Details</label>
								<textarea type="text" id="extraRequest"  name="extraRequest" rows="3" placeholder="" class="form-control"></textarea>			
								<div class="wizard-form-error"></div>
							</div>							
							<div class="form-group clearfix text-center">
								<a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
							</div>
						</fieldset>	
						<fieldset class="wizard-fieldset">
							<input type="hidden" name="services" id="services">
							<h5>Services Details</h5>
							<!-- vertical Tab Start -->
									<div class="row">
									<div class="col-sm-3">
									<ul class="nav nav-tabs tabs-left" role="tablist">
									@if(!empty($topService) && is_array($topService))
									@foreach($topService as $navPageKey=>$navPage)
									@php   $slug = $navPage['slug']; @endphp
									<li role="presentation" @if($navPageKey == 0) class="active" @endif><a href="#{{$slug}}" aria-controls="{{$slug}}" role="tab" data-toggle="tab">{{$navPage['name']}}</a></li>
									@endforeach
									@endif
								</ul>
									</div>
						
<div class="col-sm-9">
<div class="tab-content">
@if(!empty($topService) && is_array($topService))
@php $isno=1; @endphp
@foreach($topService as $navPageKey=>$navPage)
@php   $slug = $navPage['slug']; @endphp
<div role="tabpanel" class="tab-pane @php echo $isno==1? 'active':''; $isno++;@endphp" id="{{$slug}}">
    <div class="services-form">
        <div class="services-container">
        @if(!empty($navPage['children']) && count($navPage['children']) == 1)
            <div data-cat="1" class="service-box shadow-box">
                <div class="row media">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="media-body">
                                <h4 class="media-heading WTD">{{ $navPage['children'][0]['name'] }}</h4>
                            </div>	
                        </div> 
                    
                        <div class="clearfix"></div>
                        @if(!empty($navPage['children'][0]['items']) && count($navPage['children'][0]['items']) == 1)
                        <div class="col-md-6 col-xs-12 mt-15">
                            <div class="prices-item">
                                <img src="{{ asset('images/services/items/'.$navPage['children'][0]['items']['item_image'])}}">
                                <div class="prices-item-left">
                                	<div class="prices-item-name"> {{$navPage['children'][0]['items']['name']}}</div>
                                </div>
                                
                                <!-- Item Price Increase Start -->
								<div class="quantity field1" id="product1">
									<input id="number{{$navPage['children'][0]['id']}}" type="number" value="1" class="qty" name="qty" min="0" onClick="CalTotal('{{$navPage['children'][0]['id']}}')">
									<input type="hidden" id="cus_price{{$navPage['children'][0]['id']}}" name="cus_price" class="cus_price" value="{{$navPage['children'][0]['customer_price']}}">
								</div>
								
								<div class="prices-item-price">&#163;<span id="output{{$navPage['children'][0]['id']}}">{{$valItem['customer_price']}}</span></div>
                                
                                <div><i date-image="{{$navPage['children'][0]['items']['item_image']}}" data-service="{{$navPage['children'][0]['name']}}" data-value="{{$navPage['children'][0]['items']['name']}}" data-price="{{$navPage['children'][0]['items']['customer_price']}}" data-id="number{{$navPage['children'][0]['items']['id']}}" class="fa fa-plus btn_toggle"></i>
                                <!-- please this value with dynamic price -->
                                <i class="fa fa-minus" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>
                        @elseif(!empty($navPage['children'][0]['items']) && count($navPage['children'][0]['items']) > 1)
                            @foreach($navPage['children'][0]['items'] as $keyItem=>$valItem)
                            <div class="col-md-6 col-xs-12 mt-15">
                                <div class="prices-item">
                                    <img src="{{ asset('images/services/items/'.$valItem['item_image'])}}">
                                    <div class="prices-item-left">
                                    	<div class="prices-item-name"> {{$valItem['name']}}</div>
                                    </div>
                                    
                                    <!-- Item Price Increase Start -->
									<div class="quantity field1" id="product1">
										<input id="number{{$valItem['id']}}" type="number" value="1" class="qty" name="qty" min="0" onClick="CalTotal('{{$valItem['id']}}')">
										<input type="hidden" id="cus_price{{$valItem['id']}}" name="cus_price" class="cus_price" value="{{$valItem['customer_price']}}">
									</div>
									
									<div class="prices-item-price">&#163;<span id="output{{$valItem['id']}}">{{$valItem['customer_price']}}</span></div>
                                    
                                    <div><i date-image="{{$valItem['item_image']}}" data-service="{{$navPage['children'][0]['name']}}" data-value="{{$valItem['name']}}" data-price="{{$valItem['customer_price']}}" data-id="{{$valItem['id']}}" class="fa fa-plus btn_toggle"></i>
                                    <!-- please this value with dynamic price -->
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="col-md-6 col-xs-12">No Item available</div>
                        @endif
                        <div class="clearfix"></div>
                                                                            
                    </div>
                </div>
            </div>
            <div class="form-group">
				<label for="address" class="wizard-form-text-label">Other Request</label>
                <textarea type="text" id="anyRequest"  name="anyRequest" rows="3" placeholder="" class="form-control"></textarea>
                <div class="wizard-form-error"></div>
            </div>
            <div id="serviceMsg" class="res-msg"></div>
        @elseif(!empty($navPage['children']) && count($navPage['children']) > 1)
            @foreach($navPage['children'] as $childkey=>$childval)
                <div data-cat="1" class="service-box shadow-box">
                    <div class="row media">
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="media-body">
                                    <h4 class="media-heading WTD">{{ $childval['name'] }}</h4>
                                </div>	
                            </div> 
                        
                            <div class="clearfix"></div>
                            @if(!empty($childval['items']) && count($childval['items']) == 1)
                            <div class="col-md-6 col-xs-12 mt-15">
                                <div class="prices-item">
									@php $image = $childval['items']['item_image']; @endphp
                                    <img src="{{ asset('images/services/items/'.$image)}}">
                                    <div class="prices-item-left">
                                    	<div class="prices-item-name"> {{$childval['items']['name']}}</div>
                                    </div>
                                    
                                    <!-- Item Price Increase Start -->
									<div class="quantity field1" id="product1">
										<input id="number{{$childval['items']['id']}}" type="number" value="1" class="qty" name="qty" min="0" onClick="CalTotal('{{$childval['items']['id']}}')">
										<input type="hidden" id="cus_price{{$childval['items']['id']}}" name="cus_price" class="cus_price" value="{{$childval['items']['customer_price']}}">
									</div>
									
									<div class="prices-item-price">&#163;<span id="output{{$childval['items']['id']}}">{{$valItem['customer_price']}}</span></div>
                                    
                                    <div><i date-image="{{$image}}" data-service="{{$childval['name']}}" data-value="{{$childval['items']['name']}}" data-price="{{$childval['items']['customer_price']}}" data-id="{{$childval['items']['id']}}" class="fa fa-plus btn_toggle"></i>
                                    <!-- please this value with dynamic price -->
                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </div>
                            @elseif(!empty($childval['items']) && count($childval['items']) > 1)
                                @foreach($childval['items'] as $keyItem=>$valItem)
                                    <div class="col-md-6 col-xs-12 mt-15">
                                        <div class="prices-item">
											@php $image = $valItem['item_image']; @endphp
                                            <img src="{{ asset('images/services/items/'.$image)}}">
                                            <div class="prices-item-left">
                                            	<div class="prices-item-name"> {{$valItem['name']}}</div>
                                            </div>
                                            <!-- Item Price Increase Start -->
											<div class="quantity field1" id="product1">
												<input id="number{{$valItem['id']}}" type="number" value="1" class="qty" name="qty" min="0" onClick="CalTotal('{{$valItem['id']}}')">
												<input type="hidden" id="cus_price{{$valItem['id']}}" name="cus_price" class="cus_price" value="{{$valItem['customer_price']}}">
											</div>
											
											<div class="prices-item-price">&#163;<span id="output{{$valItem['id']}}">{{$valItem['customer_price']}}</span></div>
                                            <div><i date-image="{{$image}}" data-service="{{$childval['name']}}" data-value="{{$valItem['name']}}" data-price="{{$valItem['customer_price']}}" data-id="{{$valItem['id']}}" class="fa fa-plus btn_toggle"></i>
                                            <!-- please this value with dynamic price -->
                                            <i class="fa fa-minus" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="col-md-6 col-xs-12  mt-15">No Item available</div>
                            @endif
                            
                            <div class="clearfix"></div>
                                                                                
                        </div>
                    </div>
                </div>											
            @endforeach
            <div class="form-group">
					<label for="anyRequest" class="wizard-form-text-label">Other Request</label>
					<textarea type="text" id="anyRequest"  name="anyRequest" rows="3" placeholder="" class="form-control"></textarea>
                    <div class="wizard-form-error"></div>
                </div>
            <div id="serviceMsg" class="res-msg"></div>
        @endif
        </div> 
    </div>
</div>
@endforeach
@endif
									
									</div>
									</div>
									</div>
									<div class="form-group clearfix text-center">
										<a href="#" class="form-wizard-previous-btn float-left">Previous</a>
										<a href="#" class="form-wizard-next-btn float-right">Next</a>
									</div>
						</fieldset>	
						<fieldset class="wizard-fieldset">
							<h5>Collection & Delivery Time Details</h5>
							<div class="collection-form"> 
							<div class="sub-form">
							<div class="form-row">
							<div class="col-md-6 col-xs-12">
								<div class="form-group">
									<label for="exampleFormControlSelect1">Collection Date</label>
									<select  class="form-control wizard-required" name="collection_date" id="collection_date">
									  <option value="">Collection Date</option>
									  @php $current = Carbon::now()->format('Y-m-d'); @endphp
									  <option value="{{ $current }}">{{ $current }}</option>
									  <option value="{{ Carbon::now()->addDays(1)->format('Y-m-d') }}">{{ Carbon::now()->addDays(1)->format('Y-m-d') }}</option>
									  <option value="{{ Carbon::now()->addDays(2)->format('Y-m-d') }}">{{ Carbon::now()->addDays(2)->format('Y-m-d')  }}</option>
									  
									</select>
								</div>								
							</div> 
							<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Collection Time</label>
								<select style="display: none;" class="form-control bs-timepicker" name="collection_time" id="collection_time">
							
								</select>
							</div>
							</div> 
							<div class="col-md-12">
							<!-- <div class="form-group">
							<label for="colInstruction">Collection Instruction?</label> 
							<textarea id="colInstruction" name="colInstruction" rows="3" placeholder="Enter Collection Instruction" class="form-control wizard-required"></textarea>
							</div> -->
							<div class="form-group">
								<label for="colInstruction">Enter Collection Instruction</label>	
								<textarea id="colInstruction" name="colInstruction" rows="3" class="form-control "></textarea>
								<div class="wizard-form-error">Please choose delivery Instruction</div>
							</div>
							</div>
							</div></div> 
							<div id="deliveryForm" class="sub-form"><div class="form-row">
							<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Delivery Date</label>
								<select class="form-control wizard-required" id="delivery_date" name="delivery_date">
								  <option>Delivery Date</option>
								</select>
							</div>
							
							<div id="delDateMsg" class="res-msg">*Please Select Collection Date </div>
							</div> 
							<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Delivery Time</label>
								<select style="display: none;" class="form-control" name="delivery_time" id="delivery_time">
						
								</select>
								<select style="display:none;" class="form-control" id="delivery_time1">
						
								</select>
							</div>
							<div id="delTimeMsg" class="res-msg">*Please Select Collection Date </div></div> 
							<div class="col-md-12">
							<div class="form-group">
							<label for="delInstruction">Enter Delivery Instruction</label>
							<textarea id="delInstruction" name="delInstruction" rows="3" class="form-control"></textarea>
							<div class="wizard-form-error">Please choose delivery Instruction</div>
							</div>
							</div>
							</div>
							</div> 
							</div>
							<div class="form-group clearfix text-center">
								<a href="#" class="form-wizard-previous-btn float-left">Previous</a>
								<a href="#" class="form-wizard-next-btn float-right">Next</a>
							</div>
						</fieldset>	
						<fieldset class="wizard-fieldset">
							<h5>Personal Information</h5>
							<!-- Personal Details Start -->
							<div class="personal-form"> 

								<div class="form-row">
									<div class="col-md-6 col-12">
										<div class="form-group">
										<label for="fName" class="">Full Name*</label> 
										<input type="text" id="fName" name="fName" class="form-control wizard-required"  value="@php echo session()->has('token')? Auth::user()->name:''; @endphp">  
										<div class="wizard-form-error"></div>
										</div>
									</div> 
								<div class="col-md-6 col-12">
									<div class="form-group">
									<label for="cEmail" class="">Email Id*</label> 
									<input type="email" id="cEmail" name="email" class="form-control wizard-required" value="@php echo session()->has('token')? Auth::user()->email:''; @endphp" @php echo session()->has('token')? 'readonly':''; @endphp > 
									<div class="wizard-form-error"></div>
									</div>
								</div> 
								<div class="col-md-6 col-12">
									<div class="form-group">
									<label for="pCode" class="">Postcode*</label> 
									<input type="text" id="pCode" name="pCode" class="form-control wizard-required" value="@php echo session()->has('token')? Auth::user()->pCode:''; @endphp" maxlength="10">
									<div class="wizard-form-error"></div>
									</div>
								</div> 
								<div class="col-md-6 col-12">
								<div class="form-group">
									<label for="addLine" class="">Address Line*</label> 
									<input type="text" id="addLine" name="addLine" class="form-control wizard-required" value="@php echo session()->has('token')? Auth::user()->address:''; @endphp">
									<div class="wizard-form-error"></div>
									</div>
								</div>
								@if(!(session()->has('token')))
								<div class="col-md-6 col-12">
								<div class="form-group">
									<label for="password" class="">Password</label> 
									<input type="password" id="password" name="password" class="form-control wizard-required" placeholder="Enter Password"> 
									<div class="wizard-form-error"></div>
									</div>
								</div>
								@endif
								<div class="col-md-6 col-12">
									<div class="form-group"><label for="extAdd" class="">Extra Address Details</label> <input type="text" id="extAdd" name="extAdd" class="form-control" value="@php echo session()->has('token')? Auth::user()->extAdd:''; @endphp">
									<div class="wizard-form-error"></div>
									</div>
								</div> 
								<div class="col-md-6">
									<div class="form-group">
									<label for="mobNo" class="">Mobile/WhatsApp No*</label> 
									<input type="text" id="mobNo" name="mobile_number" class="form-control wizard-required" value="@php echo session()->has('token')? Auth::user()->mobile_no:''; @endphp" maxlength="10"> 
									<div class="wizard-form-error"></div>
									</div>
								</div> 
								</div> 
							</div>
							<!-- Personal Details End -->							
							<div class="form-group clearfix text-center">
								<a href="#" class="form-wizard-previous-btn float-left">Previous</a>
								<a href="#" class="form-wizard-next-btn float-right">Next</a>
							</div>
						</fieldset>	
						<fieldset class="wizard-fieldset">
							<h5>Payment Information</h5>
							<div class="form-group">
								
								<div class="wizard-form-radio">
									<label>Amount (£)</label>
									<input name="amount"  type="text" value="20" class="form-control" >
									<input type="hidden" name="payment_method"  class="payment-method">
									
								</div>
							</div>
							<div class="form-group">
								<label for="mobNo" class="">Card Holder Name</label> 
								<input id="card-holder-name" type="text">
									<!-- Stripe Elements Placeholder -->
								<div id="card-element"></div>
							</div>
							
							
							<div class="form-group clearfix text-center">
								<a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
								<button type="submit" class="form-wizard-submit float-right">Submit</button>
							</div>
						</fieldset>	
					</form>
				</div>
			
		</div>
	</section>
				</div>


			<div class="col-sm-4 col-md-4 col-xs-12">	
					<div class="odrsummary text-center"><span>Order Summary</span></div>
					<div class="vue-sidebar">
						<!-- <h5 class="sidebar-title text-center">Summary</h5>  -->
						<div class="v-order-box"><div class="order-step"><h4>Address</h4><p class="address"></p></div> <div class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div> 
						<hr class="dashed"> 
						<div class="v-order-box"><div class="order-step"><h4>Services</h4><div class="add_here"><ul class="services-list"></ul></div></div> <div class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div> 
						<hr class="dashed"> 
						<div class="v-order-box"><div class="order-step"><h4>Collection</h4><p class="collection-date"></p> <p class="collection-time"></p></div> <div class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div> 
						<hr class="dashed"> 
						<div class="v-order-box"><div class="order-step"><h4>Delivery</h4><p class="delivery-date"></p> <p class="delivery-time"></p></div> <div class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div> 
						<hr class="dashed"> 
						<div class="v-order-box disabled"><div class="order-step"><h4 data-toggle="modal" data-target="#loginModal"> Personal Details</h4><p class="f_Name"></p> <p class="l_Name"></p> <p class="email_id"></p> <p class="post_code"></p> <p class="address_line"></p> <p class="extra_address"></p> <p class="phone_no"></p></div> <div data-toggle="modal" data-target="#loginModal" class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div> 
						<hr class="dashed"> 
						<div class="v-order-box"><div class="order-step"><h4>Payment</h4></div> <div class="order-edit"><i class="fa fa-pencil" aria-hidden="true"></i></div></div>
					</div>
				</div>
				<!-- <div class="col-sm-4 col-md-4 col-xs-12" >
					<div class="odrsummary text-center"><span>Order Summary</span></div>
						<div class="sidebox"> 
							<div class="v-order-box">
								<div class="order-step">
								<h4>Address</h4>
								<p class="address"></p>	
								</div> 
								<div class="order-edit">
								<i class="fa fa-pencil" aria-hidden="true"></i>
								</div>
								</div> 
							 
						</div>
						<hr class="dashed"> 
						<div class="sidebox"> 
							<div class="v-order-box">
								<div class="order-step">
									<h4>Services Details&nbsp;&nbsp;&nbsp;&nbsp; 
										<i class="fa fa-pencil" aria-hidden="true"></i>
										<div class="add_here">
											<ul class="services-list">
												
											</ul>
										</div>
									</h4>
								</div>
							</div> 
						</div>
						<hr class="dashed"> 
						<div class="sidebox">
							<div class="v-order-box">
								<div class="order-step">
									<h4>Collection Details&nbsp;&nbsp;&nbsp;&nbsp; 
										<i class="fa fa-pencil" aria-hidden="true"></i>
										<p class="collection-date"></p> <p class="collection-time"></p>
									</h4>
								</div>
							</div> 
						</div>
						<hr class="dashed"> 
						<div class="sidebox">
							<div class="v-order-box">
								<div class="order-step">
									<h4>Delivery Details&nbsp;&nbsp;&nbsp;&nbsp; 
										<i class="fa fa-pencil" aria-hidden="true"></i>
										<p class="delivery-date"></p> <p class="delivery-time"></p>
									</h4>
								</div>
							</div> 
						</div>
						<hr class="dashed"> 
						<div class="sidebox"> 
							<div class="v-order-box">
								<div class="order-step">
									<h4>Personal Details &nbsp;&nbsp;&nbsp;&nbsp; 
										<i class="fa fa-pencil" aria-hidden="true"></i>
										<p class="f_Name"></p> 
										<p class="l_Name"></p>
										<p class="email_id"></p>
										<p class="post_code"></p>
										<p class="address_line"></p>
										<p class="extra_address"></p>
										<p class="phone_no"></p>
									</h4>
								</div>
							</div> 
						</div>
						<hr class="dashed"> 
						<div class="sidebox">
							<div class="v-order-box">
								<div class="order-step">
									<h4>Payment Details &nbsp;&nbsp;&nbsp;&nbsp; 
										<i class="fa fa-pencil" aria-hidden="true"></i>
									</h4>
								</div>
							</div> 
						</div>
					
				</div> -->
			</div>	
			</div>
		</div>
	</div>
	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
		 
@endsection
@section('frontend_scripts')
<!-- Address Details Jquery Start-->
<script>
function getAddress()
{
	var postal_code = $('#zcode').val();
	$.ajax({
		type:'GET',
		url:'{{ route("getwebaddress")}}',
		data:{postal_code:postal_code},
		success:function(response)
		{
			console.log(response);
			var len = 0;
			if(response != null)
			{
				len = response['data'].length;
			}

			$("#single_address").empty();
			
			if(len > 0)
			{
				document.getElementById('postcode_error').innerHTML='<span style="color:green;margin:12px 0px;font-weight:500;float:left">Please Select Pickup Address from below</span>';
				var option1 = "<option selected value=''>Please Select</option>";
				// Read data and create <option >
				for(var i=0; i<len; i++){
					/*CNS*/
					//var id = response['data'][i].id;
					var address = response['data'][i];
					/*CNS*/
					
					var option = "<option value='"+address+"'>"+address+"</option>"; 
					
					$("#single_address").append(option); 
				}
				
				$("#single_address").prepend(option1); 
			} 
			else
			{
				document.getElementById('postcode_error').innerHTML='<span style="color:red;margin:12px 0px;font-weight:500;float:left">Invalid Postcode or We are not serving in this area. Please <a href="{{url('contact-us')}}" target="_blank"><i class="fa fa-link"></i> Contact </a> with information</span>';
			}
		}
	});
}
</script>

<script>
$(document).ready(function(){
    $("#single_address").change(function()
    {
        var singleValues=$(this).val();
        var dataString = 'singleValues='+ singleValues;
		$( ".address" ).html('<strong>Address</strong> :' + singleValues);
       // alert(id); return false;
    });

});
</script>
<script>
	
var main_service = {};
main_service.service = [];
$(".btn_toggle").on('click',function() {

var child_service = $(this).attr('data-service');
var imagedata = $(this).attr('data-image');
var servicedata = $(this).attr('data-value');
var pricedata = $(this).attr('data-price');
var id = $(this).attr('data-id');
//alert(id);
var qty = $('#number' + id).val();
//alert(qty);
var tot = pricedata * qty;
main_service.service.push({ "service": child_service ,"item": servicedata,"price": pricedata,"image":imagedata,"qty":qty,"tot":tot});
$('#services').val( JSON.stringify(main_service));
if($(this).hasClass("fa-minus"))
 { 
	$(this).removeClass('fa fa-minus show');
	$(this).addClass('fa fa-plus');
	$('li:contains("' + servicedata + '")').remove();
//$('.services-list').find('li > [data-value="' + servicedata + '"]').remove();
return false;
//$('.services-list').find(`[data-value='${servicedata}']`).remove();
}

$(this).removeClass('fa-plus');
$(this).addClass('fa fa-minus show');
$('.services-list').append('<li data-value="'+servicedata+'">'+servicedata+'&nbsp;&nbsp;<span>Qty:'+qty+'</span>&nbsp;&nbsp;<span>Unit:'+pricedata+'&nbsp;&nbsp;<span>Tot:'+tot+'</span></span></li>');
});
</script>

<!-- Collection Section -->
<script>
$(document).ready(function(){
	var slots = '<?php echo json_encode($slots["response"]); ?>';
    $("#collection_date").change(function()
    {
		var today = new Date();
		var todayDate = String(today.getDate()).padStart(2, '0');
	
		var selectedday = new Date($(this).val());
		var selectedDate = String(selectedday.getDate()).padStart(2, '0');
		
		const diffTime = Math.abs(selectedDate - todayDate);
		const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
		
        var collectionValues=$(this).val();
		var collectionSelectedtext = $("#collection_date option:selected").text();

		@php $i=0;
			$current = Carbon::now()->setTimezone('Asia/Kolkata')->format('g:i A');
			$hours = Carbon::now()->setTimezone('Asia/Kolkata')->format('g');
			$AMPM = Carbon::now()->setTimezone('Asia/Kolkata')->format('A');
		
		@endphp
		
		var hours = "{{$hours}}";
		var am_pm = "{{$AMPM}}";

		
		if(diffTime >= 1)
		{
			var hours = "06";
			var am_pm = "AM";
			
		}
	
		
		
		if(!collectionValues)
			$("#collection_time").css("display", "none");

        var dataString = 'collectionValues='+ collectionSelectedtext;
		$( ".collection-date").html('<strong>Collection Date</strong> :' + collectionSelectedtext);
		$.ajax({
           type:'GET',
           url:'{{ route("getdelivery")}}',
           data:{collection_date:collectionValues},
           success:function(response){
			   console.log(response);
				var len = 0;
				if(response != null){
				len = response.length;
				}
			$("#collection_time").empty();
			$("#delivery_time").css("display", "none");	
			$("#delivery_date").empty();
			
             if(len > 0){
				$("#collection_time").css("display", "block");
				var option1 = "<option selected value=''>Please Select</option>";
               // Read data and create <option >
               for(var i=0; i<len; i++){

                 var delivery_date = response[i];
				 
                var option = "<option value='"+delivery_date+"'>"+delivery_date+"</option>"; 

                 $("#delivery_date").append(option); 
               }
			   $("#delivery_date").prepend(option1); 
             }
           }
        });
		// Get collection time
		$.ajax({
           type:'GET',
           url:'{{ route("getcollectiontime")}}',
           data:{start_hours:hours,am_pm:am_pm},
           success:function(response){
			   console.log(response);
				var len = 0;
				if(response != null){
				len = response.length;
				}
			
             if(len > 0){
				$("#collection_time").css("display", "block");
				var option1 = "<option value='' selected>Please Select</option>";
               // Read data and create <option >
               for(var i=0; i<len; i++){

                 var start_hours = response[i]['start_hours'];
				 var start_format = response[i]['start_format'];
				 var end_hours = response[i]['end_hours'];
				 var end_format = response[i]['end_format'];
				 
                var option = "<option value='"+start_hours+'-'+start_format+"'>"+start_hours+':00'+start_format+' - '+end_hours+':00'+end_format+"</option>"; 

                 $("#collection_time").append(option); 
               }
			   $("#collection_time").prepend(option1); 
             }
           }
        });
	   // alert(id); return false;
    });

});

$(document).ready(function(){
    $("#collection_time").change(function()
    {
        var collectionTimeSelectedtext = $("#collection_time option:selected").text();
		var delivery_time =$(this).val();
		$( ".collection-time").html('<strong>Collection Time</strong> :' + collectionTimeSelectedtext);

		$.ajax({
           type:'GET',
           url:'{{ route("getdeliverytime")}}',
           data:{delivery_time:delivery_time},
           success:function(response){

			var len = 0;
			if(response != null){
			len = response.length;
			}
			$("#delivery_time").empty();
			$('#delivery_time1').empty();
			$("#delivery_time").css("display", "none");
             if(len > 0){

				var option1 = "<option value=''>Please Select</option>";
               // Read data and create <option >
               for(var i=0; i<len; i++){

				var start_hours = response[i]['start_hours'];
				 var start_format = response[i]['start_format'];
				 var end_hours = response[i]['end_hours'];
				 var end_format = response[i]['end_format'];
				 
                var option = "<option value='"+start_hours+'-'+start_format+"'>"+start_hours+':00'+start_format+' - '+end_hours+':00'+end_format+"</option>"; 
				$("#delivery_time").append(option); 
			   }
			   $("#delivery_time").prepend(option1); 
			   $('#delivery_time1').append($('#delivery_time').html());
             }
           }
        });
       // alert(id); return false;
    });

});
</script>
<!-- Delivery Section -->

<script>
$(document).ready(function(){
    $("#delivery_date").change(function()
    {
		if($(this).val()){
			var today = new Date();
			var todayDate = String(today.getDate()).padStart(2, '0');
		
			var selectedday = new Date($(this).val());
			var selectedDate = String(selectedday.getDate()).padStart(2, '0');
			
			var collectionDay = new Date($('#collection_date').val());
			var collectionSelectedDate = String(collectionDay.getDate()).padStart(2, '0');
			
			const diffCollectionTime = Math.abs(selectedDate - collectionSelectedDate);
			const diffTime = Math.abs(selectedDate - todayDate);
			const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
			
			delivery_time = '';
		
			if(diffTime >= 2 && diffCollectionTime >=2)
			{
				var hours = "06";
				var am_pm = "AM";
				delivery_time = hours+"-"+am_pm;
			} else if(diffTime == 1)
			{
				$('#delivery_time').html($('#delivery_time1').html());
				$("#delivery_time").css("display", "block");
				return false;
			}
			if(delivery_time){
				$.ajax({
				type:'GET',
				url:'{{ route("getdeliverytime")}}',
				data:{delivery_time:delivery_time},
				success:function(response){

					var len = 0;
					if(response != null){
					len = response.length;
					}
					$("#delivery_time").empty();
					if(len > 0){

						var option1 = "<option value=''>Please Select</option>";
					// Read data and create <option >
					for(var i=0; i<len; i++){

						var start_hours = response[i]['start_hours'];
						var start_format = response[i]['start_format'];
						var end_hours = response[i]['end_hours'];
						var end_format = response[i]['end_format'];
						
						var option = "<option value='"+start_hours+'-'+start_format+"'>"+start_hours+':00'+start_format+' - '+end_hours+':00'+end_format+"</option>"; 
						$("#delivery_time").append(option); 
					}
					$("#delivery_time").prepend(option1); 
					}
				}
				});
			}
			
			$("#delivery_time").css("display", "block");
			var deliveryDateSelectedtext = $("#delivery_date option:selected").text();
			$( ".delivery-date").html('<strong>Delivery Date</strong> :' + deliveryDateSelectedtext);
		} else {
			$("#delivery_time").css("display", "none");
		}
       
       // alert(id); return false;
    });

});

$(document).ready(function(){
    $("#delivery_time").change(function()
    {
		var deliveryTimeSelectedtext = $("#delivery_time option:selected").text();
		$( ".delivery-time").html('<strong>Delivery Time</strong> :' + deliveryTimeSelectedtext);
       // alert(id); return false;
    });

});
</script> 
<!-- 
$(document).ready(function(){
    $("#delivery_time").change(function()
    {
		var deliveryTimeSelectedtext = $("#delivery_time option:selected").text();
		$( ".delivery-time").html('<strong>Delivery Time</strong> :' + deliveryTimeSelectedtext);
       // alert(id); return false;
    });

}); -->
</script>
<!-- Personal Section -->
<script>
$(document).ready(function(){
    $("#fName").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".f_Name").html('<strong>First Name</strong> :' + collectionValues);
       // alert(id); return false;
    });

});

$(document).ready(function(){
    $("#lName").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".l_Name").html('<strong>Last Name</strong> :' + collectionValues);
       // alert(id); return false;
    });

});
$(document).ready(function(){
    $("#cEmail").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".email_id").html('<strong>Email ID</strong> :' + collectionValues);
       // alert(id); return false;
    });

});
$(document).ready(function(){
    $("#pCode").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".post_code").html('<strong>Post Code</strong> :' + collectionValues);
       // alert(id); return false;
    });

});
$(document).ready(function(){
    $("#addLine").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".address_line").html('<strong>Address Line</strong> :' + collectionValues);
       // alert(id); return false;
    });

});
$(document).ready(function(){
    $("#extAdd").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".extra_address").html('<strong>Extra Address Details</strong> :' + collectionValues);
       // alert(id); return false;
    });

});
$(document).ready(function(){
    $("#mobNo").change(function()
    {
        var collectionValues=$(this).val();
        var dataString = 'collectionValues='+ collectionValues;
		$( ".phone_no").html('<strong>Phone Number</strong> :' + collectionValues);
       // alert(id); return false;
    });

});

$(document).ready(function(){
	let searchParams = new URLSearchParams(window.location.search);
	if(searchParams.has('postal_code')){
		var postal_code = searchParams.get('postal_code');
		$.ajax({
			type:'GET',
			url:'{{ route("getwebaddress")}}',
			data:{postal_code:postal_code},
			success:function(response){
				console.log(response);
				var len = 0;
				if(response != null){
					len = response['data'].length;
				}
				$("#single_address").empty();
				
				if(len > 0){
					$('#zcode').val(response['postal_code']);
					document.getElementById('postcode_error').innerHTML='<span style="color:green;margin:12px 0px;font-weight:500;float:left">Please Select Pickup Address from below</span>';
					var option1 = "<option selected value=''>Please Select</option>";
					// Read data and create <option >
					for(var i=0; i<len; i++){
						/*CNS*/
						//var id = response['data'][i].id;
						var address = response['data'][i];
						/*CNS*/

						var option = "<option value='"+address+"'>"+address+"</option>"; 
						$("#single_address").append(option); 
					
					}
					$("#single_address").prepend(option1); 
				} else {
					document.getElementById('postcode_error').innerHTML='<span style="color:red;margin:12px 0px;font-weight:500;float:left">Invalid Postcode or We are not serving in this area. Please <a href="{{url('contact-us')}}" target="_blank"><i class="fa fa-link"></i> Contact </a> with information</span>';
				}
			}
		});
	} 
 });
</script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    let stripe = Stripe("{{ env('STRIPE_KEY') }}")
    let elements = stripe.elements()
    let style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    }
    let card = elements.create('card', {style: style})
    card.mount('#card-element')

    
    let paymentMethod = null
    $('.card-form').on('submit', function (e) {
        $('button.pay').attr('disabled', true)
        if (paymentMethod) {
            return true
        }
        stripe.confirmCardSetup(
            "{{ $intent->client_secret }}",
            {
                payment_method: {
                    card: card,
                    billing_details: {name: $('.card_holder_name').val()}
                }
            }
        ).then(function (result) {
            if (result.error) {
                $('#card-errors').text(result.error.message)
                $('button.pay').removeAttr('disabled')
            } else {
                paymentMethod = result.setupIntent.payment_method
                $('.payment-method').val(paymentMethod)
                $('.card-form').submit()
            }
        })
        return false
    })
</script>

<!-- CNS -->
<script>
	function CalTotal(id){
		//alert(id);
		var qty = document.getElementById("number"+id).value;
		var cus_price = document.getElementById("cus_price"+id).value;
		//alert(qty);
		//alert(cus_price);
		total = qty*cus_price;
		document.getElementById("output"+id).innerHTML = total;
	}
</script>
@endsection
