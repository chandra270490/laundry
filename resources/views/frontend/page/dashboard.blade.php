@extends('frontend.master')
@section('frontend-styles')
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/dashboard.css') }}" />
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection
@section('body')	 
<!-- Dashboard Section -->


	 <div class="section">
	 <div class="content ">
      <div class="container">
        <div class="row">
        	
          
		  
		  @include('frontend.customer.partials.navbar')
		  
		  <div class="col-lg-9 col-md-9">
          @if(!empty($orders))
			  
		 
      @foreach($orders as $valorder)
	      
          <div class="col-lg-4 col-sm-4">
            <div class="card">
              <div class="content">
                <div class="row">
                  <div class="col-xs-12 text-centert">
                    <div class="icon-big icon-danger text-center">
                      <i class="fas fa-file-invoice"></i>
                    </div>
                    <hr/>
                    <center><a href="{{url('/invoicedetail/'.$valorder->id)}}">Invoice No #{{$valorder->order_no}}</a></center>
                  </div>
                </div>
              </div>
            </div>
          </div>
       
	  
      @endforeach
    @endif
        </div> 
        
    </div>
  </div>
    <!-- Dashboard Section End -->
<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('/contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div>
	@endsection	 
    @section('frontend_scripts')
    @endsection	