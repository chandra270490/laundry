@extends('frontend.master')
@section('meta')
	<title>Nearest Laundry - Cleaning Services</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
    <style type="text/css">
    	/* SLIDER OFFER Start */
.blog_item {margin-bottom: 30px;position: relative;}

.slideroffer .owl-theme .owl-controls .owl-nav [class*=owl-] {color: #fff;
    font-size: 21px;
    margin: 5px;
    padding: 10px 10px;
    background: #3d79aa !important;
    display: inline-block;
    cursor: pointer;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    text-transform: uppercase;
}

.card .card-body p.tt-value {font-size: 28px; color: #52b765; text-align:center}

.card {background: #fff; box-shadow: 0 6px 10px rgba(0, 0, 0, .08), 0 0 6px rgba(0, 0, 0, .05); transition: .3s transform cubic-bezier(.155, 1.105, .295, 1.12), .3s box-shadow, .3s -webkit-transform cubic-bezier(.155, 1.105, .295, 1.12); border: 0;
    border-radius: 1rem}

.card-img,.card-img-top {border-top-left-radius: calc(1rem - 1px); border-top-right-radius: calc(1rem - 1px)}

.card p {font-weight: 500;font-size: 18px;text-align: center;}

.card .card-title { overflow: hidden; font-weight: 600; font-size: 21px; text-align: center;}

.card-img-top { width: 100%; max-height: 180px; object-fit: contain; padding:20px}

.card:hover {transform: scale(1.05); box-shadow: 0 10px 20px rgba(0, 0, 0, .12), 0 4px 8px rgba(0, 0, 0, .06)}

.card p{margin-bottom:10px !important;}

@media (max-width: 768px) {.card-img-top {max-height: 250px}}
.card .btn{color:#fff}
.btn-warning { background: none #f7810a; color: #ffffff; fill: #ffffff; border: none;text-decoration: none; outline: 0;    box-shadow: -1px 6px 19px rgba(247, 129, 10, 0.25);  border-radius: 100px}

.btn-warning:hover {background: none #ff962b; color: #ffffff; box-shadow: -1px 6px 13px rgba(255, 150, 43, 0.35)}

/* SLIDER OFFER END */
    </style>
@endsection

@section('body')
<?php 
	//Convert API Data of Setting for Home Page Start
		$data = json_decode(json_encode($setting)); 
		$indexSetting=$data->response;
	//Convert API Data of Setting for Home Page End
	//Convert API Data of About Section Content for Home Page Start	
		//$dataAbout = json_decode(json_encode($aboutContent)); 
		//$aboutContent=$dataAbout->response;
	//Convert API Data of About Section Content for Home Page Start

	//Convert API Data of Services Section Content for Home Page Start	
		$dataServices = json_decode(json_encode($ServicesContent)); 
		$ServicesContent=$dataServices->response;
	//Convert API Data of Services Section Content for Home Page Start

	//Convert API Data of Services Section Content for Home Page Start	
		$blogs = json_decode(json_encode($blogs)); 
		$blogs=$blogs->response;
	//Convert API Data of Services Section Content for Home Page Start
	
	//Convert API Data of Items Section Content for Home Page Start	
		$items = json_decode(json_encode($items)); 
		$items=$items->response;
	//Convert API Data of Items Section Content for Home Page Start

?>
<!-- BANNER -->
	<div class="section banner-homepage">
		<div class="container">
			<div class="row">
				<div class="container">
					<div class="wrap-caption fadeInLeft">
						<h2 class="caption-heading_blck">Market Leading Laundry & Dry Cleaning Service</h2>
						<p class="excerpt-black">We collect, clean & return at a time and location of your choice.</p>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-6">
					<form class="header-search-form" method="GET">
						 <input type="text" name="postal_code" id="postal_code" class="form-control" placeholder="Enter Postcode">
						 <input type="button" name="search" class="btn btn-primary" onClick="getAddress();" value="Search">
						 <div id="postcode_error"></div>
					 <!-- <label for="p_submit"><span></span></label> -->
					 </form>                 
				</div>
			</div>
		</div>
	</div>	
	
	<!-- <div id="slides" class="section banner">
		<ul class="slides-container">
			<li>
				<img src="{{asset('frontend/images/banner_slider.jpg')}}" alt="">
				<div class="overlay-bg"></div>
				<div class="container">
					<div class="wrap-caption center">
						<h2 class="caption-heading">
							Professional Cleaning Services Provider
						</h2>
						<p class="excerpt">We provide best solutions for a Clean Environment. If you need any help in cleaning or maintenance.</p>	
						<a href="#" class="btn btn-primary" title="Get in Touch">Get in Touch</a>
					</div>
				</div>
			</li>
			<li>
				<img src="{{asset('frontend/images/bannerslider.jpg')}}" alt="">
				<div class="overlay-bg"></div>
				<div class="container">
					<div class="wrap-caption">
						<h2 class="caption-heading">
							We are the biggest company in the world
						</h2>
						<p class="excerpt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>	
						<a href="#" class="btn btn-primary" title="LEARN MORE">Learn More</a> <a href="#" class="btn btn-secondary" title="CONTACT US">Contact Us</a>
					</div>
				</div>
			</li>
			
		</ul>

		<nav class="slides-navigation">
			<div class="container">
				<a href="#" class="next">
					<i class="fa fa-chevron-right"></i>
				</a>
				<a href="#" class="prev">
					<i class="fa fa-chevron-left"></i>
				</a>
	      	</div>
	    </nav>
		
	</div> -->

	<!-- SERVICES -->
	<div class="section wedo pad_80 bglight">
		<div class="container">
			<div class="row gutter-wedo">
				@foreach($ServicesContent as $topservices)
				<div class="col-sm-3 col-md-3 fadeInLeft">
					<!-- BOX 1 -->
					<div class="box-image-1">
		              <div class="media">
		                <img src="{{asset("images/services/$topservices->feature_image")}}" alt="{{$topservices->name}}" class="img-responsive"> </div>
		              <div class="body">
		               <a href="{{url("service/$topservices->slug")}}" class="title">{{$topservices->name}}</a><p>
		               {{\Illuminate\Support\Str::limit($topservices->description,130, '...')}}
		                </p>
		                <a href="{{url("service/$topservices->slug")}}" class="readmore">Read More</a>
		              </div>
		            </div>
				</div>
				@endforeach
				
			</div>

			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<div class="jumbo-heading">
						<h2>We are Different in Laundry Industry</h2>
					</div>
				</div>
				<div class="col-sm-9 col-md-9">
					<p class="lead">Your clothing speaks volumes about your personality. It is your identity. At Nearest Laundry, in London, we only make sure to enhance this statement by providing you daily with fresh, neat set of clothing all washed and ironed at your doorstep.</p> 
					<p class="para">We always ensure the quality of laundry and dry cleaning. Our fleet team are efficient enough to ensure your schedule collection and delivery. On facility we have expert inspection process to make sure the quality of wash and Iron. For laundry , Dry cleaning or Ironing , we are the most reliable company in your area.</p> 

				</div>

			</div>

			

		</div>
	</div>
	 <!-- OFfer Section Start -->
	<div class="section pad bg1">
		<div class="container">
			<div class="row slideroffer">				
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">Best Laundry Deals & Offers</h2>
				</div>
				<div class="clearfix"></div>
			<div class="owl-carousel owl-theme">
            
			@foreach($ServicesContent as $index => $service_name)				
				@foreach($service_name->children as $children)
					@if(count($service_name->children) > 0)
					@foreach($children->items as $item)
						@if(count($children->items) > 0)
						<div class="blog_item">
							<div class="card shadow-sm"> <img src="{{asset("images/services/items/$item->item_image")}}" class="card-img-top" alt="...">
								<div class="card-body" style="padding:15px;">
									<h5 class="card-title">{{ $children->name }}</h5>
									<p>{{ $item->name }}</p>
									<p class="tt-value">{{ $indexSetting->currency}}{{ $item->customer_price }}</p>
									<div class="text-center my-4"> 
										<a href="{{url('/order/neworder')}}" class="btn btn-warning">Check offer</a> 
									</div>
								</div>
							</div>
						</div>
						@endif
					@endforeach
					@endif
				@endforeach
			@endforeach
			
			<!--
			<div class="blog_item">
        
                <div class="card shadow-sm"> <img src="{{url('frontend/images/dress1.png')}}" class="card-img-top" alt="...">
                    <div class="card-body" style="padding:15px;">
						<h5 class="card-title">Dress Service</h5>
                        <p>Dry Clean</p>
						<p class="tt-value">$25.00</p>
                        <div class="text-center my-4"> 
						<a href="{{url('/order/neworder')}}" class="btn btn-warning">Check offer</a> 
						</div>
                    </div>
                </div>
           
			</div>
			 <div class="blog_item">
            
                <div class="card shadow-sm"> <img src="{{url('frontend/images/duvetcover.png')}}" class="card-img-top" alt="...">
                    <div class="card-body" style="padding:15px;">
						<h5 class="card-title">Bedding</h5>
                        <p>Bed Set Wash</p>
						<p class="tt-value">$2.00</p>
                        <div class="text-center my-4"> 
						<a href="{{url('/order/neworder')}}" class="btn btn-warning">Check offer</a> 
						</div>
                    </div>
                </div>
            
			</div>
			 <div class="blog_item">
           
                <div class="card shadow-sm"> <img src="{{url('frontend/images/iron.png')}}" class="card-img-top" alt="...">
                    <div class="card-body" style="padding:15px;">
						<h5 class="card-title">Ironing Service</h5>
                        <p>Iron and Fold</p>
						<p class="tt-value">$10.00</p>
                        <div class="text-center my-4"> 
						<a href="{{url('/order/neworder')}}" class="btn btn-warning">Check offer</a> 
						</div>
                    </div>
                </div>
				
            
			</div>
			-->
				
			</div>
			</div>
		</div>
	</div>	
	<!-- OFfer Section END -->
	<!-- PROCESS -->
	<div class="section pad">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<h2 class="section-heading">
						How We Works
					</h2>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-3 col-md-3">
					<div class="box-icon-2 process-arrow">
						<div class="icon">
							<!-- <div class="rsi rsi-phone"></div> -->
							<i class="far fa-calendar-alt fa-xs"></i>
							<div class="number">1</div>
							<div class="heading">Select Pickup<br> Date</div>
						</div>							
					</div>						
				</div>
				<div class="col-sm-3 col-md-3" id="tmgn">
					<div class="topmargn">
						<div class="box-icon-2 dotted-arrow">
							<div class="icon">
								<i class="fas fa-truck-pickup fa-xs"></i>
								<div class="number">2</div>
								<div class="heading">We Pickup <br>Your Clothes</div>
							</div>						
						</div>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">					
					<div class="box-icon-2 process-arrow">
						<div class="icon">
							<i class="far fa-dryer fa-xs"></i>
							<div class="number">3</div>
							<div class="heading">We Clean <br>Your Clothes</div>
						</div>						
					</div>					
				</div>
				<div class="col-sm-3 col-md-3" id="tmgn">
					<div class="topmargn">
						<div class="box-icon-2">
							<div class="icon">
								<i class="fas fa-tshirt fa-xs"></i>
								<div class="number">4</div>
								<div class="heading"> Return Your <br> Clean Clothes </div>
							</div>						
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-12 col-md-12 text-center">
					<div class="margin-bottom-50"></div>
					<a href="{{url('/order/neworder')}}" class="btn btn-primary" title="Learn More">Book Your Order</a>
				</div>

				<div class="top-btn"><i class="fas fa-arrow-up"></i></div>			
				<!-- <div class="col-sm-3 col-md-3">
					<div class="box-icon-2 process-arrow">
						<div class="icon">
							<div class="rsi rsi-phone"></div>
							<div class="number">1</div>
						</div>
						<div class="heading">
							Book Your Order
						</div>
						<p class="text-center">Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="box-icon-2 process-arrow">
						<div class="icon">
							<div class="rsi rsi-calendar-1"></div>
							<div class="number">2</div>
						</div>
						<div class="heading">
							Schedule it
						</div>
						<p class="text-center">Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="box-icon-2 process-arrow">
						<div class="icon">
							<div class="rsi rsi-cleaning"></div>
							<div class="number">3</div>
						</div>
						<div class="heading">
							The Cleaning
						</div>
						<p class="text-center">Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
					</div>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="box-icon-2">
						<div class="icon">
							<div class="rsi rsi-payment"></div>
							<div class="number">4</div>
						</div>
						<div class="heading">
							Easy Payment
						</div>
						<p class="text-center">Contrary to popular belief, Lorem Ipsum is not simply random text.</p>
					</div>
				</div> -->				
			</div>
		</div>
	</div>
	 
	<!-- ABOUT -->
	<div class="section pad">
		<div class="container">
			
			<div class="row">
				<div class="col-sm-12 col-md-6">
					<h3>About Us</h3>
					{{\Illuminate\Support\Str::limit($aboutContent['description'],300, '...')}}
					<div class="margin-bottom-50"></div>
					<a href="{{url("$aboutContent[slug]")}}" class="btn btn-primary" title="Learn More">Learn More</a>
				</div>
				
			</div>
			<div class="sideright-img" style="background-image: url({{asset("images/pages/$aboutContent[feature_image]")}}); margin:20px 0 0 0;">
				<img src="{{asset("images/pages/$aboutContent[feature_image]")}}" alt="">
			</div>
		</div>
	</div>
	 
	<!-- FAN FACS -->
	<!-- <div class="section pad cta-bg-1">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<h2 class="cta-title-1">Save your time</h2>
					<h2 class="cta-title-2">We make it easy</h2>
					<div class="margin-bottom-70"></div>
				</div>
				<div class="clearfix"></div>
				<div class="col-sm-3 col-md-2">
					<div class="counter-1">
			            <div class="counter-number">
			              341
			            </div>
			            <div class="counter-title">Project finished</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-2">
					<div class="counter-1">
			            <div class="counter-number">
			              2108
			            </div>
			            <div class="counter-title">Clients a year</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-2">
					<div class="counter-1">
			            <div class="counter-number">
			              13
			            </div>
			            <div class="counter-title">Years of experience</div>
		          	</div>
				</div>
				<div class="col-sm-3 col-md-2">
					<div class="counter-1">
			            <div class="counter-number">
			              100%
			            </div>
			            <div class="counter-title">Satisfied customers</div>
		          	</div>
				</div>

			</div>
		</div>
	</div> -->
	
	<!-- TESTIMONIALS --> 
	<div class="section testimony">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-12 col-md-12">

					<div class="row">
						<div class="col-sm-12 col-md-12">
							<h2 class="section-heading">
								What People Says
							</h2>
						</div>
					</div>

					<div id="owl-testimony">
						@foreach($testimonialContents as $testimonial)
						<div class="item">
							<div class="testimonial-1">
				              <div class="media"><img src="{{asset('frontend/images/testimonial-user-icon.png')}}" alt="Paul Juwaal" class="img-responsive"></div>
				              <div class="body">
				                <div class="title">{{$testimonial['test_user_name']}}</div>
				                <div class="company">{{$testimonial['test_user_role']}}</div>
				                <p>{{$testimonial['description']}}</p>
				              </div>
				            </div>
						</div>
						@endforeach
						
						
					</div>
					
				</div>

			</div>
		</div>
	</div>
	<!-- Blog Section Start -->
	@if(isset($blogs))
	@if(count($blogs)>=1)
	<section class="user-blog bglight">
        <div class="container">
            <div class="heading">
                Our Blog 
            </div>
            <div id="demo1">
                <div class="span12">
                    <div id="owl-demo1" class="owl-carousel">
                    	@foreach($blogs as $blog)
                        <div class="item">
                            <div class="blog-grid">
                                <div class="img-date">
                                    <img src="{{url("images/blogs/$blog->feature_image")}}">
                                    <div class="date-blog">
                                    	{{date('M', strtotime($blog->created_at))}}<br/>
                                    	{{date('d', strtotime($blog->created_at))}}
                                    </div>
                                </div>
                                <div class="discretion-blog">
                                    <h4 class="bold">{{\Illuminate\Support\Str::limit($blog->title,62, '...')}}</h4>
                                    <p>{{\Illuminate\Support\Str::limit($blog->description,130, '...')}}</p>
                                    <a class="btn" href="{{url("blog/$blog->slug")}}">Read more</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>   

                    <!-- <div class="customNavigation">
                        <a class="btn prev"><i class="fal fa-angle-left"></i></a>
                        <a class="btn next"><i class="fal fa-angle-right"></i></a>
                    </div>   -->             
                </div>
            </div>
        </div>
        </div>
    </section>
    @endif
    @endif
	<!-- Blog Section END -->
	<!-- Application Section --> 
	@if(isset($indexSetting->android_app_link) || isset($indexSetting->ios_app_link))
	<div class="section pad bg1">
		<div class="container">			
			<div class="col-sm-12 col-md-12">
				<h2 class="section-heading">On Demand Laundry Apps</h2>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6 col-md-6">
				<div class="mobil-img">
                    <img src="{{ asset('frontend/images/mobile-new.png') }}" alt="Mobile Image" class="img-fluid">
                </div>
			</div>
			<div class="col-sm-6 col-md-6 mt_120">
			<h3>Download Our Free Mobile App </h3>
			<ul class="feature-list">
				<li><span><i class="fa fa-check"></i></span>Free Same-day collection</li>
				<li><span><i class="fa fa-check"></i></span>Delivery in less then 24 hours</li>
				<li><span><i class="fa fa-check"></i></span>Standard wash for just £14</li>
			</ul>
			@if(isset($indexSetting->android_app_link))
			<a class="btn btn-google" href="{{$indexSetting->android_app_link}}" title="Google Play"><img src="{{asset('frontend/images/play-store-icon.png')}}"> Google Play</a>
			@endif
			@if(isset($indexSetting->ios_app_link))
			<a class="btn btn-ios btn-apple" href="{{$indexSetting->ios_app_link}}" title="App Store"><i class="fab fa-apple"></i> App Store</a>
			@endif
			</div>
		</div>
	</div>
	@endif
	
	<!-- PARTNERS -->
	<div class="section partner bglight">
		<div class="container">
			
			<div class="row">
				
				<div class="col-sm-4 col-md-4">
					<div class="client-img">
						<a href="#"><img src="{{asset('frontend/images/partners-1.png')}}" alt="" class="img-responsive"></a>
					</div>
				</div>

				<div class="col-sm-4 col-md-4">
					<div class="client-img">
						<a href="#"><img src="{{asset('frontend/images/partners-2.png')}}" alt="" class="img-responsive"></a>
					</div>
				</div>

				<div class="col-sm-4 col-md-4">
					<div class="client-img">
						<a href="#"><img src="{{asset('frontend/images/partners-3.png')}}" alt="" class="img-responsive"></a>
					</div>
				</div>

				

				
			</div>
		</div>
	</div>

	<!-- CONTACT -->
	<div class="section contact pad">
		<div class="container">
			
			<div class="col-sm-12 col-md-12">
				<h2 class="section-heading white">
					Get In Touch
				</h2>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6 col-md-6">
				<div class="contact-info">
					<p>{{$indexSetting->footer_about_us}}</p>
					<p>{{$indexSetting->location_address1}}</p>
					<p><a href="#">{{$indexSetting->primary_email}}</a></p>
					<p class="phone"><i class="fa fa-phone"></i> {{$indexSetting->primary_phone}}</p>
					<p>Working Days: {{$indexSetting->working_days}} <br>Working Hours: {{$indexSetting->working_time}}</p>
				</div>
			</div>
			<!--
			<div class="col-sm-6 col-md-6">
				<form action="#" class="form-contact" id="contactForm" data-toggle="validator">
					<div class="form-group">
						<input type="text" class="form-control" id="p_name" placeholder="Full Name..." required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<input type="tel" class="form-control" id="p_email" placeholder="Enter Phone Number..." required="">
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						 <textarea id="p_message" class="form-control" rows="6" placeholder="Write message"></textarea>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group">
						<div id="success"></div>
						<button type="submit" class="btn btn-primary">Enquiry Now</button>
					</div>
				</form>

			</div>
			-->
			<div class="col-sm-6 col-md-6">
				<form action="#" method="post" class="form-contact">
				@csrf
				<div class="form-group">
				<input type="text" class="form-control" id="p_name" name="name" placeholder="Full Name..." required="">
				<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				<input type="tel" class="form-control" id="p_phone" name="phone" placeholder="Enter Phone / Whatsapp Number..." required="">
				<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				<textarea id="p_message" class="form-control" rows="6" name="message" placeholder="Write message"></textarea>
				<div class="help-block with-errors"></div>
				</div>
				<div class="form-group">
				<div id="success"></div>
				<button type="submit" class="btn btn-primary">Enquiry Now</button>
				</div>
				</form>
				
			</div>
		</div>
	</div>
@endsection
@section('frontend_scripts')
<!-- Address Details Jquery Start-->
<script>
	 function getAddress()
	 {
		 var postal_code = $('#postal_code').val();
			 $.ajax({
			 type:'GET',
			 url:'{{ route("getwebaddress")}}',
			 data:{postal_code:postal_code},
			 success:function(response){
			 console.log(response);
			 var len = 0;
			 if(response != null){
			 len = response['data'].length;
			 }
			 if(len > 0){
			 /*CNS*/
			 window.location.href = "/laundry-bita-version/order/neworder?postal_code="+response['postal_code'];
			 /*CNS*/
			 
			 } else{
			 document.getElementById('postcode_error').innerHTML='<span style="color:red;margin:12px 0px;font-weight:500;float:left">Invalid Postcode or We are not serving in this area. Please <a href="contact-us" target="_blank"><i class="fa fa-link"></i> Contact </a> with information</span>';
			 return false;
			 }
		 
		 }
		 });
	 }
 </script>

 <!-- Back To Top Script -->
<script>
     $(window).load(function (){
		$('.top-btn').fadeOut();
	});

	

	$(window).scroll(function () {
		if ($(this).scrollTop() != 0) {
				$('.top-btn').fadeIn();
			}
		else {
				$('.top-btn').fadeOut();
		}
	});

	

	$('.top-btn').click(function(){
		$("html, body").animate({ scrollTop: 0 }, 900);
		return false;
	});

	

</script>
<!-- Back To Top Script -->
 @endsection