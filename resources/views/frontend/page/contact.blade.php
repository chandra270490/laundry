@extends('frontend.master')
@section('meta')
	<title>Nearest Laundry - Contact Us</title>
    <meta name="description" content="Nearest Laundry - Contact Us.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Contact</div>
					<ol class="breadcrumb">
						<li><a href="{{ url('/')}}">Home</a></li>
						<li class="active">Contact</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad">
		<div class="container">
			
			<div class="row">

				<div class="col-sm-4 col-md-4">
					<div class="widget">
						<h4 class="lead">Feel free to leave us a message using the contact form and we will get back to you within 24 hours.</h4>
					</div>
					<div class="widget contact-info-sidebar">
						<div class="widget-title">
							Contact Info
						</div>
						<ul class="list-info">
							<li>
								<div class="info-icon">
									<span class="fa fa-map-marker"></span>
								</div>
								<div class="info-text">@php
							$address = '';
							if(isset( $setting['location_address1']))
								$address .= $setting['location_address1'];
							if(isset( $setting['city']))
								$address .= ", ".$setting['city'];
							if(isset( $setting['state']))
								$address .= ", ".$setting['state'];
							if(isset( $setting['zip_code']))
								$address .= ", ".$setting['zip_code'];

							@endphp
								{{ $address }}</div> </li>
							<li>
								<div class="info-icon">
									<span class="fa fa-phone"></span>
								</div>
								<div class="info-text">{{ $setting['primary_phone'] }}</div>
							</li>
							<li>
								<div class="info-icon">
									<span class="fa fa-envelope"></span>
								</div>
								<div class="info-text">{{ $setting['primary_email'] }}</div>
							</li>
							<li>
								<div class="contact-social-icons">
                        <h4>Follow us on</h4>
                    <ul class="social-icons">
                        <li><a href="https://api.whatsapp.com/send/?phone={{$setting['social_whatsapp_link']}}" class="social-icon"> <i class="fab fa-whatsapp"></i></a></li>
                      <li><a href="{{$setting['social_fb_link']}}" class="social-icon"> <i class="fab fa-facebook"></i></a></li>
                      <li><a href="{{$setting['social_twitter_link']}}" class="social-icon"> <i class="fab fa-twitter"></i></a></li>
                      <li><a href="{{$setting['social_linkedin_link']}}" class="social-icon"> <i class="fab fa-linkedin"></i></a></li>
                      <li><a href="{{$setting['social_instagram_link']}}" class="social-icon"> <i class="fab fa-instagram"></i></a></li>
                    </ul>
                    </div>	
							
					</div> 
				</div>
				<div class="col-sm-8 col-md-8">
					
					<div class="free-quote">
						<h2 class="section-heading-2">
							Get free quote
						</h2>
						@if (session('message'))
						<div class="alert alert-success">
							{{ session('message') }}
						</div>
						@endif
						<form action="{{ route('contactus')}}" method="post" class="form-contact" id="contactForm" data-toggle="validator" novalidate="true">
						@csrf
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_name" placeholder="Full Name..." required="">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="email" class="form-control" id="p_email" placeholder="Email..." required="">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_address" placeholder="Address...">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-6 col-md-6">
								<div class="form-group">
									<input type="text" class="form-control" id="p_phone" placeholder="Phone...">
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md-12">
								<div class="form-group">
									 <textarea id="p_message" class="form-control" rows="6" placeholder="Write message"></textarea>
									<div class="help-block with-errors"></div>
								</div>
							</div>
							<div class="col-sm-12 col-md-12">
								<div class="form-group">
									<div id="success"></div>
									<button type="submit" class="btn btn-secondary">Enquiry Now</button>
								</div>
							</div>
						</div>
					</form>
					</div>
				</div>
			</div>

		</div>
	</div>

	 


	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="#" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>

			</div>
		</div>
	</div
@endsection