@php
	
	$data = json_decode(json_encode($blogs));
	$blogs=$data->response;

@endphp
@extends('frontend.master')
@section('meta')
	<title>Blogs Page</title>
    <meta name="description" content="Nearest Laundry - Cleaning Services.">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">Blogs</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">Home</a></li>
						<li class="active">Blogs</li>
					</ol>
					@if (session('error'))
				          <div class="alert alert-danger text-center">
				              * {!! session('error') !!}
				          </div>
				    @endif
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad_80">
		<div class="container">			
			<div class="row">
				@foreach($blogs as $blog)
				<div class="col-sm-6 col-md-4">
					<!-- BOX 1 -->
					<div class="box-news-1">
						<div class="media gbr">
							<img src="{{asset("images/blogs/$blog->feature_image")}}" alt="{{$blog->title}}" class="img-responsive">
						</div>
						<div class="body">
							<div class="title"><a href="{{url("blog/$blog->slug")}}" title="{{$blog->title}}">{{\Illuminate\Support\Str::limit($blog->title,62, '...')}}</a></div>
							<div class="meta">
								<span class="date"><i class="fa fa-clock-o"></i> {{date('Y-M-d', strtotime($blog->created_at))}}</span>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<ul class="pagination">
						<li class="active"><a href="#">1</a></li>
						
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="contact.html" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>
			</div>
		</div>
	</div>	
@endsection