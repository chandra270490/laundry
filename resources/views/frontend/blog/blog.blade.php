@extends('frontend.master')
@section('meta')
	<title>{{$blog['title']}}</title>
    <meta name="description" content="{{$blog['meta_description']}}">
    <meta name="keywords" content="cleaning, laundry, blind cleaning, window cleaning, washing, floor cleaning, trash treatment, extra shiny, cloch ironing">
    <style type="text/css">
    	.blog-contact-con{float:left;width:40px;height:40px;border-radius:50px;text-align:center;  font-size:30px;	box-shadow: 2px 2px 3px #999;line-height: 40px !important;}
    	.whatsapp-bg{background-color:#25d366;color:#FFF;}
    	.messanger-bg{background: #2196F3;color: #fff;}
    	.whatsapp-text{margin-top: 8px;    float: left;margin-left: 9px;    font-weight: bold;}    </style>
@endsection

@section('body')
<!-- BANNER -->
	<div class="section banner-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="title-page">{{$blog['title']}}</div>
					<ol class="breadcrumb">
						<li><a href="{{url('/')}}">Home</a></li>
						<li><a href="{{url('blogs')}}">Blogs</a></li>
						<li class="active">{{$blog['title']}}</li>
					</ol>
				</div>
			</div>
		</div>
	</div>
	 
	<!-- ABOUT FEATURE -->
	<div class="section pad_80">
		<div class="container">
			
			<div class="row">
				@include('frontend.inc.sidebar')
				<div class="col-sm-9 col-md-9">
					
					<div class="single-news">
						<div class="image">
							<img src="{{asset("images/blogs/$blog[feature_image]")}}" alt="" class="img-responsive"> 
						</div>
						<h2 class="blok-title">
							{{$blog['title']}}
						</h2>
						<div class="meta">
							<div class="meta-date"><i class="fa fa-clock-o"></i> {{date('M-d-Y', strtotime($blog["created_at"]))}}</div>
							<div class="meta-category"></div>
							<div class="meta-comment"></div>
						</div>
						<p>{!!$blog["description"]!!}</p>
				 	</div>				
				</div>
			</div>
		</div>
	</div>
	<!-- CTA -->
	<div class="section pad cta-bgc">
		<div class="container">
			<div class="row">				
				<div class="col-sm-12 col-md-12">
					<div class="cta-content">
						<h3 class="cta-title-3">Ready to book your cleaning?</h3>
					</div>
					<div class="cta-action"><a href="{{url('contact-us')}}" class="btn btn-white" title="Learn More">Contact Us</a></div>
				</div>
			</div>
		</div>
	</div>	
@endsection