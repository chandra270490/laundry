<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Laundry | Dashboard</title>
  @include('partials.styles')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
@include('partials.header')
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
@include('partials.sidebar')
</aside>
<!--Sidebar End -->
<div class="content-wrapper">
    @yield('content')
</div>
@include('partials.footer')
</div>
@include('partials.scripts')
</body>
</html>