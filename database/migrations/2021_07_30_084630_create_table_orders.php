<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->text('address')->nullable();
            $table->string('extra_address')->nullable();
            $table->string('services')->nullable();
            $table->string('extra_service_request')->nullable();
            $table->date('collection_date')->nullable();
            $table->string('collection_time')->nullable();
            $table->string('collection_instruction')->nullable();
            $table->date('delivery_date')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('delivery_instruction')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email_id')->nullable();
            $table->string('postcode')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('payment_gateway_with_card')->nullable();
            $table->Integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
