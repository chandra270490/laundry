<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableOrdersWithNewColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('order_no')->nullable();
            $table->date('order_date')->nullable();
            $table->Integer('order_frequency')->default(1);
            $table->string('facility_name')->nullable();
            $table->string('ready_by')->nullable();
            $table->string('penalty')->nullable();
            $table->Integer('invoice_status')->default(1);
            $table->Integer('payment_status')->default(0);
            $table->string('voucher_code')->nullable();
            $table->string('item_description')->nullable();
            $table->Integer('sms_or_mail')->default(0);
            $table->Integer('customer_type')->default(0);
            $table->string('job_final')->nullable();
            $table->string('review_email')->nullable();
            $table->string('trustpiolet_review')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('order_no');
            $table->dropColumn('extra_service_request');
            $table->dropColumn('order_date');
            $table->dropColumn('order_frequency');
            $table->dropColumn('facility_name');
            $table->dropColumn('ready_by');
            $table->dropColumn('penalty');
            $table->dropColumn('invoice_status');
            $table->dropColumn('payment_status');
            $table->dropColumn('voucher_code');
            $table->dropColumn('item_description');
            $table->dropColumn('sms_or_mail');
            $table->dropColumn('customer_type');
            $table->dropColumn('job_final');
            $table->dropColumn('review_email');
            $table->dropColumn('trustpiolet_review');
        });
    }
}
