<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('logo')->nullable();
            $table->string('website_name')->nullable();
            $table->string('website_url')->nullable();
            $table->string('page_title')->nullable();
            $table->string('timezone')->nullable();
            $table->string('language')->nullable();
            $table->string('date_time_format')->nullable();
            $table->string('social_fb_link')->nullable();
            $table->string('social_twitter_link')->nullable();
            $table->string('social_linkedin_link')->nullable();
            $table->string('social_instagram_link')->nullable();
            $table->string('social_youtube_link')->nullable();
            $table->string('social_pinterest_link')->nullable();
            $table->string('social_tumblr_link')->nullable();
            $table->string('social_whatsapp_link')->nullable();
            $table->string('primary_phone')->nullable();
            $table->string('secondry_phone')->nullable();
            $table->string('primary_email')->nullable();
            $table->string('secondry_email')->nullable();
            $table->string('location_address1')->nullable();
            $table->string('location_address2')->nullable();
            $table->longText('footer_about_us')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('location_map_link')->nullable();
            $table->string('currency')->nullable();
            $table->string('working_time')->nullable();
            $table->string('working_days')->nullable();
            $table->string('copyright_text')->nullable();
            $table->string('android_app_link')->nullable();
            $table->string('ios_app_link')->nullable();
            $table->longText('script_thired_party_api')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
