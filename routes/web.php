<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NotificationController;

use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontpageController@index');
Route::middleware(['checkapitoken'])->group(function () {
Route::get('/dashboard','frontpageController@dashboard')->name('customerdashboard');
Route::get('/invoicedetail/{id}','frontpageController@invoicedetail')->name('invoicedetail');
Route::get('/customer/profile','FrontCustomerController@UserProfile')->name('profile');
Route::post('/customer/updateprofile','FrontCustomerController@UserUpdateProfile')->name('customerupdateprofile');
Route::get('/customer/orders','FrontCustomerController@orders')->name('orders');
Route::get('/customer/invoices','FrontCustomerController@invoices')->name('invoices');
});
Route::get('/{slug}/{post_type?}','frontpageController@page')->where('post_type', 'page|testimonial|partner');
Route::get('/service/{slug}','frontpageController@serviceDetails');
Route::get('/blog/{slug}','frontpageController@blogDetails');
Route::get('/order/neworder','FrontOrderController@showOrderFrom')->name('order');
Route::get('/order/getaddress','FrontOrderController@getAddress')->name('getwebaddress');
Route::get('/order/getdeliverydate','FrontOrderController@getDeliveryDate')->name('getdelivery');
Route::get('/order/getdeliverydatetime','FrontOrderController@getDeliveryTime')->name('getdeliverytime');
Route::post('/saveorder','FrontOrderController@saveOrder')->name('saveorder');
Route::get('/order/getcollectiontime','FrontOrderController@getCollectionTime')->name('getcollectiontime');
Route::get('/price','frontpageController@page');




Route::get('/admin/login', function () {
    return view('auth.login');
})->name('login');

Route::get('/admin', function () {
    return redirect()->to('login');
})->name('login');

Auth::routes();
Route::get('admin/logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('admin/login');
})->name('logout');

Route::get('/customer/register','AuthController@Register')->name('show-register');
Route::get('/customer/login','AuthController@Login')->name('show-login');

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function(){
    //Auth::routes();
   
    Route::get('/dashboard', 'Admin\DashboardController@index')->name('dashboard');

    /**CNS Notifications*/
    Route::get('/HideNotify', 'NotificationController@HideNotify')->name('HideNotify');
    /**CNS */
	
    Route::post('/setrole', 'Admin\DashboardController@setRole')->name('setrole');
    Route::resource('/settings', 'Admin\SettingsController');
    Route::resource('/services', 'Admin\ServicesController');
    Route::get('/getchildservice', 'Admin\ItemController@getSubService')
        ->name('getsubservice');
    Route::get('/services/delete/{id}', 'Admin\ServicesController@destroy')
        ->name('services.destroy');
    Route::resource('/servicearea', 'Admin\ServiceAreaController');
    Route::get('/servicearea/delete/{id}', 'Admin\ServiceAreaController@destroy')
        ->name('servicearea.destroy');
    Route::resource('/pages', 'Admin\PagesController');
    Route::get('/pages/delete/{id}', 'Admin\PagesController@destroy')
        ->name('pages.destroy');
    Route::resource('/blogs', 'Admin\BlogsController');
    Route::get('/blogs/delete/{id}', 'Admin\BlogsController@destroy')
        ->name('blogs.destroy');
    Route::resource('/orders', 'Admin\OrdersController');
    Route::resource('/users', 'Admin\UsersController');

    Route::get('/assignrole/{id}', 'Admin\RoleUserController@assignRole')->name('assignrole');
    Route::post('/saverole', 'Admin\RoleUserController@saveRole')->name('saverole');

    Route::get('modules/assignroletomodule', 'Admin\RoleModuleController@assignRoleToModule')->name('assignroletomodule');
    Route::post('modules/saveroletomodule', 'Admin\RoleModuleController@saveRoleToModule')->name('saveroletomodule');
    
    Route::get('/user/showprofile/{id}', 'Admin\UsersController@showProfile')->name('showprofile');
    Route::post('/user/updateprofile', 'Admin\UsersController@updateProfile')->name('updateprofile');
    
    Route::resource('/items', 'Admin\ItemController');
    Route::get('/items/delete/{id}', 'Admin\ItemController@destroy')
        ->name('items.destroy');

    Route::resource('/testimonials', 'Admin\TestimonialsController');
    Route::get('/testimonials/delete/{id}', 'Admin\TestimonialsController@destroy')
    ->name('testimonials.destroy');

    Route::resource('/partners', 'Admin\PartnerLogoController');
    Route::get('/partners/delete/{id}', 'Admin\PartnerLogoController@destroy')
    ->name('partners.destroy');
    // Route::get('/subscription/create', ['as'=>'home','uses'=>'Admin\SubscriptionController@index'])->name('subscription.create');
// Route::post('order-post', ['as'=>'order-post','uses'=>'Admin\SubscriptionController@orderPost']);
});