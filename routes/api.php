<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'API\AuthController@login')->name('apilogin');
    Route::post('signup', 'API\AuthController@signup')->name('apisignup');
  
    Route::group([
      'middleware' => ['checkapitoken']
    ], function() {
        Route::get('logout', 'API\AuthController@logout')->name('logoutuser');
        Route::get('users', 'API\AuthController@user');
    });
   
});
Route::get('services/{slug?}', 'API\ApiPageController@services');
//Route::get('services/{slug}', 'API\ApiPageController@serviceDetail');
Route::get('settings', 'API\ApiPageController@settings');
Route::get('blogs/{slug?}', 'API\ApiPageController@blogs');
Route::get('items', 'API\ApiPageController@items');
Route::get('pages/{slug?}/{post_type?}', 'API\ApiPageController@pages');

Route::get('nav/pages/getmenupages/{menupage}','API\ApiPageController@getMenuPages');

Route::post('contactus','API\ApiPageController@sendMail')->name('contactus');

Route::get('getaddress', 'API\ApiPageController@getAddress')->name('getaddress');

Route::get('slots/{start_time?}/{start_format?}', 'API\ApiOrderController@index')->name('slots');
