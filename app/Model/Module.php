<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name','status','created_at','updated_at'
    ];

    public function allRoles()
    {
        return $this->belongsToMany(Role::class, 'module_roles');
    }
}
