<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleModule extends Model
{
    protected $table='module_roles';
    public $timestamps = true;
    protected $fillable = [
        'role_id','module_id','created_at','updated_at'
    ];
}
