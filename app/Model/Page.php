<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'name', 'title', 'slug', 'feature_image', 'media_link','post_type','test_user_name','test_user_role' ,'description', 'add_top_navbar', 'add_footer_navbar', 'status', 'meta_title', 'meta_description', 'canonical_url', 'created_by', 'created_at', 'updated_at','post_types'
    ];
}
