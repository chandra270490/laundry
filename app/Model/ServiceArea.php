<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ServiceArea extends Model
{
    protected $table = 'service_area';

    protected $fillable = [
        'name', 'postal_code_state', 'postal_code_city', 'address', 'status', 'created_at', 'updated_at'
    ];
}
