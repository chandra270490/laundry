<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'name', 'title', 'slug', 'tags','feature_image', 'description', 'status','keywords', 'meta_description', 'created_by', 'created_at', 'updated_at'
    ];
}
