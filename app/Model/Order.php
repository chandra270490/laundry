<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'address', 'extra_address', 'services', 'extra_service_request', 'collection_date', 'collection_time', 'collection_instruction', 'delivery_date', 'delivery_time', 'delivery_instruction', 'first_name', 'last_name', 'email_id', 'postcode', 'mobile_no', 'payment_gateway_with_card', 'status', 'created_at', 'updated_at','total_payment','order_no','facility_status'
    ];
}
