<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable = [
        'name', 'title', 'slug', 'icon', 'parent_id', 'price', 'feature_image', 'media_link', 'description', 'add_top_navbar', 'add_footer_navbar', 'meta_title', 'meta_description', 'canonical_url', 'status', 'created_at', 'updated_at'
    ];

    public function children()
    {
        return $this->hasMany(Service::class, 'parent_id');
    }

    public function items()
    {
        return $this->hasMany(Item::class, 'service_id');
    }

    public function parent()
    {
        return $this->belongsTo(Service::class, 'parent_id');
    }

}
