<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
        'service_id','item_no','name','customer_price','facility_price','status','created_at','updated_at','item_image'
    ];

    public function service()
    {
        return $this->belongsTo('App\Model\Service','service_id')->select('id','name');
    }

}
