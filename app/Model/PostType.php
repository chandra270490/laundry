<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PostType extends Model
{
    protected $table = 'post_types';
    protected $fillable = [
        'name', 'slug', 'status','created_at', 'updated_at'
    ];
    
}