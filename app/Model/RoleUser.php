<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $table='user_roles';
    protected $fillable = [
        'role_id','user_id','created_at','updated_at'
    ];
}
