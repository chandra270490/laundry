<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Slot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_hours','start_format' ,'end_hours','end_format' ,'created_at', 'updated_at'
    ];
}
