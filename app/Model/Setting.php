<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'logo', 'website_name', 'website_url', 'page_title', 'timezone', 'language', 'date_time_format', 'social_fb_link', 'social_twitter_link', 'social_linkedin_link', 'social_instagram_link', 'social_youtube_link', 'social_pinterest_link', 'social_tumblr_link', 'social_whatsapp_link', 'primary_phone', 'secondary_phone', 'primary_email', 'secondary_email', 'location_address1', 'location_address2', 'city', 'state', 'country', 'zip_code', 'location_map_link', 'currency', 'working_time', 'working_days', 'copyright_text', 'android_app_link', 'ios_app_link', 'script_thired_party_api'
    ];
}
