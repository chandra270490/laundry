<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\helpers\ApiResponse;
use Validator;
use Session;
use Redirect;

class ContactController extends Controller
{
    public function contactUs(Request $request)
    {
       
        $NormalArray =  [
            'name'=>$request->name, 
            'phone' => $request->phone  ,
            'message'=>$request->message
        ];
        $EmailArray =[];
        if(isset($request->email))
            $EmailArray = ['email'=>$request->email];
        $SubjectArray=[];
        if(isset($request->email))
            $SubjectArray = ['subject'=>$request->subject];
       
        $totalFieldArray =array_merge($NormalArray,$EmailArray,$SubjectArray);
        $email_sent =  Http::post(env("API_URL", "")."contactus/",$totalFieldArray)->json();
  
        if (!isset($email_sent['response'])) {
             // redirect
         Session::flash('message', 'There is problem in sending email. Please try again');
         return Redirect::to('/');
        }

        Session::flash('message', 'Thanks For contacting us. We will get back to you soon.');
        return Redirect::to('/');
  
    }
}
