<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Model\Order;

use App\Notifications\OffersNotification;
use App\Notifications\OrdersNotification;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
  
    public function index()
    {
        return view('product');
    }
    
    /*
    public function sendOfferNotification() {
        $userSchema = User::first();
  
        $offerData = [
            'name' => 'BOGO',
            'body' => 'You received an offer.',
            'thanks' => 'Thank you',
            'offerText' => 'Check out the offer',
            'offerUrl' => url('/'),
            'offer_id' => 007
        ];
  
        //Notification::send($userSchema, new OffersNotification($offerData));
        $userSchema->notify(new OffersNotification($offerData));
    }*/

    public function sendOrderNotification() {
        $userSchema = User::first();
        $orders = Order::get();

        $orders = json_decode($orders);

        $orderData = $orders;

        $userSchema->notify(new OrdersNotification($orderData));
    }

    public function HideNotify(Request $request)
    {
        $id = $request->id;
        $notification = auth()->user()->notifications()->where('id', $id)->first();

        if ($notification) {
            $notification->markAsRead();
            //return redirect($notification->data['link']);
        }
    }
}
