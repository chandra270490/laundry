<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //Show Login Form
    public function Login()
    {
        return view('frontend.auth.login');
    }
    //Show Register Form
    public function Register()
    {
        return view('frontend.auth.register');
    }
}
