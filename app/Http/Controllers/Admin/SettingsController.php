<?php

namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use App\Model\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Return all settings
        $setting = Setting::get()->first();
        return view('admin.settings.update')->with(['setting'=>$setting]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $logo =$request->old_logo;
        //File upload
         if($request->hasFile('logo')){
            $photo = $request->file('logo');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/settings'), $new);
            $logo = $new;
        };
        
       //Create the Settings Array
       $settingArray = array(
           'logo'=>$logo,
           'website_name'=>$request->website_name,
           'website_url'=>$request->website_url,
           'page_title'=>$request->page_title,
           'timezone'=>$request->timezone,
           'language'=>$request->language,
           'footer_about_us'=>$request->footer_about_us,
           'date_time_format'=>$request->date_time_format,
           'social_fb_link'=>$request->social_fb_link,
           'social_twitter_link'=>$request->social_twitter_link,
           'social_linkedin_link'=>$request->social_linkedin_link,
           'social_instagram_link'=>$request->social_instagram_link,
           'social_youtube_link'=>$request->social_youtube_link,
           'social_pinterest_link'=>$request->social_pinterest_link,
           'social_tumblr_link'=>$request->social_tumblr_link,
           'social_whatsapp_link'=>$request->social_whatsapp_link,
           'primary_phone'=>$request->primary_phone,
           'primary_email'=>$request->primary_email,
           'secondary_phone'=>$request->secondary_phone,
           'secondary_email'=>$request->secondary_email,
           'location_address1'=>$request->location_address1,
           'location_address2'=>$request->location_address2,
           'city'=>$request->city,
           'state'=>$request->state,
           'country'=>$request->country,
           'zip_code'=>$request->zip_code,
           'location_map_link'=>$request->location_map_link,
           'currency'=>$request->currency,
           'language'=>$request->language,
           'working_time'=>$request->working_time,
           'working_days'=>$request->working_days,
           'copyright_text'=>$request->copyright,
           'android_app_link'=>$request->android_app_link,
           'ios_app_link'=>$request->ios_app_link,
           'script_thired_party_api'=>$request->script_thired_party_api
       );
       //Create or update settings
       $setting = Setting::where('id', '>', 0)->update(
            $settingArray
        );
        return redirect()->route('settings.index', ['settings' => $setting]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
