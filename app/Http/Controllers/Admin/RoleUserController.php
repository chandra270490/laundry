<?php

namespace App\Http\Controllers\Admin;
// session_start();
use App\Http\Controllers\Controller;
use App\Model\RoleUser;
use App\Model\Role;
use App\User;
use Illuminate\Http\Request;

class RoleUserController extends Controller
{
   
    public function assignRole($id)
    {
        $user = User::where('id',$id)->select('id','name')->first();
        $userRoles = array();
        if(RoleUser::where(['user_id'=>$id])->exists())
            $userRoles = RoleUser::where(['user_id'=>$id])->pluck('role_id')->toArray();
        return view('admin.users.assignrole')->with(['user'=>$user,'userroles'=>$userRoles]);
    }
   
    public function saveRole(Request $request)
   {
        $roles = $request->roles;
        //Fetched User name
        $user_name = User::where('id',$request->user)->select('name')->first();
        $userRoleArray =[];
        $messages = [];
        $roleException = '';
        //Iterate roles
        foreach($roles as $role)
        {
            $userrole = [
                'role_id' => $role,
                'user_id' => $request->user,
            ];
            $user_role = Role::where('id',$role)->select('name')->first();
            //Check if role already exists
            if(!RoleUser::where(['role_id'=>$role,'user_id'=>$request->user])->exists())
                array_push($userRoleArray,$userrole); 
            else
                array_push($messages,$user_role->name." Assigned to ".$user_name->name);
        }
        // If role already assigned to current user
        if(!empty($messages))
         $roleException = " Except with following combinations ".implode(',',$messages);
       
        $userRole = new RoleUser();
        if($userRole->insert($userRoleArray))
        {
            return redirect('admin/users')->with('message', 'Role has been assigned successfully to '.$user_name->name." ".$roleException);
        }

      
   }
   
}
