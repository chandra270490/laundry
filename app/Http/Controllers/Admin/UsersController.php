<?php

namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Session;
use Redirect;
use Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id',3)->get();
      // dd($users);
        return view('admin.users.index')->with('users',$users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {

        return view('admin.users.update')->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update User
        $user = User::find($id);
        $user->name       = $request->name;
        $user->email       = $request->email;
        if(!empty($request->password))
        $user->password       = Hash::make($request->password);
        $user->mobile_no       = $request->mobile_no;
        $user->save();

        // redirect
        Session::flash('message', 'Successfully updated user!');
        return Redirect::to('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(User::find($id)->delete())
        {
            return redirect('/admin/users')->with('message', 'The User has been deleted successfully');
        } else {
            return redirect('/admin/users')->with('message', 'There is some problem');
        }
    }

    
    /* Show user profile */

    public function showprofile($id)
    {
        $user= User::where('id',$id)->get()->first();
        return view('admin.users.profile')->with('user',$user);
    }
    
    /* Update user profile */
    
    public function updateprofile(Request $request)
    {
        // Update User
        $user = User::find($request->user_id);
        //File upload
        $profile_pic = '';
        if($request->hasFile('profile_pic')){
            $photo = $request->file('profile_pic');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/users'), $new);
            $profile_pic = $new;
        };
        if(!empty($profile_pic))
            $user->profile_pic = $profile_pic;
        $user->name       = $request->name;
        $user->email       = $request->email;
        if(!empty($request->password))
        $user->password       = Hash::make($request->password);
        $user->mobile_no       = $request->mobile_no;
        $user->save();

        // redirect
        Session::flash('message', 'Successfully updated user profile!');
        return Redirect::to('customer/profile');
    }
}
