<?php
namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RoleModule;
use App\Model\Module;
use App\Model\RoleUser;
use App\Model\Role; 
use App\Model\Order;
use App\User;
use App\Model\Service; 
use App\Model\Page;
use Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
      $services = Service::orderBy('created_at','desc')->take(5)->get();
      $pages = Page::orderBy('created_at','desc')->take(5)->get();
      $servicesCount = Service::count();
      $usersCount = User::count();
      /****CNS */
      $ordersCount = Order::count();
      /****CNS */
      /*
      $id = Auth::user()->id;
      $roleusers = RoleUser::where('user_id',$id)->select('role_id')->get();
      if(count($roleusers) > 1){
        foreach($roleusers as $role => $roleval){
        
          $roleName = Role::where('id',$roleval->role_id)->pluck('name')->first(); 
          if($roleval->role_id == 2)
          {
           
             $request->roles = $roleval->role_id;
             return $this->setRole($request);
             break;
          }
        }
      }else if(count($roleusers) == 1)
      {
        $request->roles = $roleusers[0]->role_id;
        return $this->setRole($request);
      }*/
    
       /****CNS *****/
       return view('admin.dashboard')->with(['services'=>$services,'pages'=>$pages,'servicecount'=>$servicesCount,'ordercount'=>$ordersCount,'usercount'=>$usersCount]);
       /****CNS ******/
    }

     public function setRole(Request $request)
    {
         $role_id = $request->roles;
         unset($_SESSION['roledashboard']);
         $_SESSION['roledashboard'] = $role_id;
         $moduleArray =[];
         $role_modules = RoleModule::where('role_id',$role_id)->select('module_id')->groupBy('module_id')->get();
         if(!empty($role_modules))
         {
            foreach($role_modules as $keyModule=>$valModule)
            {
              $moduleName = Module::where('id',$valModule->module_id)->pluck('name')->first();
                $moduleArray[$keyModule]['name'] =  $moduleName;
  
                switch ($moduleName) {
                  case "pages":
                      $route_name = 'pages.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'far fa-file';
                      $moduleArray[$keyModule]['url_segment'] = 'pages';
                    break;
                 
                  case "blogs":
                      $route_name = 'blogs.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'far fa-file';
                      $moduleArray[$keyModule]['url_segment'] = 'blogs';
                      break;
                  case "services":
                      $route_name = 'services.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'fas fa-map-marker';
                      $moduleArray[$keyModule]['url_segment'] = 'services';
                      break;
                  case "serviceareas":
                      $route_name = 'servicearea.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'fas fa-wrench';
                      $moduleArray[$keyModule]['url_segment'] = 'servicearea';
                    break;
                  case "users":
                    $route_name = 'users.index';
                    $moduleArray[$keyModule]['route'] = $route_name;
                    $moduleArray[$keyModule]['class'] = 'far fa-users';
                    $moduleArray[$keyModule]['url_segment'] = 'users';
                  break;
                  case "Manage Roles":
                      $route_name = 'assignroletomodule';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'far fa-users';
                      $moduleArray[$keyModule]['url_segment'] = 'assignroletomodule';
                    break;
                  
                  case "settings":
                      $route_name = 'settings.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'fas fa-cog';
                      $moduleArray[$keyModule]['url_segment'] = 'settings';
                    break;
                  
                  case "orders":
                      $route_name = 'orders.index';
                      $moduleArray[$keyModule]['route'] = $route_name;
                      $moduleArray[$keyModule]['class'] = 'fa-shopping-cart';
                      $moduleArray[$keyModule]['url_segment'] = 'orders';
                    break;
                  default:
                    
  
                   
                }
               
            }
            $_SESSION['moduleArray'] =  $moduleArray;
         }

         $services = Service::orderBy('created_at','desc')->take(5)->get();
         $pages = Page::orderBy('created_at','desc')->take(5)->get();
         $servicesCount = Service::count();
         $usersCount = User::count();

          return view('admin.dashboard')->with(['services'=>$services,'pages'=>$pages,'servicecount'=>$servicesCount,'usercount'=>$usersCount]);
    } 
}
