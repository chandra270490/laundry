<?php

namespace App\Http\Controllers\Admin;
// session_start();
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Order;
use Session;
use Redirect;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::get();
        return view('admin.orders.index')->with(['orders'=>$orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orders = Order::where('id',$id)->get()->first();
        return view('admin.orders.show')->with('order',$orders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orders = Order::where('id',$id)->get()->first();
        return view('admin.orders.update')->with('order',$orders);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $orders = Order::where('id',$id)->get()->first();
        return view('admin.orders.update')->with('order',$orders);
        */

        // Update the data
        $order = Order::find($id);
        if(isset($request->facility_status))
        {
            $order->facility_status = $request->facility_status;
        }
        if(isset($request->invoice_status))
        {
            $order->invoice_status = $request->invoice_status;
        }
        $order->save();
        // redirect
        Session::flash('message', 'Successfully updated Order!');
        return Redirect::to('admin/orders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
