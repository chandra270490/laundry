<?php

namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Redirect;

class PagesController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = page::where('post_types','1')->orderBy('status','DESC')->orderBy('created_at','DESC')->get();
        return view('admin.pages.index')->with(['pages'=>$pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
       // dd($request->all());
       //Create the Service Array
       $pageArray = array(
           'name'=>$request->name,
           'title'=>$request->title,
           'slug'=>Str::slug($request->name),
           'feature_image'=> $feature_image,
           'media_link'=>$request->media_link,
           'description'=>$request->description,
           'add_top_navbar'=>$request->add_top_navbar ?? 1,
           'add_footer_navbar'=>$request->add_footer_navbar ?? 1,
           'meta_title'=>$request->title,
           'meta_description'=>'',
           'created_by'=>auth()->user()->id,
           'post_types'=>1,
           'canonical_url'=>Str::slug($request->name),
           'status'=>1
       );
       //dd($serviceArray);

       //Create Service
       $page = Page::Create(
            $pageArray
        );
        return redirect('/admin/pages')->with('message', 'Page has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('admin.pages.show')->with('page',$page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('admin.pages.update')->with('page',$page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
       
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
      
        // store
        $page = Page::find($id);
        $page->name       = $request->name;
        $page->title       = $request->title;
        if(!empty($feature_image))
             $page->feature_image = $feature_image;

        $page->media_link       = $request->media_link;
        $page->description       = $request->description;
        $page->add_top_navbar       = $request->add_top_navbar;
        $page->add_footer_navbar       = $request->add_footer_navbar;
        $page->meta_title = $request->title;
        $page->meta_description = '';
        $page->created_by = auth()->user()->id;
        $page->post_types=1;
        $page->canonical_url = Str::slug($request->name);
        $page->save();

        // redirect
        Session::flash('message', 'Successfully updated Page!');
        return Redirect::to('admin/pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page,$id)
    {
      
        if(Page::find($id)->delete())
        {
            return redirect('/admin/pages')->with('message', 'Page has been deleted successfully');
        } else {
            return redirect('/admin/pages')->with('message', 'There is some problem');
        }
       
    }
}
