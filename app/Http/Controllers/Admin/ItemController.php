<?php

namespace App\Http\Controllers\Admin;
// session_start();
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Item;
use App\Model\Service;
use Session;
use Redirect;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::with('service')->get();
        return view('admin.items.index')->with(['items'=>$items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::select('id','name')->get();
        return view('admin.items.create')->with(['services'=>$services]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item_image ='';
        //File upload
         if($request->hasFile('item_image')){
            $photo = $request->file('item_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/items'), $new);
            $item_image = $new;
        };
        $itemArray = array(
            'name'=>$request->name,
            'service_id'=> !empty($request->services) ? $request->services : '',
            'item_image'=>$item_image,
            'status'=>1,
            'customer_price'=>$request->customer_price
        );

        //dd($itemArray);
        //Create Service
       $item = Item::Create(
            $itemArray 
        );
    return redirect('/admin/items')->with('message', 'Item has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = Service::select('id','name')->get();
        $item = Item::where('id',$id)->get()->first();
        return view('admin.items.update')->with(['item'=>$item,'services'=>$services]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update the data
        $item_image ='';
        //File upload
         if($request->hasFile('item_image')){
            $photo = $request->file('item_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/items'), $new);
            $item_image = $new;
        };
        $item = Item::find($id);
        $item->name       = $request->name;
        if(!empty($item_image))
        $item->item_image = $item_image;
        if(!empty($request->services)) 
        $item->service_id       = $request->services;
        $item->customer_price       = $request->customer_price;
        $item->facility_price       = $request->facility_price;
        $item->status = $request->status;
        $item->save();
        // redirect
        Session::flash('message', 'Successfully updated item!');
        return Redirect::to('admin/items');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Item::find($id)->delete())
        {
            return redirect('/admin/items')->with('message', 'Item has been deleted successfully');
        } else {
            return redirect('/admin/items')->with('message', 'There is some problem');
        }
    }

    public function getSubService(Request $request){
        $service = $request->service;
        $services = Service::where('parent_id',$service)->get();
        $servicedata = [];
        if(!empty($services)) 
            return response()->json($services);
        else
            return response()->json($servicedata);
    }
}
