<?php

namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use App\Model\Blog;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Auth;
use Session;
use Redirect;

class BlogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::orderBy('status','DESC')->orderBy('created_at','DESC')->get();
        return view('admin.blogs.index')->with(['blogs'=>$blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/blogs'), $new);
            $feature_image = $new;
        };
       // dd($request->all());
       //Create the Service Array
       $blogArray = array(
           'title'=>$request->title,
           'keywords'=>$request->keywords,
           'tags'=>$request->tags,
           'slug'=>Str::slug($request->title),
           'feature_image'=> $feature_image,
           'description'=>strip_tags($request->description),
           'meta_description'=>'',
           'created_by'=> Auth::user()->id,
           'status'=>$request->status
       );
       //dd($serviceArray);

       //Create Service
       $blog = Blog::Create(
            $blogArray
        );
        return redirect('/admin/blogs')->with('message', 'blog has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        return view('admin.blogs.show')->with('blog',$blog);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        return view('admin.blogs.update')->with('blog',$blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/blogs'), $new);
            $feature_image = $new;
        };
      
        // store
        $blog = Blog::find($id);
 
        $blog->title       = $request->title;
        if(!empty($feature_image))
            $blog->feature_image = $feature_image;


        $request->slug = Str::slug($request->title);
        $blog->tags  = $request->tags;
        $blog->keywords  = $request->keywords;
        $blog->description       = strip_tags($request->description);
        $blog->status       = $request->status;
       
        $blog->meta_description = '';
        $blog->created_by = Auth::user()->id;
   
        $blog->save();

        // redirect
        Session::flash('message', 'Successfully updated blog!');
        return Redirect::to('admin/blogs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog,$id)
    {
        if(Blog::find($id)->delete())
        {
            return redirect('/admin/blogs')->with('message', 'blog has been deleted successfully');
        } else {
            return redirect('/admin/blogs')->with('message', 'There is some problem');
        }
    }
}
