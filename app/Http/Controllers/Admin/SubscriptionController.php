<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
Use App\User;
use Stripe;
use Session;
use Exception;
use Form;

class SubscriptionController extends Controller
{
    public function index()
    {
        return view('frontend.subscription.create');
    }

    public function orderPost(Request $request)
    {
            $user = auth()->user();
            $input = $request->all();
            $token =  $request->stripeToken;
            $paymentMethod = $request->paymentMethod;
           // try {

                Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
                

                if (!isset($user->stripe_id) && is_null($user->stripe_id)) {
                   // $stripeCustomer = $user->createAsStripeCustomer();
                }
    
                // \Stripe\Customer::createSource(
                //     $user->stripe_id, hiddenInput.setAttribute('value', token.id);
                //     ['source' => $token]
                // );

                $customer = \Stripe\Customer::create(array(
                    'name' => 'test',
                    'description' => 'test description',
                    'email' => $user->email,
                    'source' => $token,
                    "address" => ["city" => "New York", "country" => "IND", "line1" => "Rajeev Chowk, New Delhi", "line2" => "Rajeev Chowk, New Delhi", "postal_code" => "110001", "state" => "New Delhi"]
                ));
             
                $charge = \Stripe\Charge::create(array( 
                    'customer' => $customer->id, 
                    'amount'   => '100', 
                    'currency' => 'INR', 
                    'description' => "TrueCAD 2021 Premium", 
                ));

                $user->newSubscription('test',$input['plane'])
                    ->create($paymentMethod, [                     
                        'email' => $user->email,    
                ]);
                
                return back()->with('success','Subscription is completed.');
           
            
    }
}