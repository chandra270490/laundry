<?php

namespace App\Http\Controllers\Admin;
session_start();
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Model\Service;
use Illuminate\Http\Request;
use Session;
use Redirect;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::get();
        return view('admin.services.index')->with(['services'=>$services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $services = Service::where(['parent_id'=>null,'status'=>1])->get();

        return view('admin.services.create')->with('services',$services);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $icon ='';
        //File upload
         if($request->hasFile('icon')){
            $photo = $request->file('icon');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/icons'), $new);
            $icon = $new;
        };

        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/'), $new);
            $feature_image = $new;
        };
       // dd($request->all());
       //Create the Service Array
       $serviceArray = array(
           'name'=>$request->name,
           'title'=>$request->title,
           'slug'=>Str::slug($request->title),
           'icon'=>$icon,
           'feature_image'=> $feature_image,
           'parent_id'=> $request->parent_id,
           'price'=>$request->price,
           'media_link'=>$request->media_link,
           'description'=>$request->description,
           'add_top_navbar'=>$request->add_top_navbar,
           'add_footer_navbar'=>$request->add_footer_navbar,
           'meta_title'=>$request->meta_title,
           'meta_description'=>$request->meta_description,
           'canonical_url'=>$request->canonical_url,
           'status'=>$request->service_availablity
       );
       //dd($serviceArray);

       //Create Service
       $service = Service::Create(
            $serviceArray
        );
        return redirect('/admin/services')->with('message', 'Service has been created successfully');
          
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return view('admin.services.show')->with('service',$service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $services = Service::where('status',1)->get();
        return view('admin.services.update')->with(['service'=>$service,'services'=>$services]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/'), $new);
            $feature_image = $new;
        };

        $icon ='';
        //File upload
         if($request->hasFile('icon')){
            $photo = $request->file('icon');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/services/icons'), $new);
            $icon = $new;
        };
      
       // store
        $service = Service::find($id);
        $service->name       = $request->name;
        $service->price       = $request->price;
        $service->title       = $request->title;
        $service->slug       = Str::slug($request->title);
        if(!empty($feature_image))
            $service->feature_image = $feature_image;

        if(!empty($icon))
            $service->icon = $icon;
 
        $service->media_link       = $request->media_link;
        $service->description       = $request->description;
        $service->add_top_navbar       = $request->add_top_navbar;
        $service->add_footer_navbar       = $request->add_footer_navbar;
        $service->parent_id       = $request->parent_id;
        $service->meta_title = $request->title;
        $service->meta_description = $request->meta_description;
        $service->canonical_url = Str::slug($request->name);
        $service->save();

        // redirect
        Session::flash('message', 'Successfully updated service!');
        return Redirect::to('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service,$id)
    {
        if(Service::find($id)->delete())
        {
            return redirect('/admin/services')->with('message', 'Service has been deleted successfully');
        } else {
            return redirect('/admin/services')->with('message', 'Service has not been deleted successfully');
        }
    }

   
}
