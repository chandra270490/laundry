<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
session_start();
use App\Model\RoleModule;
use App\Model\Module;
use App\Model\Role;
use Illuminate\Http\Request;
use DB;

class RoleModuleController extends Controller
{
    public function assignRoleToModule()
    {
        $roleArray =[];
       // $module = Module::where('id',$id)->select('id','name')->first();
        $modules = Module::select('id','name')->get();
        $roles = Role::all();
        $modulesWithRoles = '';
        $modulesWithRoles = Module::with('allRoles')->whereHas('allRoles')->get();
      
        $assignedRoles = RoleModule::where('role_id','>',0)->select('role_id')->groupBy('role_id')->get();
        //Manipulate original assigned roles to module array to simple roles array 
        if(!empty($assignedRoles))
        {
            foreach($assignedRoles as $roleKey=>$roleVal)
            {
                $roleArray[]=$roleVal->role_id;
            }
        }
       
        $moduleRoles = array();
       // if(RoleModule::where(['module_id'=>$id])->exists())
         //   $moduleRoles = RoleModule::where(['module_id'=>$id])->pluck('role_id')->toArray();
        return view('admin.modules.rolemodule')->with(['roleModulesArray'=>$roleArray,'modules'=>$modules,'roles'=>$roles,'moduleroles'=>$modulesWithRoles]);
    }
   
    public function saveRoleToModule(Request $request)
   {
        DB::enableQueryLog();
        $roles = $request->roles;
        $modules = $request->modules;

        $assignedModules = RoleModule::where('module_id','>',0)->select('module_id')->groupBy('module_id')->get();
        $assignedRoles = RoleModule::where('module_id','>',0)->select('role_id')->groupBy('role_id')->get();
        //Manipulate original assigned roles to module array to simple roles array 
     
        $modArray =[];
        if(!empty($assignedModules))
        {
        
            foreach($assignedModules as $moduleKey=>$moduleVal)
            {
                $modArray[]=$moduleVal->module_id;
               
            }
        }

        $roleArray =[];
        if(!empty($assignedRoles))
        {
        
            foreach($assignedRoles as $roleKey=>$roleVal)
            {
               $roleArray[]=$roleVal->role_id;
            }
        }
        
        // Deallocate the roles with modules
        if(!empty($roleArray) && !empty($request->roles))
        {

            $resultrole=array_diff($roleArray,$request->roles);
            if(!empty($resultrole))
            {
                RoleModule::whereIn('role_id', $resultrole)->delete(); 
            }
           
        }
       // Deallocate the modules with roles
        if(!empty($modArray) && !empty($request->modules))
        {

            $result=array_diff($modArray,$request->modules);
            if(!empty($result))
            {
                RoleModule::whereIn('module_id', $result)->delete(); 
            }
           
        }
    
        $moduleRoleArray =[];
        $messages = [];
        $roleException = '';
        //Iterate roles and allocate module with roles
        foreach($roles as $role)
        {
           foreach($modules as $module)
           {
             $modulerole = [
                'role_id' => $role,
                'module_id' => $module,
             ];
             $user_role = Role::where('id',$role)->select('name')->first();
             $module_name = Module::where('id',$module)->select('name')->first();
            //Check if role already exists
            if(!RoleModule::where(['role_id'=>$role,'module_id'=>$module])->exists())
                array_push($moduleRoleArray,$modulerole); 
            else
                array_push($messages,$user_role->name." Assigned to ".$module_name->name);
           }
           
            
        }
    
        // If role already assigned to current user
        if(!empty($messages))
         $roleException = " Except with following combinations ".implode(',',$messages);
       
        $moduleRole = new RoleModule();
        if($moduleRole->insert($moduleRoleArray))
        {
            return redirect('admin/modules/assignroletomodule')->with('message', 'Role has been assigned successfully to '.$module_name->name." ".$roleException);
        }

      
   }
}
