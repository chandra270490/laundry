<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
session_start();
use App\Model\ServiceArea;
use Illuminate\Http\Request;
use Session;
use Redirect;

class ServiceAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = ServiceArea::all();
        return view('admin.servicearea.index')->with('locations',$locations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.servicearea.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $locationArray = array(
            'name'=>$request->name,
            'address'=>$request->address,
            'postal_code_state'=>$request->postal_code_state,
            'postal_code_city'=>$request->postal_code_city,
            'status'=> $request->status
        );
        //Create Service
       $service = ServiceArea::Create(
        $locationArray
    );
    return redirect('/admin/servicearea')->with('message', 'Location has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ServiceArea  $serviceArea
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $serviceArea = ServiceArea::where('id',$id)->get()->first();
        return view('admin.servicearea.show')->with('location',$serviceArea);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ServiceArea  $serviceArea
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $serviceArea = ServiceArea::where('id',$id)->get()->first();
        return view('admin.servicearea.update')->with('location',$serviceArea);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ServiceArea  $serviceArea
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update the data
        $location = ServiceArea::find($id);
        $location->name       = $request->name;
        $location->address       = $request->address;
        $location->postal_code_state       = $request->postal_code_state;
        $location->postal_code_city       = $request->postal_code_city;
        $location->status = $request->status;

        $location->save();

        // redirect
        Session::flash('message', 'Successfully updated Location!');
        return Redirect::to('admin/servicearea');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ServiceArea  $serviceArea
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(ServiceArea::find($id)->delete())
        {
            return redirect('/admin/servicearea')->with('message', 'Location has been deleted successfully');
        } else {
            return redirect('/admin/servicearea')->with('message', 'Location has not been deleted successfully');
        }
    }
}
