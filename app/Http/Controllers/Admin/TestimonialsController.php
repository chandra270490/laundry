<?php
namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Redirect;

class TestimonialsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::where('post_types','2')->orderBy('status','DESC')->orderBy('created_at','DESC')->get();
        return view('admin.testimonials.index')->with(['pages'=>$pages]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.testimonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
       // dd($request->all());
       //Create the Service Array
       $pageArray = array(
           'test_user_name'=>$request->test_user_name,
           'test_user_role'=>$request->test_user_role,
           'slug'=>'testimonial',
           'feature_image'=> $feature_image,
           'description'=>strip_tags($request->description),      
           'created_by'=>auth()->user()->id,
           'post_types'=>2,
           'status'=>1,

       );
       //dd($serviceArray);

       //Create Service
       $page = Page::Create(
            $pageArray
        );
        return redirect('/admin/testimonials')->with('message', 'Page has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.testimonials.show')->with('page',$page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::where('id',$id)->first();
        return view('admin.testimonials.update')->with('page',$page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
      
        // store
        $page = Page::find($id);
        $page->test_user_name       = $request->test_user_name;
        $page->test_user_role       = $request->test_user_role;
        if(!empty($feature_image))
             $page->feature_image = $feature_image;

        $page->description       = strip_tags($request->description);   
        $page->created_by = auth()->user()->id;
        $page->save();

        // redirect
        Session::flash('message', 'Successfully updated Testimonial!');
        return Redirect::to('admin/testimonials');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Page::find($id)->delete())
        {
            return redirect('/admin/testimonials')->with('message', 'Testimonial has been deleted successfully');
        } else {
            return redirect('/admin/testimonials')->with('message', 'There is some problem');
        }
    }
}
