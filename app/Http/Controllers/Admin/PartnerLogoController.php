<?php
namespace App\Http\Controllers\Admin;
session_start();
use App\Http\Controllers\Controller;
use App\Model\Page;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Session;
use Redirect;

class PartnerLogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = page::where('post_types','3')->orderBy('status','DESC')->orderBy('created_at','DESC')->get();
        return view('admin.partners.index')->with(['pages'=>$pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
       // dd($request->all());
       //Create the Service Array
       $pageArray = array(   
           'feature_image'=> $feature_image,
           'created_by'=>auth()->user()->id,
           'post_type'=>3,
           'status'=>1
       );
       //dd($serviceArray);

       //Create Service
       $page = Page::Create(
            $pageArray
        );
        return redirect('/admin/partners')->with('message', 'Partner has been created successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin.partners.show')->with('page',$page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::where('id',$id)->get();
        return view('admin.partners.update')->with('page',$page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feature_image ='';
        //File upload
         if($request->hasFile('feature_image')){
            $photo = $request->file('feature_image');
            $new =  $photo->getClientOriginalName();
            $photo->move(public_path('images/pages'), $new);
            $feature_image = $new;
        };
      
        // store
        $page = Page::find($id);
       
        if(!empty($feature_image))
             $page->feature_image = $feature_image;
  
        $page->created_by = auth()->user()->id;
        $page->save();

        // redirect
        Session::flash('message', 'Successfully updated Partner!');
        return Redirect::to('admin/partners');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Page::find($id)->delete())
        {
            return redirect('/admin/partners')->with('message', 'Partner has been deleted successfully');
        } else {
            return redirect('/admin/partners')->with('message', 'There is some problem');
        }
    }
}
