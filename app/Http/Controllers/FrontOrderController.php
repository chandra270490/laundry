<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\Order;
use App\User;
use App\Model\RoleUser;
use ApiResponse;
use Validator;
use Redirect;
use Stripe;
use App\helpers\OnFleet;
/***CNS */
use App\Notifications\OrdersNotification;
/***CNS */

class FrontOrderController extends Controller
{
    // Showing Order form
    public function showOrderFrom()
    {
        $slots =   Http::get(env("API_URL", "")."slots")->json();
        $topService =   Http::get(env("API_URL", "")."services")->json();
        $user = new User();
        $intent = $user->createSetupIntent();
    	return view('frontend.page.order')->with(['topService'=>$topService['response'],'slots'=>$slots,'intent'=>$intent]);
    }

    //Get Collection time based on collection date
    public function getCollectionTime(Request $request)
    {
        $start_hours = $request->start_hours;
        $am_pm = $request->am_pm;
        $slots =   Http::get(env("API_URL", "")."slots/".$start_hours.'/'.$am_pm)->json();   
        return response()->json($slots['response']);
    }

    // Get addresses based on API URL 
    public function getAddress(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $postal_code = $request->postal_code;
        
        /*CNS*/
        //$addressDetails=Http::get(env("API_URL", "")."getaddress?postal_code=".$postal_code)->json();
         
        $addressDetails = $client->request('GET', 'https://api.getAddress.io/find/'.$postal_code, [
            'headers' => [
                 'api-key' => 'ZGfuyRDpv06JQS5DPsoFww32552'
             ]
        ]);
        
        /*
        $addressDetails = $client->request('GET', 'https://api.getAddress.io/find/XX200X', [
            'headers' => [
                 'api-key' => 'ZGfuyRDpv06JQS5DPsoFww32552'
             ]
        ]);
        */
         
        $addressDetails = json_decode($addressDetails->getBody(), true);
        //dd(count($addressDetails));

        $addressdata=array();
        $addressdata['data'] = '';
        if(count($addressDetails) > 1){
           $addressdata['data'] = $addressDetails["addresses"];
           $addressdata['postal_code'] = $postal_code;
           return response()->json($addressdata);  
        } else {
            return response()->json($addressdata);
        }
        
         /*CNS*/
    }
    
     //Fetched the delivery date on bases of collection date
     public function getDeliveryDate(Request $request)
     {
        $collection_date = $request->collection_date;
        $delivery_date =[];
        $delivery_date['data'][0] = Carbon::createFromFormat('Y-m-d',  $collection_date)->addDays(1)->format('Y-m-d');
        $delivery_date['data'][1] = Carbon::createFromFormat('Y-m-d',  $collection_date)->addDays(2)->format('Y-m-d');
        $delivery_date['data'][2] = Carbon::createFromFormat('Y-m-d',  $collection_date)->addDays(3)->format('Y-m-d');
        return response()->json($delivery_date['data']);
     }

     //Fetched the delivery time on bases of delivery date
     public function getDeliveryTime(Request $request)
     {
        $delivery_time = explode('-',$request->delivery_time);
        $start_hours = $delivery_time[0];
        $am_pm = $delivery_time[1];
        $slots =   Http::get(env("API_URL", "")."slots/".$start_hours.'/'.$am_pm)->json();   
        return response()->json($slots['response']);
        
     }

      //Fetched the delivery time on bases of delivery date
      public function getDeliveryTimeBCKup(Request $request)
      {
         $delivery_time = explode('-',$request->delivery_time);
         // Slot 1 for dropdown
         $slots1 =[];
         $slots1[0] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(0)->format('H:i A');
         $slots1[1] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(2)->format('H:i A');
          // Slot 2 for dropdown
         $slots2 =[];
         $timeSecond = Carbon::createFromFormat('H',  $delivery_time[0])->addhours(4)->format('H');
         if($timeSecond >= 7 && $timeSecond <= 23)
         {
             $slots2[0] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(2)->format('H:i A');
             $slots2[1] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(4)->format('H:i A');
         }
         
        
          // Slot 3 for dropdown
         $slots3 =[];
         $timeThird = Carbon::createFromFormat('H',  $delivery_time[0])->addhours(8)->format('H');
         if($timeThird >= 7 && $timeThird <= 23)
         {
             $slots3[0] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(6)->format('H:i A');
             $slots3[1] = Carbon::createFromFormat('H:i A',  $delivery_time[0])->addhours(8)->format('H:i A');
         }
        if(!empty($slots1))
         $delivery_time_array['data'][0] = $slots1[0]." - ".$slots1[1];
        if(!empty($slots2))
         $delivery_time_array['data'][1] = $slots2[0]." - ".$slots2[1];
        if(!empty($slots3))
         $delivery_time_array['data'][2] = $slots3[0]." - ".$slots3[1];
         // echo"<pre>";
         // print_r($delivery_time_array);
         // die;
          return response()->json($delivery_time_array['data']);
      }

    public function saveOrder(Request $request)
    {  
        $totalCost=0;
        $jsonData=json_encode($request->services,true);

        //dd($jsonData);
        
        $items = json_decode(json_decode($jsonData));
        $validator = Validator::make($request->all(),[
            'items' => 'required',
          ]);

        if($items)
        {
          foreach($items->service as $mydata)
          {
            $totalCost+=$mydata->price;
          }
        }

        if(!($user=User::where('email',$request->email)->first()))
       {
          $user = new User([
              'name' =>  $request->fName,
              'email' => $request->email,
              'password' => bcrypt($request->password),
              'mobile_no'=>$request->mobile_number,
              'role_id'=> 3,
              'addLine'=>$request->addLine,
              'pCode'=>$request->pCode,
          ]);
          if($user->save())
          {
            
            //$user = $request->user();
            $this->login($request);
            
          }

       } else {
            $user = $request->user();
       }
        
        
        
      
        $amount = $request->amount;
        $paymentMethod = $request->input('payment_method');
      
        $user->createOrGetStripeCustomer();
        $user->updateDefaultPaymentMethod($paymentMethod);
        $user->charge($amount * 100,$paymentMethod ); 
        
        $order_no = str_pad(Date('mYdis') + 1, 8, "0", STR_PAD_LEFT);
       //Create the Settings Array
       $orderArray = array(
           'postcode'=>$request->postalCode,
           'address'=>$request->single_address,
           'extra_address'=>$request->extraRequest,
           'services'=>json_encode($request->services,true),
           'extra_service_request'=>$request->anyRequest,
           'collection_date'=>$request->collection_date,
           'collection_time'=>$request->collection_time,
           'collection_instruction'=>$request->colInstruction,
           'delivery_date'=>$request->delivery_date,
           'delivery_time'=>$request->delivery_time,
           'delivery_instruction'=>$request->delInstruction,
           'first_name'=>$request->fName,
           'last_name'=>$request->lName,
           'email_id'=>$request->email,
           'mobile_no'=>$request->mobile_number,
           'payment_gateway_with_card'=>1,
           'status'=>1,
           /*'order_no'=>str_pad(Date('mYdis') + 1, 8, "0", STR_PAD_LEFT),*/
           'order_no'=>$order_no,
           'order_date'=>date('y-d-m'),
           'order_frequency'=>1,
           'facility_name'=>$request->fName,
           'ready_by'=> '',
           'invoice_status'=>0,
           'payment_status'=>1,
           'sms_or_mail'=>1,
           'customer_type' =>3,     
           'total_payment'=>$totalCost,
           'facility_status'=>1
       );
       if(Order::create($orderArray))
       {
        $config = array("api_name" => "Username", "api_key" => "ac7cf839a067ad96ce78c86203a2c373");
        $onfleet = new Onfleet($config);

        //$val = array("destination"=> array("address"=> array("unparsed"=> "2829 Vallejo St, SF, CA, USA"), "notes"=> "Small green door by garage door has pin pad, enter *4821*"), "recipients"=> array("0"=> array("name"=> $request->fName, "phone"=> "7825432715", "notes"=> "Knows Neiman, VIP status.")), "notes"=> "Order 332: 24oz Stumptown Finca El Puente, 10 x Aji de Gallina Empanadas, 13-inch Lelenitas Tres Leches", "autoAssign"=> array("mode"=> "distance"));

        /*Task Creation*/
        //$val = array("destination"=> array("address"=> array("unparsed"=> $request->single_address), "notes"=> $request->extraRequest), "recipients"=> array("0"=> array("name"=> $request->fName.' '.$request->lName, "phone"=> $request->mobile_number, "notes"=> $request->delInstruction)), "notes"=> $request->colInstruction, "autoAssign"=> array("mode"=> "distance"));

        $apart = '123 Floor';
        $state = 'West Midlands';
        $postalCode = 'SW1A';
        $number = '123';
        $street = '1 New Cathedral Street, Manchester';
        $city = 'Birmingham';
        $country = 'England';
        $lat = 53.483856;
        $long = -2.245231;
        $notes = 'GOOD NOTES';

        //$address = $request->single_address.', '.$request->extraRequest.', '.$request->postalCode;

        $address = '{"address": {"apartment": '.$apart.', "state": '.$state.',"postalCode": '.$postalCode.',"number": '.$number.',"street": '.$street.',"city": '.$street.',"country": '.$country.'}}';

        //$compb = strtotime(".$request->collection_date." ".$request->collection_time.");
        $compb = time();
        $val = array("destination"=> array("address"=> array("unparsed"=> $address), "notes"=> $request->extraRequest), "recipients"=> array("0"=> array("name"=> $request->fName.' '.$request->lName, "phone"=> $request->mobile_number, "notes"=> $request->delInstruction)), "notes"=> $request->colInstruction, "autoAssign"=> array("mode"=> "distance"));

        $msg=$onfleet->task_create($val);

        /*Receipent Creation*/
        $receipient = array("name"=>$request->fName,"phone"=> $request->mobile_number,"notes"=> $request->single_address.', '.$request->extraRequest.', '.$request->delInstruction);
        $msg2 = $onfleet->recipient_create($receipient);

        /*Destination Creation*/
        $apart = '123 Floor';
        $state = 'West Midlands';
        $postalCode = 'SW1A';
        $number = '123';
        $street = '1 New Cathedral Street, Manchester';
        $city = 'Birmingham';
        $country = 'England';
        $lat = 53.483856;
        $long = -2.245231;
        $notes = 'GOOD NOTES';

        $destination = '{"address": {"apartment": '.$apart.', "state": '.$state.',"postalCode": '.$postalCode.',"number": '.$number.',"street": '.$street.',"city": '.$street.',"country": '.$country.'},"notes":'.$notes.'"}';

        $destination = '{"address":{"number":'.$request->single_address.',"street":'.$request->single_address.',"apartment":'.$request->single_address.',"city":'.$request->single_address.',"state":'.$request->extraRequest.',"country":"UK"},"notes":'.$request->delInstruction.'"}';
        //$destination = json_decode($destination);
        $msg3=$onfleet->destination_create($destination);
        
        // echo $msg3;
        // echo $msg;
        // echo $msg2;
        // echo $msg;
       }
       /*
       if(!($user=User::where('email',$request->email)->first()))
       {
          $user = new User([
              'name' =>  $request->fName,
              'email' => $request->email,
              'password' => bcrypt($request->password),
              'mobile_no'=>$request->mobile_number,
              'role_id'=> 3,
              'addLine'=>$request->addLine,
              'pCode'=>$request->pCode,
          ]);
          if($user->save())
          {
            
            //$user = $request->user();
            return $this->login($request);
            
          }
       } */

        /****CNS */
        $userSchema = User::first();

        $orders = Order::get()->where('order_no', $order_no);

        $orders = json_decode($orders);

        $orderData = $orders;

        $userSchema->notify(new OrdersNotification($orderData));
        /****CNS */

        return redirect()->route('customerdashboard');
    }
    
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors(['errors'=>$validator->errors()->all()]);
            //return response(['errors'=>$validator->errors()->all()], 422);
        }
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
        {
            return Redirect::back()->withErrors(['errors'=>'Please check credentials']);
            // return response()->json([
            //     'message' => 'Unauthorized'
            // ], 401);
        }
           
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
       
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $request->session()->put('token',$tokenResult->accessToken);
        return Redirect::to('/dashboard');
    }
}
