<?php

namespace App\Http\Controllers\API;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Model\RoleUser;
use ApiResponse;
use Validator;
use Redirect;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

    class AuthController extends Controller
    {
        /**
         * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'first_name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed',
            'mobile_no'=>'string'
        ]);
        if ($validator->fails())
        {
        
            return Redirect::back()->withErrors(['errors',$validator->errors()->all()]);
            //return response(['errors'=>$validator->errors()->all()], 422);
        }
        $name = $request->first_name;
        $name .= isset($request->last_name) && !empty($request->last_name) ? " ".$request->last_name:'';
        $user = new User([
            'name' =>  $name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'mobile_no'=>$request->mobile_no,
            'role_id'=> 3
        ]);
       
        if($user->save())
        {
            return $this->login($request);
            // return Redirect::to('/dashboard');
            //return ApiResponse::ApiJsonResponse(200,'API User Creation','API User Created',$user,200);
        }
       
    }
  
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors(['errors'=>$validator->errors()->all()]);
            //return response(['errors'=>$validator->errors()->all()], 422);
        }
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
        {
            return Redirect::back()->withErrors(['errors'=>'Please check credentials']);
            // return response()->json([
            //     'message' => 'Unauthorized'
            // ], 401);
        }
           
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
       
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $request->session()->put('token',$tokenResult->accessToken);
        // return response()->json([
        //     'access_token' => $tokenResult->accessToken,
        //     'token_type' => 'Bearer',
        //     'expires_at' => Carbon::parse(
        //         $tokenResult->token->expires_at
        //     )->toDateTimeString()
        // ]);
        return Redirect::to('/dashboard');
    }
  
    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
       // dd($request->user());
       // $request->user()->token()->revoke();
        $request->session()->forget('token');
        // return response()->json([
        //     'message' => 'Successfully logged out'
        // ]);
        return Redirect::to('/login');
    }
  
    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}