<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\helpers\ApiResponse;
use App\Model\Slot;

class ApiOrderController extends Controller
{
    public function index($start_time=null,$start_format=null)
    {
        
        $message1 = 'Slots Lists';
        if(!empty($start_time) && !empty($start_format))
        {
            if($start_time != 12)
                $time_id = Slot::where('start_hours','<=',$start_time)->where('end_hours','>',$start_time)->where('start_format',$start_format)->where('status',1)->pluck('id')->first();
            else if($start_time == 12)
                $time_id = Slot::where('start_hours','=',$start_time)->where('start_format',$start_format)->where('status',1)->pluck('id')->first();
            $slotsDB = Slot::where('id','>=',$time_id)->where('status',1)->get();
            
        } else{
            $slotsDB = Slot::where('status',1)->get();
        }
        $status = '200';
            if(!empty($slotsDB))
            {
                $message2 = 'Slots Listing';
            }else
            {
                $message2 = 'You don’t have any slot for the selected day';
                $status = '404';
            }
        $responseData = ApiResponse::ApiJsonResponse($status, $message1,$message2,$slotsDB, 200);
        return $responseData;
    }
}
