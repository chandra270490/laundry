<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Service;
use App\Model\Setting;
use App\Model\ServiceArea;
use App\Model\Blog;
use App\Model\Page;
use App\Model\PostType;
use App\Model\Item;
use App\helpers\ApiResponse;
use Validator;
 
class ApiPageController extends Controller
{
   // Get all services
    public function services($slug=null){
        $status = '200';
        if(empty($slug))
        {
            $message1 = 'Service Lists';
            $services = Service::with(['children.items'])->where(['status'=>1,'parent_id'=>null])->get();
            if(!empty($services))
            {
                $message2 = 'Service listing';
            }else
            {
                $message2 = 'You don’t have any service for the selected day';
                $status = '404';
            }
               
        } else {
            if($slug === 'topnavbar'){
                $message1 = 'Service Details';
                $services = Service::with(['children'=>function($query){
                    $query->select('name','slug','parent_id');
                }])->where(['status'=>1,'add_top_navbar'=>1])->get();
                if(!empty($services))
                {
                    $message2 = 'Service Details';
                }else{
                    $message2 = 'You don’t have any detail regarding this service for the selected day';
                    $status = '404';
                }
            }else if($slug === 'footernavbar'){
                $message1 = 'Service Details';
                $services = Service::with(['children'=>function($query){
                    $query->select('name','slug','parent_id');
                }])->where(['status'=>1,'add_footer_navbar'=>1])->get();
                if(!empty($services))
                {
                    $message2 = 'Service Details';
                }else{
                    $message2 = 'You don’t have any detail regarding this service for the selected day';
                    $status = '404';
                }
            }  else {
                $message1 = 'Service Details';
                $services = Service::with(['children'=>function($query){
                    $query->select('name','slug','parent_id');
                }])->where(['status'=>1,'slug'=>$slug])->first();
                if(!empty($services))
                {
                    $message2 = 'Service Details';
                }else{
                    $message2 = 'You don’t have any detail regarding this service for the selected day';
                    $status = '404';
                }
            }
           
               
        }
    
       $responseData = ApiResponse::ApiJsonResponse($status, $message1,$message2, $services, 200);
        return $responseData;
    
    }
    
    // Get all settings
    public function settings(){
        $settings=[];
        $settings = Setting::first();
        if(!empty($settings)) {  
            $responseData = ApiResponse::ApiJsonResponse('200', 'Settings','Settings listing', $settings, 200);
        } else {
            $responseData = ApiResponse::ApiJsonResponse('404', 'Settings Lists',' You don’t have any Setting for the selected day', $settings,200); 
        }
        return $responseData;
        
    }
    // Get all blogs
    public function blogs($slug=null){
        $status = '200';
        if(empty($slug))
        {
            $message1 = 'Blogs Lists';
            $blogs = Blog::where('status',1)->get();
            if(!empty($blogs))
            {
                $message2 = 'Blogs listing';
            }else{
                $message2 = 'You don’t have any blog for the selected day';
                $status = '404';
            }
               
        } else {
            $message1 = 'Blog Details';
            $blogs = Blog::where(['status'=>1,'slug'=>$slug])->get()->first();
            if(!empty($blogs))
            {
                $message2 = 'Blog Details';
            }else{
                $message2 = 'You don’t have any detail regarding this blog for the selected day';
                $status = '404';
            }
               
        }

       $responseData = ApiResponse::ApiJsonResponse($status, $message1,$message2, $blogs, 200);
       
       return $responseData;

    }
    
    // Get all pages
    public function pages($slug=null,$postType=null){
        $status = '200';

       if(empty($slug) && empty($postType)){
             //Get All pages
             $message1 = 'Pages Lists';
             $pages = Page::where('status',1)->where('post_types',1)->get();
           
        } else if(!empty($slug) && empty($postType)){
            $message1 = 'Page Details';
            $pages = Page::where(['status'=>1,'slug'=>$slug])->first();
        } else {
             //Get Page Detail
            $message1 = 'Page Details';
            $post_type = PostType::where('id',$postType)->orWhere('slug',$postType)->pluck('id')->first();
            $pages = Page::where(['status'=>1,'slug'=>$slug])->where('post_types',$post_type)->get();
            // dd($pages);   
        }
        if($slug=="about-us")
        {
            $pages=Page::where(['status'=>1,'slug'=>$slug])->first();

        }
        if($slug=="testimonial")
        {
            $pages=Page::where(['status'=>1,'slug'=>$slug])->get();

        } 

        if(!empty($pages))
        {
            $message2 = 'Page Details';
        }else{
            $status = '404';
            $message2 = 'You don’t have any detail regarding this page for the selected day';
        }
    
       $responseData = ApiResponse::ApiJsonResponse($status, $message1,$message2, $pages, 200);
       
       return $responseData;
        
    }
  
    //Get header nav menus items
    public function getMenuPages($add_navbar=null){
       
        $status = '200';
       if($add_navbar == 1)
       {
          //Get top nav menus items
         $message1 = 'Top Nav bar pages';
         $MenuPageItems=Page::where(['add_top_navbar'=>1,'post_types'=>1,'status'=>1])->get();
         if(!empty($MenuPageItems))
            {
                $message2 = 'Top Nav bar pages';
            }else{
                $status = '404';
                $message2 = 'You don’t have any Top Nav bar pages';
            }
       } else if($add_navbar == 2) {
         //Get footer nav menus items
        $message1 = 'Footer Nav bar pages';
        $MenuPageItems=Page::where(['add_footer_navbar'=>1,'post_types'=>1,'status'=>1])->get();
        if(!empty($MenuPageItems))
           {
               $message2 = 'Footer Nav bar pages';
           }else{
               $status = '404';
               $message2 = 'You don’t have any Footer Nav bar pages';
           }  

       }
         
        $responseData = ApiResponse::ApiJsonResponse($status,  $message1, $message2, $MenuPageItems, 200);
        return $responseData;
    }
   
    // Contact Us page
     public function sendMail(Request $request)
     {
        $validator = Validator::make($request->all(),[ 
            'name'=>'required|min:3', 
            'phone' => 'required',
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return ApiResponse::ApiJsonResponse('404', "Send Email Error",  $validator->errors(),false,200);
        }
        $address = !empty($request->address) ? $request->address :'';
        $email = !empty($request->email) ? $request->email :'';
        $address_data ='';
        $email_data ='';
        if(!empty($address))
        $address_data = " Address- ".$address;
        if(!empty($email))
        $email_data = " Email- ".$email;
        $details = [
            'name'=>$request->name,
            'subject' => $request->name." thanks for registering with us with following details: Name-".$request->name." Phone/Whatsapp number- ".$request->phone.$address_data.$email_data,
            'message' => $request->message
        ];
       
        if(\Mail::to('hareeshmandhan83@gmail.com')->send(new \App\Mail\MyTestMail($details)))
            $responseData = ApiResponse::ApiJsonResponse('200', 'Email is Sent.',' Email is Sent.',true,200); 
        else
            $responseData = ApiResponse::ApiJsonResponse('200', 'Email Sent Error.',' There is some problem in sending email.',false,200); 
        
        return $responseData;
     }
     //Get address based on zip code

     public function getAddress(Request $request)
     {
        $postal_code =$request->postal_code;
         //Get top nav menus items
         $message1 = 'Address Listing';
         $status = '200';
         $addresses=ServiceArea::where('postal_code_state',$postal_code)->orWhere('postal_code_city',$postal_code)->get();
         if(!empty($addresses))
            {
                $message2 = 'Address Listing';
            }else{
                $status = '404';
                $message2 = 'You don’t have any address for selected page';
            }
        $responseData = ApiResponse::ApiJsonResponse($status,  $message1, $message2, $addresses, 200);
        return $responseData;
     }
     public function items()
     {
        $items=[];
       $items = Item::with('service')->get();
        if(!empty($items)) {  
            $responseData = ApiResponse::ApiJsonResponse('200', 'Item','Item listing', $items, 200);
        } else {
            $responseData = ApiResponse::ApiJsonResponse('404', 'Item Lists',' You don’t have any Items for the', $items,200); 
        }
        return $responseData;
     }
   
}
