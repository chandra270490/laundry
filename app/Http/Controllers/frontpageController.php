<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Model\Blog;
use App\Model\Order;
use Auth;
class frontpageController extends Controller
{
    public function __construct()
    {
        $this->ServicesContent=Http::get(env("API_URL", "")."services")->json();
        $this->blogs=Http::get(env("API_URL", "")."blogs")->json();
    }
    public function index()
    {
        $setting=Http::get(env("API_URL", "")."settings")->json();
        $items=Http::get(env("API_URL", "")."items")->json();
        //dd($items);
        $aboutContent=Http::get(env("API_URL", "")."pages/about-us")->json();
        $testimonialContents=Http::get(env("API_URL", "")."pages/testimonial/2")->json();
        $aboutContentWithIndex=$aboutContent;
        $aboutContent=$aboutContentWithIndex['response'];
        //dd($aboutContent['response']);
       	$topNavPages=Http::get(env("API_URL", "")."nav/pages/getmenupages/1")->json();
    	return view('frontend.page.index')->with(
            [
                'setting'=>$setting,
                'items'=>$items,
                'aboutContent'=>$aboutContent,
                'ServicesContent'=>$this->ServicesContent,
                "topNavPages"=>$topNavPages,
                "blogs"=>$this->blogs,
                'testimonialContents'=>$testimonialContents['response']
            ]
        );
    }
    // Showing dashboard
    public function dashboard()
    {
		
		$user_email = Auth::user()->email;
        $orders = Order::where('email_id',$user_email)->get()->where('invoice_status',1);
		
		
        return view('frontend.page.dashboard')->with(['orders'=>$orders]);
    }
	
	public function invoicedetail(Request $request)
    {
		$id = $request->segment(2);
		$user_email = Auth::user()->email;
        $orders = Order::where('id',$id)->get();
		
		
        return view('frontend.page.invoicedetail')->with(['orders'=>$orders]);
    }

    public function page($slug,$post_type =null)
    {
       $items=Http::get(env("API_URL", "")."items")->json();
    	if($slug==='contact-us' || $slug==='contact_us')
    	{
            $setting=Http::get(env("API_URL", "")."settings")->json();
    		return view('frontend.page.contact')->with(['setting'=>$setting['response']]);
    	}
    	else if($slug==='services')
    	{
            return view('frontend.service.services')->with(['ServicesContent'=>$this->ServicesContent]);
    	}else if($slug==='price')
        {
            return view('frontend.page.price')->with(['ServicesContent'=>$this->ServicesContent,'items'=>$items]);
        }else if($slug==='blogs')
    	{
            return view('frontend.blog.blogs')->with(['blogs'=>$this->blogs]);
    	}else if($slug=='register')
        {
            return view('frontend.auth.register');
        }
        else if($slug=='login')
        {
            return view('frontend.auth.login');
        }
    	else 
    	{
            if($post_type == null)
            {
                $page=Http::get(env("API_URL","")."pages/".$slug)->json();
            }else{
                $page=Http::get(env("API_URL","")."pages/".$slug.'/'.$post_type)->json();
            }
            if($page['response']!='')
            {
    		  return view('frontend.page.page')->with(['page'=>$page]);
            }
            else
            {
               // return redirect()->to('/');
            }
    	}
    }
    public function serviceDetails($slug)
    {
        $ServiceDetails=Http::get(env("API_URL", "")."services/".$slug)->json();
    	if($ServiceDetails["response"]!='')
        {
            return view('frontend.service.service')->with(['ServiceDetails'=>$ServiceDetails,'ServicesContent'=>$this->ServicesContent]);
        }
        else
        {
            return redirect()->to('/services');
        }
    }
    public function blogDetails($slug)
    {
            $blog=Http::get(env("API_URL","")."blogs/".$slug)->json()['response'];
            if (isset($blog)) {
                return view('frontend.blog.blog')->with(['blog'=>$blog]);   
            }
            else
            {
                $error='<strong> Sorry!!!</strong> We are unable to find blog- <i> '.$slug.'</i>';
                return redirect('blogs')->with('error',$error);
            }
    }

}

