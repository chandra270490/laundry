<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\Model\Order;
use App\User;
use Session;
use Redirect;

class FrontCustomerController extends Controller
{
    //Showing customer profile
    public function userProfile()
    {
        $user = Auth::user();
        return view('frontend.customer.profile')->with(['customer'=>$user]);
    }
    //customer update profile
    public function UserUpdateProfile(Request $request)
    {
         // Update User
         $user = User::find($request->customer_id);
         //File upload
         $profile_pic = '';
         if($request->hasFile('profile_pic')){
             $photo = $request->file('profile_pic');
             $new =  $photo->getClientOriginalName();
             $photo->move(public_path('images/frontend/users'), $new);
             $profile_pic = $new;
         };
         if(!empty($profile_pic))
             $user->profile_pic = $profile_pic;
         $user->name       = $request->name;
         $user->email       = $request->email;
         if(!empty($request->password))
            $user->password       = Hash::make($request->password);
         $user->mobile_no       = $request->mobile_no;
         $user->addLine       = $request->addLine;
         $user->pCode         = $request->pCode;
         $user->save();
         // redirect
         Session::flash('message', 'Profile update successfully!!!');
         return Redirect::to('customer/profile');
    }
    //Showing customer orders
    public function orders()
    {
        $user_email = Auth::user()->email;
        $orders = Order::where('email_id',$user_email)->get();
        return view('frontend.customer.order')->with(['orders'=>$orders]);
    }
     //Showing customer invoices
     public function invoices()
     {
        $user_email = Auth::user()->email;
        $orders = Order::where('email_id',$user_email)->first();
        return view('frontend.customer.invoice')->with(['orders'=>$orders]);
     }
}
