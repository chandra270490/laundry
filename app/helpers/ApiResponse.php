<?php

namespace App\Helpers;

class ApiResponse{

    public static function ApiJsonResponse($statusApi,$title,$message,$response,$statusServer)
    {
        return response()->json([
            'status'=>$statusApi,
            'title'=>$title,
            'message' => $message,
            'response'=>$response
        ], 200); 
    }
}
