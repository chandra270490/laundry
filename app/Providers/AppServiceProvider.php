<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Http;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.master',function($view){
	        $setting    =   Http::get(env("API_URL", "")."settings")->json();
            $topNavPages =   Http::get(env("API_URL", "")."nav/pages/getmenupages/1")->json();
            $topNavService =   Http::get(env("API_URL", "")."services")->json();
            $view->with(['setting'=>$setting['response'],'topNavPages'=>$topNavPages,'topNavService'=>$topNavService['response']]);
        });
        //Sidebar Data
        view()->composer('frontend.inc.sidebar',function($view){
            $sidebarSetting    =   Http::get(env("API_URL", "")."settings")->json();
            $view->with(['sidebarSetting'=>$sidebarSetting['response']]);
        });
    }
}
